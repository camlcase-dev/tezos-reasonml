'use strict';

var Tezos_Util = require("./Tezos_Util.js");
var Json_decode = require("@glennsl/bs-json/lib/js/src/Json_decode.js");
var Tezos_Entrypoint = require("./Tezos_Entrypoint.js");
var Tezos_Expression = require("./Tezos_Expression.js");
var Caml_js_exceptions = require("bs-platform/lib/js/caml_js_exceptions.js");

var Raw = {};

function toRaw(t) {
  return {
          entrypoint: Tezos_Entrypoint.toString(t.entrypoint),
          value: Tezos_Expression.encode(t.value)
        };
}

function decode(json) {
  var val;
  var val$1;
  try {
    val = Json_decode.field("entrypoint", (function (a) {
            return Tezos_Util.unwrapResult(Tezos_Entrypoint.decode(a));
          }), json);
    val$1 = Json_decode.field("value", (function (a) {
            return Tezos_Util.unwrapResult(Tezos_Expression.decode(a));
          }), json);
  }
  catch (raw_error){
    var error = Caml_js_exceptions.internalToOCamlException(raw_error);
    if (error.RE_EXN_ID === Json_decode.DecodeError) {
      return {
              TAG: /* Error */1,
              _0: error._1
            };
    }
    throw error;
  }
  return {
          TAG: /* Ok */0,
          _0: {
            entrypoint: val,
            value: val$1
          }
        };
}

exports.Raw = Raw;
exports.toRaw = toRaw;
exports.decode = decode;
/* No side effect */
