'use strict';

var Jest = require("@glennsl/bs-jest/lib/js/src/jest.js");
var Int64 = require("bs-platform/lib/js/int64.js");
var Caml_int64 = require("bs-platform/lib/js/caml_int64.js");
var Pervasives = require("bs-platform/lib/js/pervasives.js");
var Belt_Result = require("bs-platform/lib/js/belt_Result.js");
var Tezos_Mutez = require("../../src/Tezos_Mutez.js");
var Caml_exceptions = require("bs-platform/lib/js/caml_exceptions.js");

var TestError = Caml_exceptions.create("Tezos_Mutez_Spec.TestError");

function ofStringUnsafe(s) {
  var s$1 = Tezos_Mutez.ofString(s);
  if (!s$1.TAG) {
    return s$1._0;
  }
  throw {
        RE_EXN_ID: TestError,
        _1: s$1._0,
        Error: new Error()
      };
}

Jest.describe("succ", (function (param) {
        return Jest.test("0", (function (param) {
                      return Jest.Expect.toEqual(Tezos_Mutez.one, Jest.Expect.expect(Tezos_Mutez.succ(Tezos_Mutez.zero)));
                    }));
      }));

Jest.describe("ofInt", (function (param) {
        Jest.test("0", (function (param) {
                return Jest.Expect.toEqual({
                            TAG: /* Ok */0,
                            _0: Tezos_Mutez.zero
                          }, Jest.Expect.expect(Tezos_Mutez.ofInt(0)));
              }));
        Jest.test("1", (function (param) {
                return Jest.Expect.toEqual({
                            TAG: /* Ok */0,
                            _0: Tezos_Mutez.one
                          }, Jest.Expect.expect(Tezos_Mutez.ofInt(1)));
              }));
        Jest.test("max_int", (function (param) {
                return Jest.Expect.toEqual(Tezos_Mutez.ofString("2147483647"), Jest.Expect.expect(Tezos_Mutez.ofInt(Pervasives.max_int)));
              }));
        return Jest.test("-1", (function (param) {
                      return Jest.Expect.toEqual({
                                  TAG: /* Error */1,
                                  _0: "ofInt: Expected non-negative int."
                                }, Jest.Expect.expect(Tezos_Mutez.ofInt(-1)));
                    }));
      }));

Jest.describe("ofInt64", (function (param) {
        Jest.test("0", (function (param) {
                return Jest.Expect.toEqual({
                            TAG: /* Ok */0,
                            _0: Tezos_Mutez.zero
                          }, Jest.Expect.expect(Tezos_Mutez.ofInt64(Caml_int64.zero)));
              }));
        Jest.test("1", (function (param) {
                return Jest.Expect.toEqual({
                            TAG: /* Ok */0,
                            _0: Tezos_Mutez.one
                          }, Jest.Expect.expect(Tezos_Mutez.ofInt64(Caml_int64.one)));
              }));
        Jest.test("max_int", (function (param) {
                return Jest.Expect.toEqual(Tezos_Mutez.ofString("9223372036854775807"), Jest.Expect.expect(Tezos_Mutez.ofInt64(Int64.max_int)));
              }));
        return Jest.test("-1", (function (param) {
                      return Jest.Expect.toEqual({
                                  TAG: /* Error */1,
                                  _0: "ofInt64: Expected non-negative int64."
                                }, Jest.Expect.expect(Tezos_Mutez.ofInt64(Caml_int64.neg_one)));
                    }));
      }));

Jest.describe("ofFloat", (function (param) {
        Jest.test("0", (function (param) {
                return Jest.Expect.toEqual({
                            TAG: /* Ok */0,
                            _0: Tezos_Mutez.zero
                          }, Jest.Expect.expect(Tezos_Mutez.ofFloat(0.0)));
              }));
        Jest.test("1", (function (param) {
                return Jest.Expect.toEqual({
                            TAG: /* Ok */0,
                            _0: Tezos_Mutez.one
                          }, Jest.Expect.expect(Tezos_Mutez.ofFloat(1.0)));
              }));
        Jest.test("1.25", (function (param) {
                return Jest.Expect.toEqual({
                            TAG: /* Ok */0,
                            _0: Tezos_Mutez.one
                          }, Jest.Expect.expect(Tezos_Mutez.ofFloat(1.25)));
              }));
        Jest.test("max_float", (function (param) {
                return Jest.Expect.toEqual(Tezos_Mutez.ofString("9223372036854775807"), Jest.Expect.expect(Tezos_Mutez.ofFloat(Pervasives.max_float)));
              }));
        return Jest.test("-1", (function (param) {
                      return Jest.Expect.toEqual({
                                  TAG: /* Error */1,
                                  _0: "ofFloat: Expected non-negative float."
                                }, Jest.Expect.expect(Tezos_Mutez.ofFloat(-1.0)));
                    }));
      }));

Jest.describe("ofString", (function (param) {
        Jest.test("\"0\"", (function (param) {
                return Jest.Expect.toEqual({
                            TAG: /* Ok */0,
                            _0: Tezos_Mutez.zero
                          }, Jest.Expect.expect(Tezos_Mutez.ofString("0")));
              }));
        Jest.test("\"1\"", (function (param) {
                return Jest.Expect.toEqual({
                            TAG: /* Ok */0,
                            _0: Tezos_Mutez.one
                          }, Jest.Expect.expect(Tezos_Mutez.ofString("1")));
              }));
        Jest.test("\"1000000\"", (function (param) {
                return Jest.Expect.toEqual({
                            TAG: /* Ok */0,
                            _0: Tezos_Mutez.oneTez
                          }, Jest.Expect.expect(Tezos_Mutez.ofString("1000000")));
              }));
        Jest.test("\"-1\"", (function (param) {
                return Jest.Expect.toEqual({
                            TAG: /* Error */1,
                            _0: "ofString: Expected a non-negative int64."
                          }, Jest.Expect.expect(Tezos_Mutez.ofString("-1")));
              }));
        return Jest.test("\"abc\"", (function (param) {
                      return Jest.Expect.toEqual({
                                  TAG: /* Error */1,
                                  _0: "ofString: Expected a non-negative int64 encoded as a string."
                                }, Jest.Expect.expect(Tezos_Mutez.ofString("abc")));
                    }));
      }));

Jest.describe("ofTezString", (function (param) {
        Jest.test("\"0\"", (function (param) {
                return Jest.Expect.toEqual({
                            TAG: /* Ok */0,
                            _0: Tezos_Mutez.zero
                          }, Jest.Expect.expect(Tezos_Mutez.ofTezString("0")));
              }));
        Jest.test("\"0.\"", (function (param) {
                return Jest.Expect.toEqual({
                            TAG: /* Ok */0,
                            _0: Tezos_Mutez.zero
                          }, Jest.Expect.expect(Tezos_Mutez.ofTezString("0.")));
              }));
        Jest.test("\"0.0\"", (function (param) {
                return Jest.Expect.toEqual({
                            TAG: /* Ok */0,
                            _0: Tezos_Mutez.zero
                          }, Jest.Expect.expect(Tezos_Mutez.ofTezString("0.0")));
              }));
        Jest.test("\"1.1\"", (function (param) {
                return Jest.Expect.toEqual(Tezos_Mutez.ofInt64(Caml_int64.mk(1100000, 0)), Jest.Expect.expect(Tezos_Mutez.ofTezString("1.1")));
              }));
        Jest.test("\"0.000001\"", (function (param) {
                return Jest.Expect.toEqual(Tezos_Mutez.ofInt64(Caml_int64.one), Jest.Expect.expect(Tezos_Mutez.ofTezString("0.000001")));
              }));
        Jest.test("\"1.12\"", (function (param) {
                return Jest.Expect.toEqual(Tezos_Mutez.ofInt64(Caml_int64.mk(1120000, 0)), Jest.Expect.expect(Tezos_Mutez.ofTezString("1.12")));
              }));
        Jest.test("\"1.123456\"", (function (param) {
                return Jest.Expect.toEqual(Tezos_Mutez.ofInt64(Caml_int64.mk(1123456, 0)), Jest.Expect.expect(Tezos_Mutez.ofTezString("1.123456")));
              }));
        Jest.test("\"12\"", (function (param) {
                return Jest.Expect.toEqual(Tezos_Mutez.ofInt64(Caml_int64.mk(12000000, 0)), Jest.Expect.expect(Tezos_Mutez.ofTezString("12")));
              }));
        Jest.test("\"23\"", (function (param) {
                return Jest.Expect.toEqual(Tezos_Mutez.ofInt64(Caml_int64.mk(23000000, 0)), Jest.Expect.expect(Tezos_Mutez.ofTezString("23")));
              }));
        Jest.test("\"123.145\"", (function (param) {
                return Jest.Expect.toEqual(Tezos_Mutez.ofInt64(Caml_int64.mk(123145000, 0)), Jest.Expect.expect(Tezos_Mutez.ofTezString("123.145")));
              }));
        Jest.test("\"0.0000001\"", (function (param) {
                return Jest.Expect.toEqual(true, Jest.Expect.expect(Belt_Result.isError(Tezos_Mutez.ofTezString("0.0000001"))));
              }));
        Jest.test("\"0.0000000\"", (function (param) {
                return Jest.Expect.toEqual(true, Jest.Expect.expect(Belt_Result.isError(Tezos_Mutez.ofTezString("0.0000000"))));
              }));
        Jest.test("\"0.00000001\"", (function (param) {
                return Jest.Expect.toEqual(true, Jest.Expect.expect(Belt_Result.isError(Tezos_Mutez.ofTezString("0.00000001"))));
              }));
        Jest.test("\"abc\"", (function (param) {
                return Jest.Expect.toEqual(true, Jest.Expect.expect(Belt_Result.isError(Tezos_Mutez.ofTezString("abc"))));
              }));
        return Jest.test("\"-1.0\"", (function (param) {
                      return Jest.Expect.toEqual(true, Jest.Expect.expect(Belt_Result.isError(Tezos_Mutez.ofTezString("-1.0"))));
                    }));
      }));

Jest.describe("toInt64", (function (param) {
        Jest.test("0", (function (param) {
                return Jest.Expect.toEqual(Caml_int64.zero, Jest.Expect.expect(Tezos_Mutez.toInt64(Tezos_Mutez.zero)));
              }));
        Jest.test("1", (function (param) {
                return Jest.Expect.toEqual(Caml_int64.one, Jest.Expect.expect(Tezos_Mutez.toInt64(Tezos_Mutez.one)));
              }));
        Jest.test("1.000000", (function (param) {
                return Jest.Expect.toEqual(Caml_int64.mk(1000000, 0), Jest.Expect.expect(Tezos_Mutez.toInt64(Tezos_Mutez.oneTez)));
              }));
        return Jest.test("45.670012", (function (param) {
                      return Jest.Expect.toEqual({
                                  TAG: /* Ok */0,
                                  _0: Caml_int64.mk(45670012, 0)
                                }, Jest.Expect.expect(Belt_Result.map(Tezos_Mutez.ofTezString("45.670012"), Tezos_Mutez.toInt64)));
                    }));
      }));

Jest.describe("toFloat", (function (param) {
        Jest.test("0", (function (param) {
                return Jest.Expect.toEqual(0.0, Jest.Expect.expect(Tezos_Mutez.toFloat(Tezos_Mutez.zero)));
              }));
        Jest.test("1", (function (param) {
                return Jest.Expect.toEqual(1.0, Jest.Expect.expect(Tezos_Mutez.toFloat(Tezos_Mutez.one)));
              }));
        Jest.test("1.000000", (function (param) {
                return Jest.Expect.toEqual(1000000.0, Jest.Expect.expect(Tezos_Mutez.toFloat(Tezos_Mutez.oneTez)));
              }));
        return Jest.test("45.670012", (function (param) {
                      return Jest.Expect.toEqual({
                                  TAG: /* Ok */0,
                                  _0: 45670012.0
                                }, Jest.Expect.expect(Belt_Result.map(Tezos_Mutez.ofTezString("45.670012"), Tezos_Mutez.toFloat)));
                    }));
      }));

Jest.describe("toTezFloat", (function (param) {
        Jest.test("0", (function (param) {
                return Jest.Expect.toEqual(0.0, Jest.Expect.expect(Tezos_Mutez.toTezFloat(Tezos_Mutez.zero)));
              }));
        Jest.test("1", (function (param) {
                return Jest.Expect.toEqual(0.000001, Jest.Expect.expect(Tezos_Mutez.toTezFloat(Tezos_Mutez.one)));
              }));
        Jest.test("1.000000", (function (param) {
                return Jest.Expect.toEqual(1.000000, Jest.Expect.expect(Tezos_Mutez.toTezFloat(Tezos_Mutez.oneTez)));
              }));
        return Jest.test("45.670012", (function (param) {
                      return Jest.Expect.toEqual({
                                  TAG: /* Ok */0,
                                  _0: 45.670012
                                }, Jest.Expect.expect(Belt_Result.map(Tezos_Mutez.ofTezString("45.670012"), Tezos_Mutez.toTezFloat)));
                    }));
      }));

Jest.describe("toString", (function (param) {
        Jest.test("0", (function (param) {
                return Jest.Expect.toEqual("0", Jest.Expect.expect(Tezos_Mutez.toString(Tezos_Mutez.zero)));
              }));
        Jest.test("1", (function (param) {
                return Jest.Expect.toEqual("1", Jest.Expect.expect(Tezos_Mutez.toString(Tezos_Mutez.one)));
              }));
        Jest.test("1.000000", (function (param) {
                return Jest.Expect.toEqual("1000000", Jest.Expect.expect(Tezos_Mutez.toString(Tezos_Mutez.oneTez)));
              }));
        return Jest.test("45.670012", (function (param) {
                      return Jest.Expect.toEqual({
                                  TAG: /* Ok */0,
                                  _0: "45670012"
                                }, Jest.Expect.expect(Belt_Result.map(Tezos_Mutez.ofTezString("45.670012"), Tezos_Mutez.toString)));
                    }));
      }));

Jest.describe("toTezString", (function (param) {
        Jest.test("0", (function (param) {
                return Jest.Expect.toEqual("0", Jest.Expect.expect(Tezos_Mutez.toTezString(Tezos_Mutez.zero)));
              }));
        Jest.test("1", (function (param) {
                return Jest.Expect.toEqual("0.000001", Jest.Expect.expect(Tezos_Mutez.toTezString(Tezos_Mutez.one)));
              }));
        Jest.test("1.000000", (function (param) {
                return Jest.Expect.toEqual("1", Jest.Expect.expect(Tezos_Mutez.toTezString(Tezos_Mutez.oneTez)));
              }));
        return Jest.test("45.670012", (function (param) {
                      return Jest.Expect.toEqual({
                                  TAG: /* Ok */0,
                                  _0: "45.670012"
                                }, Jest.Expect.expect(Belt_Result.map(Tezos_Mutez.ofTezString("45.670012"), Tezos_Mutez.toTezString)));
                    }));
      }));

Jest.describe("toTezStringWithCommas", (function (param) {
        Jest.test("0", (function (param) {
                return Jest.Expect.toEqual("0", Jest.Expect.expect(Tezos_Mutez.toTezStringWithCommas(Tezos_Mutez.zero)));
              }));
        Jest.test("1", (function (param) {
                return Jest.Expect.toEqual("0.000001", Jest.Expect.expect(Tezos_Mutez.toTezStringWithCommas(Tezos_Mutez.one)));
              }));
        Jest.test("1.000000", (function (param) {
                return Jest.Expect.toEqual("1", Jest.Expect.expect(Tezos_Mutez.toTezStringWithCommas(Tezos_Mutez.oneTez)));
              }));
        Jest.test("45.670012", (function (param) {
                return Jest.Expect.toEqual({
                            TAG: /* Ok */0,
                            _0: "45.670012"
                          }, Jest.Expect.expect(Belt_Result.map(Tezos_Mutez.ofTezString("45.670012"), Tezos_Mutez.toTezStringWithCommas)));
              }));
        Jest.test("1334.6", (function (param) {
                return Jest.Expect.toEqual({
                            TAG: /* Ok */0,
                            _0: "1,334.6"
                          }, Jest.Expect.expect(Belt_Result.map(Tezos_Mutez.ofTezString("1334.6"), Tezos_Mutez.toTezStringWithCommas)));
              }));
        return Jest.test("3991334.683928", (function (param) {
                      return Jest.Expect.toEqual({
                                  TAG: /* Ok */0,
                                  _0: "3,991,334.683928"
                                }, Jest.Expect.expect(Belt_Result.map(Tezos_Mutez.ofTezString("3991334.683928"), Tezos_Mutez.toTezStringWithCommas)));
                    }));
      }));

Jest.describe("add", (function (param) {
        Jest.test("0 + 0", (function (param) {
                return Jest.Expect.toEqual(Tezos_Mutez.zero, Jest.Expect.expect(Tezos_Mutez.add(Tezos_Mutez.zero, Tezos_Mutez.zero)));
              }));
        Jest.test("0 + 1", (function (param) {
                return Jest.Expect.toEqual(Tezos_Mutez.one, Jest.Expect.expect(Tezos_Mutez.add(Tezos_Mutez.zero, Tezos_Mutez.one)));
              }));
        Jest.test("1 + 0", (function (param) {
                return Jest.Expect.toEqual(Tezos_Mutez.one, Jest.Expect.expect(Tezos_Mutez.add(Tezos_Mutez.one, Tezos_Mutez.zero)));
              }));
        return Jest.test("1234567 + 234345", (function (param) {
                      return Jest.Expect.toEqual(ofStringUnsafe("1468912"), Jest.Expect.expect(Tezos_Mutez.add(ofStringUnsafe("1234567"), ofStringUnsafe("234345"))));
                    }));
      }));

Jest.describe("sub", (function (param) {
        Jest.test("0 - 0", (function (param) {
                return Jest.Expect.toEqual(Tezos_Mutez.zero, Jest.Expect.expect(Tezos_Mutez.sub(Tezos_Mutez.zero, Tezos_Mutez.zero)));
              }));
        Jest.test("0 - 1", (function (param) {
                return Jest.Expect.toEqual(undefined, Jest.Expect.expect(Tezos_Mutez.sub(Tezos_Mutez.zero, Tezos_Mutez.one)));
              }));
        Jest.test("1 - 0", (function (param) {
                return Jest.Expect.toEqual(Tezos_Mutez.one, Jest.Expect.expect(Tezos_Mutez.sub(Tezos_Mutez.one, Tezos_Mutez.zero)));
              }));
        return Jest.test("2343450 - 1234567", (function (param) {
                      return Jest.Expect.toEqual(ofStringUnsafe("1108883"), Jest.Expect.expect(Tezos_Mutez.sub(ofStringUnsafe("2343450"), ofStringUnsafe("1234567"))));
                    }));
      }));

Jest.describe("mul", (function (param) {
        Jest.test("0 * 0", (function (param) {
                return Jest.Expect.toEqual(Tezos_Mutez.zero, Jest.Expect.expect(Tezos_Mutez.mul(Tezos_Mutez.zero, Tezos_Mutez.zero)));
              }));
        Jest.test("0 * 1", (function (param) {
                return Jest.Expect.toEqual(Tezos_Mutez.zero, Jest.Expect.expect(Tezos_Mutez.mul(Tezos_Mutez.zero, Tezos_Mutez.one)));
              }));
        Jest.test("1 * 0", (function (param) {
                return Jest.Expect.toEqual(Tezos_Mutez.zero, Jest.Expect.expect(Tezos_Mutez.mul(Tezos_Mutez.one, Tezos_Mutez.zero)));
              }));
        Jest.test("1 * 1", (function (param) {
                return Jest.Expect.toEqual(Tezos_Mutez.one, Jest.Expect.expect(Tezos_Mutez.mul(Tezos_Mutez.one, Tezos_Mutez.one)));
              }));
        Jest.test("4 * 5", (function (param) {
                return Jest.Expect.toEqual(ofStringUnsafe("20"), Jest.Expect.expect(Tezos_Mutez.mul(ofStringUnsafe("4"), ofStringUnsafe("5"))));
              }));
        Jest.test("9223372036854775807 * 0", (function (param) {
                return Jest.Expect.toEqual(Tezos_Mutez.zero, Jest.Expect.expect(Tezos_Mutez.mul(Tezos_Mutez.maxBound, Tezos_Mutez.zero)));
              }));
        Jest.test("9223372036854775807 * 2", (function (param) {
                return Jest.Expect.toEqual(undefined, Jest.Expect.expect(Tezos_Mutez.mul(ofStringUnsafe("9223372036854775807"), ofStringUnsafe("2"))));
              }));
        Jest.test("9223372036854775807 * 3", (function (param) {
                return Jest.Expect.toEqual(undefined, Jest.Expect.expect(Tezos_Mutez.mul(Tezos_Mutez.maxBound, ofStringUnsafe("3"))));
              }));
        return Jest.test("9223372036854775807 * 2", (function (param) {
                      return Jest.Expect.toEqual(undefined, Jest.Expect.expect(Tezos_Mutez.mul(ofStringUnsafe("9223372036854775807"), ofStringUnsafe("2"))));
                    }));
      }));

Jest.describe("div", (function (param) {
        Jest.test("3 / 2", (function (param) {
                return Jest.Expect.toEqual(Tezos_Mutez.one, Jest.Expect.expect(Tezos_Mutez.div(ofStringUnsafe("3"), ofStringUnsafe("2"))));
              }));
        Jest.test("3 / 0", (function (param) {
                return Jest.Expect.toEqual(undefined, Jest.Expect.expect(Tezos_Mutez.div(ofStringUnsafe("3"), ofStringUnsafe("0"))));
              }));
        return Jest.test("10 / 5", (function (param) {
                      return Jest.Expect.toEqual(ofStringUnsafe("2"), Jest.Expect.expect(Tezos_Mutez.div(ofStringUnsafe("10"), ofStringUnsafe("5"))));
                    }));
      }));

Jest.describe("addUnsafe", (function (param) {
        Jest.test("max + 0", (function (param) {
                return Jest.Expect.toEqual(Tezos_Mutez.maxBound, Jest.Expect.expect(Tezos_Mutez.addUnsafe(Tezos_Mutez.maxBound, Tezos_Mutez.zero)));
              }));
        return Jest.test("max + 1", (function (param) {
                      return Jest.Expect.toThrow(Jest.Expect.expectFn((function (param) {
                                        return Tezos_Mutez.addUnsafe(Tezos_Mutez.maxBound, param);
                                      }), Tezos_Mutez.one));
                    }));
      }));

Jest.describe("subUnsafe", (function (param) {
        return Jest.test("minBound - 1", (function (param) {
                      return Jest.Expect.toThrow(Jest.Expect.expectFn((function (param) {
                                        return Tezos_Mutez.subUnsafe(Tezos_Mutez.minBound, param);
                                      }), Tezos_Mutez.one));
                    }));
      }));

Jest.describe("mulUnsafe", (function (param) {
        Jest.test("maxBound * 2", (function (param) {
                return Jest.Expect.toThrow(Jest.Expect.expectFn((function (param) {
                                  return Tezos_Mutez.mulUnsafe(Tezos_Mutez.maxBound, param);
                                }), ofStringUnsafe("2")));
              }));
        Jest.test("maxBound * 3", (function (param) {
                return Jest.Expect.toThrow(Jest.Expect.expectFn((function (param) {
                                  return Tezos_Mutez.mulUnsafe(Tezos_Mutez.maxBound, param);
                                }), ofStringUnsafe("3")));
              }));
        return Jest.test("maxBound * 4", (function (param) {
                      return Jest.Expect.toThrow(Jest.Expect.expectFn((function (param) {
                                        return Tezos_Mutez.mulUnsafe(Tezos_Mutez.maxBound, param);
                                      }), ofStringUnsafe("4")));
                    }));
      }));

Jest.describe("compare", (function (param) {
        Jest.test("(0,0)", (function (param) {
                return Jest.Expect.toEqual(0, Jest.Expect.expect(Tezos_Mutez.compare(Tezos_Mutez.zero, Tezos_Mutez.zero)));
              }));
        Jest.test("(0,1)", (function (param) {
                return Jest.Expect.toEqual(-1, Jest.Expect.expect(Tezos_Mutez.compare(Tezos_Mutez.zero, Tezos_Mutez.one)));
              }));
        return Jest.test("(1,0)", (function (param) {
                      return Jest.Expect.toEqual(1, Jest.Expect.expect(Tezos_Mutez.compare(Tezos_Mutez.one, Tezos_Mutez.zero)));
                    }));
      }));

Jest.describe("equal", (function (param) {
        Jest.test("(0,0)", (function (param) {
                return Jest.Expect.toEqual(true, Jest.Expect.expect(Tezos_Mutez.equal(Tezos_Mutez.zero, Tezos_Mutez.zero)));
              }));
        Jest.test("(0,1)", (function (param) {
                return Jest.Expect.toEqual(false, Jest.Expect.expect(Tezos_Mutez.equal(Tezos_Mutez.zero, Tezos_Mutez.one)));
              }));
        Jest.test("(1,0)", (function (param) {
                return Jest.Expect.toEqual(false, Jest.Expect.expect(Tezos_Mutez.equal(Tezos_Mutez.one, Tezos_Mutez.zero)));
              }));
        Jest.test("(4,10)", (function (param) {
                return Jest.Expect.toEqual(false, Jest.Expect.expect(Tezos_Mutez.equal(ofStringUnsafe("4"), ofStringUnsafe("10"))));
              }));
        Jest.test("(13,27)", (function (param) {
                return Jest.Expect.toEqual(false, Jest.Expect.expect(Tezos_Mutez.equal(ofStringUnsafe("13"), ofStringUnsafe("27"))));
              }));
        return Jest.test("(56,56)", (function (param) {
                      return Jest.Expect.toEqual(true, Jest.Expect.expect(Tezos_Mutez.equal(ofStringUnsafe("56"), ofStringUnsafe("56"))));
                    }));
      }));

Jest.describe("leq", (function (param) {
        Jest.test("(0,0)", (function (param) {
                return Jest.Expect.toEqual(true, Jest.Expect.expect(Tezos_Mutez.leq(Tezos_Mutez.zero, Tezos_Mutez.zero)));
              }));
        Jest.test("(0,1)", (function (param) {
                return Jest.Expect.toEqual(true, Jest.Expect.expect(Tezos_Mutez.leq(Tezos_Mutez.zero, Tezos_Mutez.one)));
              }));
        Jest.test("(1,0)", (function (param) {
                return Jest.Expect.toEqual(false, Jest.Expect.expect(Tezos_Mutez.leq(Tezos_Mutez.one, Tezos_Mutez.zero)));
              }));
        Jest.test("(4,10)", (function (param) {
                return Jest.Expect.toEqual(true, Jest.Expect.expect(Tezos_Mutez.leq(ofStringUnsafe("4"), ofStringUnsafe("10"))));
              }));
        Jest.test("(13,27)", (function (param) {
                return Jest.Expect.toEqual(true, Jest.Expect.expect(Tezos_Mutez.leq(ofStringUnsafe("13"), ofStringUnsafe("27"))));
              }));
        Jest.test("(56,56)", (function (param) {
                return Jest.Expect.toEqual(true, Jest.Expect.expect(Tezos_Mutez.leq(ofStringUnsafe("56"), ofStringUnsafe("56"))));
              }));
        return Jest.test("(123,245)", (function (param) {
                      return Jest.Expect.toEqual(true, Jest.Expect.expect(Tezos_Mutez.leq(ofStringUnsafe("123"), ofStringUnsafe("245"))));
                    }));
      }));

Jest.describe("geq", (function (param) {
        Jest.test("(0,0)", (function (param) {
                return Jest.Expect.toEqual(true, Jest.Expect.expect(Tezos_Mutez.geq(Tezos_Mutez.zero, Tezos_Mutez.zero)));
              }));
        Jest.test("(0,1)", (function (param) {
                return Jest.Expect.toEqual(false, Jest.Expect.expect(Tezos_Mutez.geq(Tezos_Mutez.zero, Tezos_Mutez.one)));
              }));
        Jest.test("(1,0)", (function (param) {
                return Jest.Expect.toEqual(true, Jest.Expect.expect(Tezos_Mutez.geq(Tezos_Mutez.one, Tezos_Mutez.zero)));
              }));
        Jest.test("(4,10)", (function (param) {
                return Jest.Expect.toEqual(false, Jest.Expect.expect(Tezos_Mutez.geq(ofStringUnsafe("4"), ofStringUnsafe("10"))));
              }));
        Jest.test("(13,27)", (function (param) {
                return Jest.Expect.toEqual(false, Jest.Expect.expect(Tezos_Mutez.geq(ofStringUnsafe("13"), ofStringUnsafe("27"))));
              }));
        Jest.test("(56,56)", (function (param) {
                return Jest.Expect.toEqual(true, Jest.Expect.expect(Tezos_Mutez.geq(ofStringUnsafe("56"), ofStringUnsafe("56"))));
              }));
        Jest.test("(123,245)", (function (param) {
                return Jest.Expect.toEqual(false, Jest.Expect.expect(Tezos_Mutez.geq(ofStringUnsafe("123"), ofStringUnsafe("245"))));
              }));
        return Jest.test("(245,123)", (function (param) {
                      return Jest.Expect.toEqual(true, Jest.Expect.expect(Tezos_Mutez.geq(ofStringUnsafe("245"), ofStringUnsafe("123"))));
                    }));
      }));

Jest.describe("gt", (function (param) {
        Jest.test("(0,0)", (function (param) {
                return Jest.Expect.toEqual(false, Jest.Expect.expect(Tezos_Mutez.gt(Tezos_Mutez.zero, Tezos_Mutez.zero)));
              }));
        Jest.test("(0,1)", (function (param) {
                return Jest.Expect.toEqual(false, Jest.Expect.expect(Tezos_Mutez.gt(Tezos_Mutez.zero, Tezos_Mutez.one)));
              }));
        Jest.test("(1,0)", (function (param) {
                return Jest.Expect.toEqual(true, Jest.Expect.expect(Tezos_Mutez.gt(Tezos_Mutez.one, Tezos_Mutez.zero)));
              }));
        Jest.test("(4,10)", (function (param) {
                return Jest.Expect.toEqual(false, Jest.Expect.expect(Tezos_Mutez.gt(ofStringUnsafe("4"), ofStringUnsafe("10"))));
              }));
        Jest.test("(13,27)", (function (param) {
                return Jest.Expect.toEqual(false, Jest.Expect.expect(Tezos_Mutez.gt(ofStringUnsafe("13"), ofStringUnsafe("27"))));
              }));
        Jest.test("(56,56)", (function (param) {
                return Jest.Expect.toEqual(false, Jest.Expect.expect(Tezos_Mutez.gt(ofStringUnsafe("56"), ofStringUnsafe("56"))));
              }));
        Jest.test("(123,245)", (function (param) {
                return Jest.Expect.toEqual(false, Jest.Expect.expect(Tezos_Mutez.gt(ofStringUnsafe("123"), ofStringUnsafe("245"))));
              }));
        return Jest.test("(245,123)", (function (param) {
                      return Jest.Expect.toEqual(true, Jest.Expect.expect(Tezos_Mutez.gt(ofStringUnsafe("245"), ofStringUnsafe("123"))));
                    }));
      }));

Jest.describe("lt", (function (param) {
        Jest.test("(0,0)", (function (param) {
                return Jest.Expect.toEqual(false, Jest.Expect.expect(Tezos_Mutez.lt(Tezos_Mutez.zero, Tezos_Mutez.zero)));
              }));
        Jest.test("(0,1)", (function (param) {
                return Jest.Expect.toEqual(true, Jest.Expect.expect(Tezos_Mutez.lt(Tezos_Mutez.zero, Tezos_Mutez.one)));
              }));
        Jest.test("(1,0)", (function (param) {
                return Jest.Expect.toEqual(false, Jest.Expect.expect(Tezos_Mutez.lt(Tezos_Mutez.one, Tezos_Mutez.zero)));
              }));
        Jest.test("(4,10)", (function (param) {
                return Jest.Expect.toEqual(true, Jest.Expect.expect(Tezos_Mutez.lt(ofStringUnsafe("4"), ofStringUnsafe("10"))));
              }));
        Jest.test("(13,27)", (function (param) {
                return Jest.Expect.toEqual(true, Jest.Expect.expect(Tezos_Mutez.lt(ofStringUnsafe("13"), ofStringUnsafe("27"))));
              }));
        Jest.test("(56,56)", (function (param) {
                return Jest.Expect.toEqual(false, Jest.Expect.expect(Tezos_Mutez.lt(ofStringUnsafe("56"), ofStringUnsafe("56"))));
              }));
        Jest.test("(123,245)", (function (param) {
                return Jest.Expect.toEqual(true, Jest.Expect.expect(Tezos_Mutez.lt(ofStringUnsafe("123"), ofStringUnsafe("245"))));
              }));
        return Jest.test("(245,123)", (function (param) {
                      return Jest.Expect.toEqual(false, Jest.Expect.expect(Tezos_Mutez.lt(ofStringUnsafe("245"), ofStringUnsafe("123"))));
                    }));
      }));

Jest.describe("min", (function (param) {
        Jest.test("(0,0)", (function (param) {
                return Jest.Expect.toEqual(Tezos_Mutez.zero, Jest.Expect.expect(Tezos_Mutez.min(Tezos_Mutez.zero, Tezos_Mutez.zero)));
              }));
        Jest.test("(0,1)", (function (param) {
                return Jest.Expect.toEqual(Tezos_Mutez.zero, Jest.Expect.expect(Tezos_Mutez.min(Tezos_Mutez.zero, Tezos_Mutez.one)));
              }));
        Jest.test("(1,0)", (function (param) {
                return Jest.Expect.toEqual(Tezos_Mutez.zero, Jest.Expect.expect(Tezos_Mutez.min(Tezos_Mutez.one, Tezos_Mutez.zero)));
              }));
        Jest.test("(4,10)", (function (param) {
                return Jest.Expect.toEqual(ofStringUnsafe("4"), Jest.Expect.expect(Tezos_Mutez.min(ofStringUnsafe("4"), ofStringUnsafe("10"))));
              }));
        Jest.test("(13,27)", (function (param) {
                return Jest.Expect.toEqual(ofStringUnsafe("13"), Jest.Expect.expect(Tezos_Mutez.min(ofStringUnsafe("13"), ofStringUnsafe("27"))));
              }));
        Jest.test("(56,56)", (function (param) {
                return Jest.Expect.toEqual(ofStringUnsafe("56"), Jest.Expect.expect(Tezos_Mutez.min(ofStringUnsafe("56"), ofStringUnsafe("56"))));
              }));
        Jest.test("(123,245)", (function (param) {
                return Jest.Expect.toEqual(ofStringUnsafe("123"), Jest.Expect.expect(Tezos_Mutez.min(ofStringUnsafe("123"), ofStringUnsafe("245"))));
              }));
        return Jest.test("(245,123)", (function (param) {
                      return Jest.Expect.toEqual(ofStringUnsafe("123"), Jest.Expect.expect(Tezos_Mutez.min(ofStringUnsafe("245"), ofStringUnsafe("123"))));
                    }));
      }));

Jest.describe("max", (function (param) {
        Jest.test("(0,0)", (function (param) {
                return Jest.Expect.toEqual(Tezos_Mutez.zero, Jest.Expect.expect(Tezos_Mutez.max(Tezos_Mutez.zero, Tezos_Mutez.zero)));
              }));
        Jest.test("(0,1)", (function (param) {
                return Jest.Expect.toEqual(Tezos_Mutez.one, Jest.Expect.expect(Tezos_Mutez.max(Tezos_Mutez.zero, Tezos_Mutez.one)));
              }));
        Jest.test("(1,0)", (function (param) {
                return Jest.Expect.toEqual(Tezos_Mutez.one, Jest.Expect.expect(Tezos_Mutez.max(Tezos_Mutez.one, Tezos_Mutez.zero)));
              }));
        Jest.test("(4,10)", (function (param) {
                return Jest.Expect.toEqual(ofStringUnsafe("10"), Jest.Expect.expect(Tezos_Mutez.max(ofStringUnsafe("4"), ofStringUnsafe("10"))));
              }));
        Jest.test("(13,27)", (function (param) {
                return Jest.Expect.toEqual(ofStringUnsafe("27"), Jest.Expect.expect(Tezos_Mutez.max(ofStringUnsafe("13"), ofStringUnsafe("27"))));
              }));
        Jest.test("(56,56)", (function (param) {
                return Jest.Expect.toEqual(ofStringUnsafe("56"), Jest.Expect.expect(Tezos_Mutez.max(ofStringUnsafe("56"), ofStringUnsafe("56"))));
              }));
        Jest.test("(123,245)", (function (param) {
                return Jest.Expect.toEqual(ofStringUnsafe("245"), Jest.Expect.expect(Tezos_Mutez.max(ofStringUnsafe("123"), ofStringUnsafe("245"))));
              }));
        return Jest.test("(245,123)", (function (param) {
                      return Jest.Expect.toEqual(ofStringUnsafe("245"), Jest.Expect.expect(Tezos_Mutez.max(ofStringUnsafe("245"), ofStringUnsafe("123"))));
                    }));
      }));

Jest.describe("encode", (function (param) {
        Jest.test("0", (function (param) {
                return Jest.Expect.toEqual("0", Jest.Expect.expect(Tezos_Mutez.encode(Tezos_Mutez.zero)));
              }));
        Jest.test("1", (function (param) {
                return Jest.Expect.toEqual("1", Jest.Expect.expect(Tezos_Mutez.encode(Tezos_Mutez.one)));
              }));
        return Jest.test("123", (function (param) {
                      return Jest.Expect.toEqual("123", Jest.Expect.expect(Tezos_Mutez.encode(ofStringUnsafe("123"))));
                    }));
      }));

Jest.describe("decode", (function (param) {
        Jest.test("0", (function (param) {
                return Jest.Expect.toEqual({
                            TAG: /* Ok */0,
                            _0: Tezos_Mutez.zero
                          }, Jest.Expect.expect(Tezos_Mutez.decode("0")));
              }));
        Jest.test("1", (function (param) {
                return Jest.Expect.toEqual({
                            TAG: /* Ok */0,
                            _0: Tezos_Mutez.one
                          }, Jest.Expect.expect(Tezos_Mutez.decode("1")));
              }));
        Jest.test("23", (function (param) {
                return Jest.Expect.toEqual({
                            TAG: /* Ok */0,
                            _0: ofStringUnsafe("23")
                          }, Jest.Expect.expect(Tezos_Mutez.decode("23")));
              }));
        return Jest.test("abc", (function (param) {
                      return Jest.Expect.toEqual({
                                  TAG: /* Error */1,
                                  _0: "Mutez.decode failed: abc"
                                }, Jest.Expect.expect(Tezos_Mutez.decode("abc")));
                    }));
      }));

exports.TestError = TestError;
exports.ofStringUnsafe = ofStringUnsafe;
/*  Not a pure module */
