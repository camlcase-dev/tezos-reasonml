'use strict';

var Fs = require("fs");
var Jest = require("@glennsl/bs-jest/lib/js/src/jest.js");
var List = require("bs-platform/lib/js/list.js");
var Tezos_Util = require("../../src/Tezos_Util.js");
var Json_decode = require("@glennsl/bs-json/lib/js/src/Json_decode.js");
var Tezos_BalanceUpdate = require("../../src/Tezos_BalanceUpdate.js");

Jest.describe("Tezos_BalanceUpdate.decode", (function (param) {
        return Jest.test("golden", (function (param) {
                      var json = JSON.parse(Fs.readFileSync("__tests__/golden/balance_updates.json", "utf8"));
                      return Jest.Expect.toEqual(2, Jest.Expect.expect(List.length(Json_decode.list((function (a) {
                                                return Tezos_Util.unwrapResult(Tezos_BalanceUpdate.decode(a));
                                              }), json))));
                    }));
      }));

/*  Not a pure module */
