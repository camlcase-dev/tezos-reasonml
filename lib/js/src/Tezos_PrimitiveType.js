'use strict';

var Json_decode = require("@glennsl/bs-json/lib/js/src/Json_decode.js");
var Caml_js_exceptions = require("bs-platform/lib/js/caml_js_exceptions.js");

function encode(primitiveType) {
  switch (primitiveType) {
    case /* Timestamp */0 :
        return "timestamp";
    case /* Signature */1 :
        return "signature";
    case /* Set */2 :
        return "set";
    case /* Pair */3 :
        return "pair";
    case /* Bytes */4 :
        return "bytes";
    case /* Address */5 :
        return "address";
    case /* Or */6 :
        return "or";
    case /* List */7 :
        return "list";
    case /* Storage */8 :
        return "storage";
    case /* KeyHash */9 :
        return "key_hash";
    case /* Unit */10 :
        return "unit";
    case /* Option */11 :
        return "option";
    case /* BigMap */12 :
        return "big_map";
    case /* String */13 :
        return "string";
    case /* Mutez */14 :
        return "mutez";
    case /* Bool */15 :
        return "bool";
    case /* Operation */16 :
        return "operation";
    case /* Contract */17 :
        return "contract";
    case /* Map */18 :
        return "map";
    case /* Nat */19 :
        return "nat";
    case /* Key */20 :
        return "key";
    case /* Lambda */21 :
        return "lambda";
    case /* Int */22 :
        return "int";
    case /* Parameter */23 :
        return "parameter";
    case /* Code */24 :
        return "code";
    
  }
}

function decode(json) {
  var str;
  try {
    str = Json_decode.string(json);
  }
  catch (raw_error){
    var error = Caml_js_exceptions.internalToOCamlException(raw_error);
    if (error.RE_EXN_ID === Json_decode.DecodeError) {
      return {
              TAG: /* Error */1,
              _0: "PrimitiveType.decode: " + error._1
            };
    }
    throw error;
  }
  switch (str) {
    case "address" :
        return {
                TAG: /* Ok */0,
                _0: /* Address */5
              };
    case "big_map" :
        return {
                TAG: /* Ok */0,
                _0: /* BigMap */12
              };
    case "bool" :
        return {
                TAG: /* Ok */0,
                _0: /* Bool */15
              };
    case "bytes" :
        return {
                TAG: /* Ok */0,
                _0: /* Bytes */4
              };
    case "code" :
        return {
                TAG: /* Ok */0,
                _0: /* Code */24
              };
    case "contract" :
        return {
                TAG: /* Ok */0,
                _0: /* Contract */17
              };
    case "int" :
        return {
                TAG: /* Ok */0,
                _0: /* Int */22
              };
    case "key" :
        return {
                TAG: /* Ok */0,
                _0: /* Key */20
              };
    case "key_hash" :
        return {
                TAG: /* Ok */0,
                _0: /* KeyHash */9
              };
    case "lambda" :
        return {
                TAG: /* Ok */0,
                _0: /* Lambda */21
              };
    case "list" :
        return {
                TAG: /* Ok */0,
                _0: /* List */7
              };
    case "map" :
        return {
                TAG: /* Ok */0,
                _0: /* Map */18
              };
    case "mutez" :
        return {
                TAG: /* Ok */0,
                _0: /* Mutez */14
              };
    case "nat" :
        return {
                TAG: /* Ok */0,
                _0: /* Nat */19
              };
    case "operation" :
        return {
                TAG: /* Ok */0,
                _0: /* Operation */16
              };
    case "option" :
        return {
                TAG: /* Ok */0,
                _0: /* Option */11
              };
    case "or" :
        return {
                TAG: /* Ok */0,
                _0: /* Or */6
              };
    case "pair" :
        return {
                TAG: /* Ok */0,
                _0: /* Pair */3
              };
    case "parameter" :
        return {
                TAG: /* Ok */0,
                _0: /* Parameter */23
              };
    case "set" :
        return {
                TAG: /* Ok */0,
                _0: /* Set */2
              };
    case "signature" :
        return {
                TAG: /* Ok */0,
                _0: /* Signature */1
              };
    case "storage" :
        return {
                TAG: /* Ok */0,
                _0: /* Storage */8
              };
    case "string" :
        return {
                TAG: /* Ok */0,
                _0: /* String */13
              };
    case "timestamp" :
        return {
                TAG: /* Ok */0,
                _0: /* Timestamp */0
              };
    case "unit" :
        return {
                TAG: /* Ok */0,
                _0: /* Unit */10
              };
    default:
      return {
              TAG: /* Error */1,
              _0: "PrimitiveType.decode: " + str
            };
  }
}

exports.encode = encode;
exports.decode = decode;
/* No side effect */
