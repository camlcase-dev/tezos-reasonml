type t =
  | BigMapId(int);

/**
 * conversions
 */

let ofInt = i => BigMapId(i);

let ofString = (string: string): Belt.Result.t(t, string) =>
  switch (Belt.Int.fromString(string)) {
  | Some(i) => Belt.Result.Ok(BigMapId(i))
  | None => Belt.Result.Error("Tezos_BigMapId error: " ++ string)
  };

let toInt = t =>
  switch (t) {
  | BigMapId(i) => i
  };

let toString = (t: t): string =>
  switch (t) {
  | BigMapId(i) => string_of_int(i)
  };

/**
 * JSON encoding and decoding
 */

let encode = (t: t): Js.Json.t =>
  switch (t) {
  | BigMapId(i) => Json.Encode.int(i)
  };

let decode = (json: Js.Json.t): Belt.Result.t(t, string) =>
  switch (Json.Decode.int(json)) {
  | v => Belt.Result.Ok(BigMapId(v))
  | exception (Json.Decode.DecodeError(error)) =>
    Belt.Result.Error("Tezos_BigMapId.decode failed: " ++ error)
  };

/**
 * Comparable module
 */

type comparableType = t;

module Comparable =
  Belt.Id.MakeComparable({
    type t = comparableType;
    let cmp = (c0, c1) =>
      switch (c0, c1) {
      | (BigMapId(int0), BigMapId(int1)) => Pervasives.compare(int0, int1)
      };
  });
