'use strict';

var Tezos_Util = require("./Tezos_Util.js");
var Json_decode = require("@glennsl/bs-json/lib/js/src/Json_decode.js");
var Tezos_ChainId = require("./Tezos_ChainId.js");
var Tezos_BlockHash = require("./Tezos_BlockHash.js");
var Caml_js_exceptions = require("bs-platform/lib/js/caml_js_exceptions.js");
var Tezos_OperationHash = require("./Tezos_OperationHash.js");
var Tezos_OperationContents = require("./Tezos_OperationContents.js");
var Tezos_OperationContentsAndResult = require("./Tezos_OperationContentsAndResult.js");

function decode(json) {
  var o = Tezos_OperationContentsAndResult.decode(json);
  if (!o.TAG) {
    return {
            TAG: /* Ok */0,
            _0: {
              TAG: /* ContentsAndResults */0,
              _0: o._0
            }
          };
  }
  var o$1 = Tezos_OperationContents.decode(json);
  if (o$1.TAG) {
    return {
            TAG: /* Error */1,
            _0: "Unable to decode operation"
          };
  } else {
    return {
            TAG: /* Ok */0,
            _0: {
              TAG: /* Contents */1,
              _0: o$1._0
            }
          };
  }
}

var Content = {
  decode: decode
};

function decode$1(json) {
  var val;
  var val$1;
  var val$2;
  var val$3;
  try {
    val = Json_decode.field("chain_id", (function (a) {
            return Tezos_Util.unwrapResult(Tezos_ChainId.decode(a));
          }), json);
    val$1 = Json_decode.field("hash", (function (a) {
            return Tezos_Util.unwrapResult(Tezos_OperationHash.decode(a));
          }), json);
    val$2 = Json_decode.field("branch", (function (a) {
            return Tezos_Util.unwrapResult(Tezos_BlockHash.decode(a));
          }), json);
    val$3 = Json_decode.field("contents", (function (param) {
            return Json_decode.list((function (a) {
                          return Tezos_Util.unwrapResult(decode(a));
                        }), param);
          }), json);
  }
  catch (raw_error){
    var error = Caml_js_exceptions.internalToOCamlException(raw_error);
    if (error.RE_EXN_ID === Json_decode.DecodeError) {
      return {
              TAG: /* Error */1,
              _0: error._1
            };
    }
    throw error;
  }
  return {
          TAG: /* Ok */0,
          _0: {
            chainId: val,
            hash: val$1,
            branch: val$2,
            contents: val$3
          }
        };
}

exports.Content = Content;
exports.decode = decode$1;
/* No side effect */
