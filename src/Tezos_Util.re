/**
 * turn a string into a list of characters
 */
let explode = s => List.init(String.length(s), String.get(s));

/**
 * determine if a character is a digit
 */
let isDigit = (char): bool => {
  let c = Char.code(char);
  c >= 48 && c <= 57;
};

/**
 * determine if a string only contains chars 0-9
 * or if it begins with - and is non-zero in length
 */
let isStringOfDigits = s =>
  if (String.length(s) === 0) {
    false;
  } else {
    let exploded = explode(s);
    if (List.hd(exploded) == '-') {
      if (String.length(s) > 1) {
        List.fold_right(
          (c, x) => isDigit(c) && x,
          List.tl(exploded),
          true,
        );
      } else {
        false;
      };
    } else {
      List.fold_right((c, x) => isDigit(c) && x, exploded, true);
    };
  };

/**
 * convert a string to a Bigint in safe way
 */
let bigintOfString = (t: string): option(Bigint.t) =>
  if (isStringOfDigits(t)) {
    Some(Bigint.of_string(t));
  } else {
    None;
  };

let bigintEncode = t => Json.Encode.string(Bigint.to_string(t));

let bigintDecode = json =>
  switch (Json.Decode.string(json)) {
  | v =>
    switch (bigintOfString(v)) {
    | Some(v) => Belt.Result.Ok(v)
    | None => Belt.Result.Error("bigintDecode failed: " ++ v)
    }
  | exception (Json.Decode.DecodeError(error)) =>
    Belt.Result.Error("bigintDecode failed: " ++ error)
  };

let unwrapResult = r =>
  switch (r) {
  | Belt.Result.Ok(v) => v
  | Belt.Result.Error(message) => raise @@ Json.Decode.DecodeError(message)
  };

let floatOfString = (x: string): option(float) =>
  try(Some(float_of_string(x))) {
  | Failure(_) => None
  };

let int64OfString = (x: string): option(Int64.t) =>
  try(Some(Int64.of_string(x))) {
  | Failure(_) => None
  };

let decodeInt64String = json =>
  if (Js.typeof(json) == "string") {
    let source: string = Obj.magic(json: Js.Json.t);

    if (isStringOfDigits(source)) {
      Int64.of_string(source);
    } else {
      raise @@ Json.Decode.DecodeError("Expected int64, got " ++ source);
    };
  } else {
    raise @@
    Json.Decode.DecodeError(
      "Expected int64, got " ++ Js.Json.stringify(json),
    );
  };

let explode = s => {
  let rec exp = (i, l) =>
    if (i < 0) {
      l;
    } else {
      exp(i - 1, [s.[i], ...l]);
    };
  exp(String.length(s) - 1, []);
};

let getDecimalsCount = (floatString: string): int => {
  let split = Js.String.split(".", floatString);
  Array.length(split) == 2 ? String.length(split[1]) : 0;
};

let removeRedundantDecimals = (floatString: string, decimals: int): string => {
  let split = Js.String.split(".", floatString);
  if (Array.length(split) == 2 && String.length(split[1]) > decimals) {
    split[0] ++ "." ++ Js.String.slice(~from=0, ~to_=decimals, split[1]);
  } else {
    floatString;
  };
};

let getPositionOfSmallestNonZero = (float: float): int => {
  let floatString = Js.Float.toString(float);
  let split = Js.String.split(".", floatString);
  if (Array.length(split) == 1) {
    0;
  } else if (Array.length(split) == 2) {
    let str = split[1];
    let chars = explode(str);
    let length = List.length(chars);
    let charsRev = List.rev(chars);

    let (i, _b) =
      List.fold_left(
        ((l, finished), char) =>
          if (finished) {
            (l, finished);
          } else if (char == '0') {
            (l - 1, false);
          } else {
            (l, true);
          },
        (length, false),
        charsRev,
      );
    i;
  } else {
    0;
  };
};

let rec repeatString = (n, s) =>
  if (n == 0) {
    "";
  } else {
    s ++ repeatString(n - 1, s);
  };

let rec pow = a =>
  fun
  | 0 => 1
  | 1 => a
  | n => {
      let b = pow(a, n / 2);
      b
      * b
      * (
        if (n mod 2 == 0) {
          1;
        } else {
          a;
        }
      );
    };

let decodeSafeList =
    (decode: Js.Json.t => Belt.Result.t('a, string), json: Js.Json.t)
    : list('a) =>
  if (Js.Array.isArray(json)) {
    let source: array(Js.Json.t) = Obj.magic(json: Js.Json.t);
    let length = Js.Array.length(source);
    let output = ref([]);
    for (i in 0 to length - 1) {
      let value = decode(Array.unsafe_get(source, i));
      switch (value) {
      | Belt.Result.Ok(value) => output := List.append(output^, [value])
      | Belt.Result.Error(_) => ()
      };
    };
    output^;
  } else {
    raise @@
    Json.Decode.DecodeError(
      "Expected array, got " ++ Js.Json.stringify(json),
    );
  };

/**
 * decode an optional field
 */

let optionalField =
    (key: string, decode: Js.Json.t => 'a, json: Js.Json.t): option('a) =>
  if (Js.typeof(json) == "object"
      && !Js.Array.isArray(json)
      && !((Obj.magic(json): Js.null('a)) === Js.null)) {
    let dict: Js.Dict.t(Js.Json.t) = Obj.magic(json: Js.Json.t);
    switch (Js.Dict.get(dict, key)) {
    | Some(value) =>
      if (value === Json_encode.null) {
        None;
      } else {
        Some(decode(value));
      }
    | None => None
    };
  } else {
    raise @@ Json.Decode.DecodeError("Expected object, got " ++ Js.Json.stringify(json));
  };
