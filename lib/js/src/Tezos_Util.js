'use strict';

var List = require("bs-platform/lib/js/list.js");
var Curry = require("bs-platform/lib/js/curry.js");
var Bigint = require("bs-zarith/lib/js/src/Bigint.js");
var Js_dict = require("bs-platform/lib/js/js_dict.js");
var Caml_array = require("bs-platform/lib/js/caml_array.js");
var Caml_format = require("bs-platform/lib/js/caml_format.js");
var Caml_option = require("bs-platform/lib/js/caml_option.js");
var Caml_string = require("bs-platform/lib/js/caml_string.js");
var Json_decode = require("@glennsl/bs-json/lib/js/src/Json_decode.js");
var Caml_js_exceptions = require("bs-platform/lib/js/caml_js_exceptions.js");

function explode(s) {
  return List.init(s.length, (function (param) {
                return Caml_string.get(s, param);
              }));
}

function isDigit($$char) {
  if ($$char >= 48) {
    return $$char <= 57;
  } else {
    return false;
  }
}

function isStringOfDigits(s) {
  if (s.length === 0) {
    return false;
  }
  var exploded = explode(s);
  if (List.hd(exploded) === /* "-" */45) {
    if (s.length > 1) {
      return List.fold_right((function (c, x) {
                    if (isDigit(c)) {
                      return x;
                    } else {
                      return false;
                    }
                  }), List.tl(exploded), true);
    } else {
      return false;
    }
  } else {
    return List.fold_right((function (c, x) {
                  if (isDigit(c)) {
                    return x;
                  } else {
                    return false;
                  }
                }), exploded, true);
  }
}

function bigintOfString(t) {
  if (isStringOfDigits(t)) {
    return Bigint.of_string(t);
  }
  
}

var bigintEncode = Bigint.to_string;

function bigintDecode(json) {
  var v;
  try {
    v = Json_decode.string(json);
  }
  catch (raw_error){
    var error = Caml_js_exceptions.internalToOCamlException(raw_error);
    if (error.RE_EXN_ID === Json_decode.DecodeError) {
      return {
              TAG: /* Error */1,
              _0: "bigintDecode failed: " + error._1
            };
    }
    throw error;
  }
  var v$1 = bigintOfString(v);
  if (v$1 !== undefined) {
    return {
            TAG: /* Ok */0,
            _0: v$1
          };
  } else {
    return {
            TAG: /* Error */1,
            _0: "bigintDecode failed: " + v
          };
  }
}

function unwrapResult(r) {
  if (!r.TAG) {
    return r._0;
  }
  throw {
        RE_EXN_ID: Json_decode.DecodeError,
        _1: r._0,
        Error: new Error()
      };
}

function floatOfString(x) {
  try {
    return Caml_format.caml_float_of_string(x);
  }
  catch (raw_exn){
    var exn = Caml_js_exceptions.internalToOCamlException(raw_exn);
    if (exn.RE_EXN_ID === "Failure") {
      return ;
    }
    throw exn;
  }
}

function int64OfString(x) {
  try {
    return Caml_format.caml_int64_of_string(x);
  }
  catch (raw_exn){
    var exn = Caml_js_exceptions.internalToOCamlException(raw_exn);
    if (exn.RE_EXN_ID === "Failure") {
      return ;
    }
    throw exn;
  }
}

function decodeInt64String(json) {
  if (typeof json === "string") {
    if (isStringOfDigits(json)) {
      return Caml_format.caml_int64_of_string(json);
    }
    throw {
          RE_EXN_ID: Json_decode.DecodeError,
          _1: "Expected int64, got " + json,
          Error: new Error()
        };
  }
  throw {
        RE_EXN_ID: Json_decode.DecodeError,
        _1: "Expected int64, got " + JSON.stringify(json),
        Error: new Error()
      };
}

function explode$1(s) {
  var _i = s.length - 1 | 0;
  var _l = /* [] */0;
  while(true) {
    var l = _l;
    var i = _i;
    if (i < 0) {
      return l;
    }
    _l = {
      hd: Caml_string.get(s, i),
      tl: l
    };
    _i = i - 1 | 0;
    continue ;
  };
}

function getDecimalsCount(floatString) {
  var split = floatString.split(".");
  if (split.length === 2) {
    return Caml_array.get(split, 1).length;
  } else {
    return 0;
  }
}

function removeRedundantDecimals(floatString, decimals) {
  var split = floatString.split(".");
  if (split.length === 2 && Caml_array.get(split, 1).length > decimals) {
    return Caml_array.get(split, 0) + ("." + Caml_array.get(split, 1).slice(0, decimals));
  } else {
    return floatString;
  }
}

function getPositionOfSmallestNonZero($$float) {
  var floatString = $$float.toString();
  var split = floatString.split(".");
  if (split.length === 1) {
    return 0;
  }
  if (split.length !== 2) {
    return 0;
  }
  var str = Caml_array.get(split, 1);
  var chars = explode$1(str);
  var length = List.length(chars);
  var charsRev = List.rev(chars);
  return List.fold_left((function (param, $$char) {
                  var finished = param[1];
                  var l = param[0];
                  if (finished) {
                    return [
                            l,
                            finished
                          ];
                  } else if ($$char === /* "0" */48) {
                    return [
                            l - 1 | 0,
                            false
                          ];
                  } else {
                    return [
                            l,
                            true
                          ];
                  }
                }), [
                length,
                false
              ], charsRev)[0];
}

function repeatString(n, s) {
  if (n === 0) {
    return "";
  } else {
    return s + repeatString(n - 1 | 0, s);
  }
}

function pow(a, n) {
  if (n === 0) {
    return 1;
  }
  if (n === 1) {
    return a;
  }
  var b = pow(a, n / 2 | 0);
  return Math.imul(Math.imul(b, b), n % 2 === 0 ? 1 : a);
}

function decodeSafeList(decode, json) {
  if (Array.isArray(json)) {
    var length = json.length;
    var output = /* [] */0;
    for(var i = 0; i < length; ++i){
      var value = Curry._1(decode, json[i]);
      if (!value.TAG) {
        output = List.append(output, {
              hd: value._0,
              tl: /* [] */0
            });
      }
      
    }
    return output;
  }
  throw {
        RE_EXN_ID: Json_decode.DecodeError,
        _1: "Expected array, got " + JSON.stringify(json),
        Error: new Error()
      };
}

function optionalField(key, decode, json) {
  if (typeof json === "object" && !Array.isArray(json) && json !== null) {
    var value = Js_dict.get(json, key);
    if (value === undefined) {
      return ;
    }
    var value$1 = Caml_option.valFromOption(value);
    if (value$1 === null) {
      return ;
    } else {
      return Caml_option.some(Curry._1(decode, value$1));
    }
  }
  throw {
        RE_EXN_ID: Json_decode.DecodeError,
        _1: "Expected object, got " + JSON.stringify(json),
        Error: new Error()
      };
}

exports.isDigit = isDigit;
exports.isStringOfDigits = isStringOfDigits;
exports.bigintOfString = bigintOfString;
exports.bigintEncode = bigintEncode;
exports.bigintDecode = bigintDecode;
exports.unwrapResult = unwrapResult;
exports.floatOfString = floatOfString;
exports.int64OfString = int64OfString;
exports.decodeInt64String = decodeInt64String;
exports.explode = explode$1;
exports.getDecimalsCount = getDecimalsCount;
exports.removeRedundantDecimals = removeRedundantDecimals;
exports.getPositionOfSmallestNonZero = getPositionOfSmallestNonZero;
exports.repeatString = repeatString;
exports.pow = pow;
exports.decodeSafeList = decodeSafeList;
exports.optionalField = optionalField;
/* No side effect */
