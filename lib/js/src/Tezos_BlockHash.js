'use strict';

var Json_decode = require("@glennsl/bs-json/lib/js/src/Json_decode.js");
var Caml_js_exceptions = require("bs-platform/lib/js/caml_js_exceptions.js");

function ofString(str) {
  return /* BlockHash */{
          _0: str
        };
}

function toString(t) {
  return t._0;
}

function encode(t) {
  return t._0;
}

function decode(json) {
  var v;
  try {
    v = Json_decode.string(json);
  }
  catch (raw_error){
    var error = Caml_js_exceptions.internalToOCamlException(raw_error);
    if (error.RE_EXN_ID === Json_decode.DecodeError) {
      return {
              TAG: /* Error */1,
              _0: "Tezos_BlockHash.decode failed: " + error._1
            };
    }
    throw error;
  }
  return {
          TAG: /* Ok */0,
          _0: /* BlockHash */{
            _0: v
          }
        };
}

exports.ofString = ofString;
exports.toString = toString;
exports.encode = encode;
exports.decode = decode;
/* No side effect */
