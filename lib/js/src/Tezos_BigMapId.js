'use strict';

var Belt_Id = require("bs-platform/lib/js/belt_Id.js");
var Belt_Int = require("bs-platform/lib/js/belt_Int.js");
var Json_decode = require("@glennsl/bs-json/lib/js/src/Json_decode.js");
var Caml_primitive = require("bs-platform/lib/js/caml_primitive.js");
var Caml_js_exceptions = require("bs-platform/lib/js/caml_js_exceptions.js");

function ofInt(i) {
  return /* BigMapId */{
          _0: i
        };
}

function ofString(string) {
  var i = Belt_Int.fromString(string);
  if (i !== undefined) {
    return {
            TAG: /* Ok */0,
            _0: /* BigMapId */{
              _0: i
            }
          };
  } else {
    return {
            TAG: /* Error */1,
            _0: "Tezos_BigMapId error: " + string
          };
  }
}

function toInt(t) {
  return t._0;
}

function toString(t) {
  return String(t._0);
}

function encode(t) {
  return t._0;
}

function decode(json) {
  var v;
  try {
    v = Json_decode.$$int(json);
  }
  catch (raw_error){
    var error = Caml_js_exceptions.internalToOCamlException(raw_error);
    if (error.RE_EXN_ID === Json_decode.DecodeError) {
      return {
              TAG: /* Error */1,
              _0: "Tezos_BigMapId.decode failed: " + error._1
            };
    }
    throw error;
  }
  return {
          TAG: /* Ok */0,
          _0: /* BigMapId */{
            _0: v
          }
        };
}

function cmp(c0, c1) {
  return Caml_primitive.caml_int_compare(c0._0, c1._0);
}

var Comparable = Belt_Id.MakeComparable({
      cmp: cmp
    });

exports.ofInt = ofInt;
exports.ofString = ofString;
exports.toInt = toInt;
exports.toString = toString;
exports.encode = encode;
exports.decode = decode;
exports.Comparable = Comparable;
/* Comparable Not a pure module */
