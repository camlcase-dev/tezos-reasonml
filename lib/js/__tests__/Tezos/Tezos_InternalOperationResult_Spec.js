'use strict';

var Fs = require("fs");
var Jest = require("@glennsl/bs-jest/lib/js/src/jest.js");
var Belt_Result = require("bs-platform/lib/js/belt_Result.js");
var Tezos_InternalOperationResult = require("../../src/Tezos_InternalOperationResult.js");

Jest.describe("Tezos_InternalOperationResult.decode", (function (param) {
        return Jest.test("golden", (function (param) {
                      var json = JSON.parse(Fs.readFileSync("__tests__/golden/internal_operation_result.json", "utf8"));
                      return Jest.Expect.toEqual(true, Jest.Expect.expect(Belt_Result.isOk(Tezos_InternalOperationResult.decode(json))));
                    }));
      }));

/*  Not a pure module */
