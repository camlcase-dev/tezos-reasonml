'use strict';

var Belt_List = require("bs-platform/lib/js/belt_List.js");
var Tezos_Util = require("./Tezos_Util.js");
var Json_decode = require("@glennsl/bs-json/lib/js/src/Json_decode.js");
var Json_encode = require("@glennsl/bs-json/lib/js/src/Json_encode.js");
var Tezos_Expression = require("./Tezos_Expression.js");
var Caml_js_exceptions = require("bs-platform/lib/js/caml_js_exceptions.js");

function encode(contract) {
  var parameter_0 = {
    TAG: /* PrimitiveType */2,
    _0: /* Parameter */23
  };
  var parameter_1 = {
    hd: contract.parameter,
    tl: /* [] */0
  };
  var parameter = {
    TAG: /* SingleExpression */4,
    _0: parameter_0,
    _1: parameter_1,
    _2: undefined
  };
  var storage_0 = {
    TAG: /* PrimitiveType */2,
    _0: /* Storage */8
  };
  var storage_1 = {
    hd: contract.storage,
    tl: /* [] */0
  };
  var storage = {
    TAG: /* SingleExpression */4,
    _0: storage_0,
    _1: storage_1,
    _2: undefined
  };
  var code_0 = {
    TAG: /* PrimitiveType */2,
    _0: /* Code */24
  };
  var code_1 = {
    hd: contract.code,
    tl: /* [] */0
  };
  var code = {
    TAG: /* SingleExpression */4,
    _0: code_0,
    _1: code_1,
    _2: undefined
  };
  return Json_encode.list(Tezos_Expression.encode, {
              hd: parameter,
              tl: {
                hd: storage,
                tl: {
                  hd: code,
                  tl: /* [] */0
                }
              }
            });
}

function isParameter(expression) {
  if (expression.TAG !== /* SingleExpression */4) {
    return false;
  }
  var prim = expression._0;
  switch (prim.TAG | 0) {
    case /* PrimitiveInstruction */0 :
    case /* PrimitiveData */1 :
        return false;
    case /* PrimitiveType */2 :
        return prim._0 === 23;
    
  }
}

function isStorage(expression) {
  if (expression.TAG !== /* SingleExpression */4) {
    return false;
  }
  var prim = expression._0;
  switch (prim.TAG | 0) {
    case /* PrimitiveInstruction */0 :
    case /* PrimitiveData */1 :
        return false;
    case /* PrimitiveType */2 :
        return prim._0 === 8;
    
  }
}

function isCode(expression) {
  if (expression.TAG !== /* SingleExpression */4) {
    return false;
  }
  var prim = expression._0;
  switch (prim.TAG | 0) {
    case /* PrimitiveInstruction */0 :
    case /* PrimitiveData */1 :
        return false;
    case /* PrimitiveType */2 :
        return prim._0 >= 24;
    
  }
}

function getFirstArg(expression) {
  if (expression.TAG !== /* SingleExpression */4) {
    return {
            TAG: /* Error */1,
            _0: "getFirstArg: expected a SingleExpression"
          };
  }
  var oArgs = expression._1;
  if (oArgs === undefined) {
    return {
            TAG: /* Error */1,
            _0: "getFirstArg: args is unexpectedly None. It should be a list of one element."
          };
  }
  var arg = Belt_List.head(oArgs);
  if (arg !== undefined) {
    return {
            TAG: /* Ok */0,
            _0: arg
          };
  } else {
    return {
            TAG: /* Error */1,
            _0: "getFirstArg: args is unexpectedly an empty list. It should have one element."
          };
  }
}

function getParameter(expression) {
  if (isParameter(expression)) {
    return getFirstArg(expression);
  } else {
    return {
            TAG: /* Error */1,
            _0: "getParameter: "
          };
  }
}

function getStorage(expression) {
  if (isStorage(expression)) {
    return getFirstArg(expression);
  } else {
    return {
            TAG: /* Error */1,
            _0: "getStorage: "
          };
  }
}

function getCode(expression) {
  if (isCode(expression)) {
    return getFirstArg(expression);
  } else {
    return {
            TAG: /* Error */1,
            _0: "getCode: "
          };
  }
}

function decode(json) {
  var val;
  try {
    val = Json_decode.tuple3((function (a) {
            return Tezos_Util.unwrapResult(Tezos_Expression.decode(a));
          }), (function (a) {
            return Tezos_Util.unwrapResult(Tezos_Expression.decode(a));
          }), (function (a) {
            return Tezos_Util.unwrapResult(Tezos_Expression.decode(a));
          }), json);
  }
  catch (raw_error){
    var error = Caml_js_exceptions.internalToOCamlException(raw_error);
    if (error.RE_EXN_ID === Json_decode.DecodeError) {
      return {
              TAG: /* Error */1,
              _0: error._1
            };
    }
    throw error;
  }
  var match = getParameter(val[0]);
  var match$1 = getStorage(val[1]);
  var match$2 = getCode(val[2]);
  if (match.TAG || match$1.TAG || match$2.TAG) {
    return {
            TAG: /* Error */1,
            _0: "unmatched code expressions"
          };
  } else {
    return {
            TAG: /* Ok */0,
            _0: {
              parameter: match._0,
              storage: match$1._0,
              code: match$2._0
            }
          };
  }
}

exports.encode = encode;
exports.isParameter = isParameter;
exports.isStorage = isStorage;
exports.isCode = isCode;
exports.getFirstArg = getFirstArg;
exports.getParameter = getParameter;
exports.getStorage = getStorage;
exports.getCode = getCode;
exports.decode = decode;
/* No side effect */
