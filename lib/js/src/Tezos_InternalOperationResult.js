'use strict';

var Tezos_Util = require("./Tezos_Util.js");
var Belt_Result = require("bs-platform/lib/js/belt_Result.js");
var Json_decode = require("@glennsl/bs-json/lib/js/src/Json_decode.js");
var Tezos_Mutez = require("./Tezos_Mutez.js");
var Tezos_Parameters = require("./Tezos_Parameters.js");
var Caml_js_exceptions = require("bs-platform/lib/js/caml_js_exceptions.js");

function decode(json) {
  var val;
  var val$1;
  var val$2;
  var val$3;
  try {
    val = Json_decode.field("source", Json_decode.string, json);
    val$1 = Json_decode.field("amount", (function (a) {
            return Tezos_Util.unwrapResult(Tezos_Mutez.decode(a));
          }), json);
    val$2 = Json_decode.field("destination", Json_decode.string, json);
    val$3 = Json_decode.optional((function (param) {
            return Json_decode.field("parameters", (function (a) {
                          return Tezos_Util.unwrapResult(Tezos_Parameters.decode(a));
                        }), param);
          }), json);
  }
  catch (raw_error){
    var error = Caml_js_exceptions.internalToOCamlException(raw_error);
    if (error.RE_EXN_ID === Json_decode.DecodeError) {
      return {
              TAG: /* Error */1,
              _0: error._1
            };
    }
    throw error;
  }
  return {
          TAG: /* Ok */0,
          _0: {
            source: val,
            amount: val$1,
            destination: val$2,
            parameters: val$3
          }
        };
}

var Transaction = {
  decode: decode
};

function decode$1(json) {
  var kind;
  try {
    kind = Json_decode.field("kind", Json_decode.string, json);
  }
  catch (raw_error){
    var error = Caml_js_exceptions.internalToOCamlException(raw_error);
    if (error.RE_EXN_ID === Json_decode.DecodeError) {
      return {
              TAG: /* Error */1,
              _0: error._1
            };
    }
    throw error;
  }
  switch (kind) {
    case "delegation" :
        return {
                TAG: /* Ok */0,
                _0: /* Delegation */2
              };
    case "origination" :
        return {
                TAG: /* Ok */0,
                _0: /* Origination */1
              };
    case "reveal" :
        return {
                TAG: /* Ok */0,
                _0: /* Reveal */0
              };
    case "transaction" :
        return Belt_Result.map(decode(json), (function (a) {
                      return /* Transaction */{
                              _0: a
                            };
                    }));
    default:
      return {
              TAG: /* Error */1,
              _0: "Unexpected kind: " + kind
            };
  }
}

exports.Transaction = Transaction;
exports.decode = decode$1;
/* No side effect */
