/**
 * $alpha.operation.alpha.contents:
 *   { /* Endorsement */
 *     "kind": "endorsement",
 *     "level": integer ∈ [-2^31-2, 2^31+2] }
 *   || { /* Seed_nonce_revelation */
 *        "kind": "seed_nonce_revelation",
 *        "level": integer ∈ [-2^31-2, 2^31+2],
 *        "nonce": /^[a-zA-Z0-9]+$/ }
 *   || { /* Double_endorsement_evidence */
 *        "kind": "double_endorsement_evidence",
 *        "op1": $alpha.inlined.endorsement,
 *        "op2": $alpha.inlined.endorsement }
 *   || { /* Double_baking_evidence */
 *        "kind": "double_baking_evidence",
 *        "bh1": $alpha.block_header.alpha.full_header,
 *        "bh2": $alpha.block_header.alpha.full_header }
 *   || { /* Activate_account */
 *        "kind": "activate_account",
 *        "pkh": $Ed25519.Public_key_hash,
 *        "secret": /^[a-zA-Z0-9]+$/ }
 *   || { /* Proposals */
 *        "kind": "proposals",
 *        "source": $Signature.Public_key_hash,
 *        "period": integer ∈ [-2^31-2, 2^31+2],
 *        "proposals": [ $Protocol_hash ... ] }
 *   || { /* Ballot */
 *        "kind": "ballot",
 *        "source": $Signature.Public_key_hash,
 *        "period": integer ∈ [-2^31-2, 2^31+2],
 *        "proposal": $Protocol_hash,
 *        "ballot": "nay" | "yay" | "pass" }
 *   || { /* Reveal */
 *        "kind": "reveal",
 *        "source": $Signature.Public_key_hash,
 *        "fee": $alpha.mutez,
 *        "counter": $positive_bignum,
 *        "gas_limit": $positive_bignum,
 *        "storage_limit": $positive_bignum,
 *        "public_key": $Signature.Public_key }
 *   || { /* Transaction */
 *        "kind": "transaction",
 *        "source": $Signature.Public_key_hash,
 *        "fee": $alpha.mutez,
 *        "counter": $positive_bignum,
 *        "gas_limit": $positive_bignum,
 *        "storage_limit": $positive_bignum,
 *        "amount": $alpha.mutez,
 *        "destination": $alpha.contract_id,
 *        "parameters"?:
 *          { "entrypoint": $alpha.entrypoint,
 *            "value": $micheline.alpha.michelson_v1.expression } }
 *   || { /* Origination */
 *        "kind": "origination",
 *        "source": $Signature.Public_key_hash,
 *        "fee": $alpha.mutez,
 *        "counter": $positive_bignum,
 *        "gas_limit": $positive_bignum,
 *        "storage_limit": $positive_bignum,
 *        "balance": $alpha.mutez,
 *        "delegate"?: $Signature.Public_key_hash,
 *        "script": $alpha.scripted.contracts }
 *   || { /* Delegation */
 *        "kind": "delegation",
 *        "source": $Signature.Public_key_hash,
 *        "fee": $alpha.mutez,
 *        "counter": $positive_bignum,
 *        "gas_limit": $positive_bignum,
 *        "storage_limit": $positive_bignum,
 *        "delegate"?: $Signature.Public_key_hash }
 */
module Transaction = {
  type t = {
    source: string,
    fee: Tezos_Mutez.t,
    counter: Bigint.t,
    gasLimit: Bigint.t,
    storageLimit: Bigint.t,
    amount: Tezos_Mutez.t,
    destination: string, /* contract id */
    parameters: option(Tezos_Parameters.t),
  };

  let decode = (json: Js.Json.t) => {
    switch (
      Json.Decode.(
        field("source", string, json),
        field(
          "fee",
          a => Tezos_Util.unwrapResult(Tezos_Mutez.decode(a)),
          json,
        ),
        field(
          "counter",
          a => Tezos_Util.unwrapResult(Tezos_Util.bigintDecode(a)),
          json,
        ),
        field(
          "gas_limit",
          a => Tezos_Util.unwrapResult(Tezos_Util.bigintDecode(a)),
          json,
        ),
        field(
          "storage_limit",
          a => Tezos_Util.unwrapResult(Tezos_Util.bigintDecode(a)),
          json,
        ),
        field(
          "amount",
          a => Tezos_Util.unwrapResult(Tezos_Mutez.decode(a)),
          json,
        ),
        field("destination", string, json),
        optional(
          field("parameters", a =>
            Tezos_Util.unwrapResult(Tezos_Parameters.decode(a))
          ),
          json,
        ),
      )
    ) {
    | (
        source,
        fee,
        counter,
        gasLimit,
        storageLimit,
        amount,
        destination,
        parameters,
      ) =>
      Belt.Result.Ok({
        source,
        fee,
        counter,
        gasLimit,
        storageLimit,
        amount,
        destination,
        parameters,
      })
    | exception (Json.Decode.DecodeError(error)) => Belt.Result.Error(error)
    };
  };
};

type t =
  | Endorsement
  | SeedNonceRevelation
  | DoubleEndorsementEvidence
  | DoubleBakingEvidence
  | ActivateAccount
  | Proposals
  | Ballot
  | Reveal
  | Transaction(Transaction.t)
  | Origination
  | Delegation;

let decode = (json: Js.Json.t): Belt.Result.t(t, string) => {
  Json.Decode.(
    switch (field("kind", string, json)) {
    | "endorsement" => Belt.Result.Ok(Endorsement)

    | "seed_nonce_revelation" => Belt.Result.Ok(SeedNonceRevelation)

    | "double_endorsement_evidence" =>
      Belt.Result.Ok(DoubleEndorsementEvidence)

    | "double_baking_evidence" => Belt.Result.Ok(DoubleBakingEvidence)
    | "activate_account" => Belt.Result.Ok(ActivateAccount)
    | "proposals" => Belt.Result.Ok(Proposals)
    | "ballot" => Belt.Result.Ok(Ballot)

    | "reveal" => Belt.Result.Ok(Reveal)
    | "transaction" =>
      Belt.Result.map(Transaction.decode(json), a => Transaction(a))
    | "origination" => Belt.Result.Ok(Origination)
    | "delegation" => Belt.Result.Ok(Delegation)
    | kind => Belt.Result.Error("Unexpected kind: " ++ kind)
    | exception (DecodeError(error)) => Belt.Result.Error(error)
    }
  );
};
