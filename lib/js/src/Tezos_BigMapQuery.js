'use strict';

var Tezos_Util = require("./Tezos_Util.js");
var Json_decode = require("@glennsl/bs-json/lib/js/src/Json_decode.js");
var Json_encode = require("@glennsl/bs-json/lib/js/src/Json_encode.js");
var Tezos_Expression = require("./Tezos_Expression.js");
var Caml_js_exceptions = require("bs-platform/lib/js/caml_js_exceptions.js");

function encode(query) {
  return Json_encode.object_({
              hd: [
                "key",
                Tezos_Expression.encode(query.key)
              ],
              tl: {
                hd: [
                  "type",
                  Tezos_Expression.encode(query.type_)
                ],
                tl: /* [] */0
              }
            });
}

function decode(json) {
  var v;
  try {
    v = {
      key: Tezos_Util.unwrapResult(Json_decode.field("key", Tezos_Expression.decode, json)),
      type_: Tezos_Util.unwrapResult(Json_decode.field("type", Tezos_Expression.decode, json))
    };
  }
  catch (raw_err){
    var err = Caml_js_exceptions.internalToOCamlException(raw_err);
    if (err.RE_EXN_ID === Json_decode.DecodeError) {
      return {
              TAG: /* Error */1,
              _0: err._1
            };
    }
    throw err;
  }
  return {
          TAG: /* Ok */0,
          _0: v
        };
}

exports.encode = encode;
exports.decode = decode;
/* No side effect */
