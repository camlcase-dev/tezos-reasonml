'use strict';

var List = require("bs-platform/lib/js/list.js");
var Bigint = require("bs-zarith/lib/js/src/Bigint.js");
var Tezos_Util = require("./Tezos_Util.js");
var Json_decode = require("@glennsl/bs-json/lib/js/src/Json_decode.js");
var Json_encode = require("@glennsl/bs-json/lib/js/src/Json_encode.js");
var Tezos_Primitives = require("./Tezos_Primitives.js");
var Caml_js_exceptions = require("bs-platform/lib/js/caml_js_exceptions.js");

function expressions(x) {
  return {
          TAG: /* Expressions */3,
          _0: x
        };
}

function singleExpression(prim, args, annots, param) {
  return {
          TAG: /* SingleExpression */4,
          _0: prim,
          _1: args,
          _2: annots
        };
}

function getIntExpression(t) {
  if (t.TAG) {
    return ;
  } else {
    return t._0;
  }
}

function getStringExpression(t) {
  if (t.TAG === /* StringExpression */1) {
    return t._0;
  }
  
}

function encodeRec(expression) {
  switch (expression.TAG | 0) {
    case /* IntExpression */0 :
        return Json_encode.object_({
                    hd: [
                      "int",
                      Bigint.to_string(expression._0)
                    ],
                    tl: /* [] */0
                  });
    case /* StringExpression */1 :
        return Json_encode.object_({
                    hd: [
                      "string",
                      expression._0
                    ],
                    tl: /* [] */0
                  });
    case /* BytesExpression */2 :
        return Json_encode.object_({
                    hd: [
                      "bytes",
                      expression._0
                    ],
                    tl: /* [] */0
                  });
    case /* Expressions */3 :
        return Json_encode.list(encodeRec, expression._0);
    case /* SingleExpression */4 :
        var annots = expression._2;
        var args = expression._1;
        var argsEncoded = args !== undefined ? ({
              hd: [
                "args",
                Json_encode.list(encodeRec, args)
              ],
              tl: /* [] */0
            }) : /* [] */0;
        var annotsEncoded = annots !== undefined ? ({
              hd: [
                "annots",
                Json_encode.list((function (prim) {
                        return prim;
                      }), annots)
              ],
              tl: /* [] */0
            }) : /* [] */0;
        return Json_encode.object_(List.concat({
                        hd: {
                          hd: [
                            "prim",
                            Tezos_Primitives.encode(expression._0)
                          ],
                          tl: /* [] */0
                        },
                        tl: {
                          hd: argsEncoded,
                          tl: {
                            hd: annotsEncoded,
                            tl: /* [] */0
                          }
                        }
                      }));
    
  }
}

var encode = encodeRec;

function decodeRec(json) {
  var v;
  try {
    v = Json_decode.list((function (a) {
            return Tezos_Util.unwrapResult(decodeRec(a));
          }), json);
  }
  catch (raw__error){
    var _error = Caml_js_exceptions.internalToOCamlException(raw__error);
    if (_error.RE_EXN_ID === Json_decode.DecodeError) {
      var exit = 0;
      var v$1;
      try {
        v$1 = Json_decode.field("int", Json_decode.string, json);
        exit = 2;
      }
      catch (raw__error$1){
        var _error$1 = Caml_js_exceptions.internalToOCamlException(raw__error$1);
        if (_error$1.RE_EXN_ID === Json_decode.DecodeError) {
          var exit$1 = 0;
          var v$2;
          try {
            v$2 = Json_decode.field("string", Json_decode.string, json);
            exit$1 = 3;
          }
          catch (raw__error$2){
            var _error$2 = Caml_js_exceptions.internalToOCamlException(raw__error$2);
            if (_error$2.RE_EXN_ID === Json_decode.DecodeError) {
              var exit$2 = 0;
              var v$3;
              try {
                v$3 = Json_decode.field("bytes", Json_decode.string, json);
                exit$2 = 4;
              }
              catch (raw__error$3){
                var _error$3 = Caml_js_exceptions.internalToOCamlException(raw__error$3);
                if (_error$3.RE_EXN_ID === Json_decode.DecodeError) {
                  var exit$3 = 0;
                  var val;
                  var val$1;
                  var val$2;
                  try {
                    val = Json_decode.field("prim", (function (a) {
                            return Tezos_Util.unwrapResult(Tezos_Primitives.decode(a));
                          }), json);
                    val$1 = Json_decode.optional((function (param) {
                            return Json_decode.field("args", (function (param) {
                                          return Json_decode.list((function (a) {
                                                        return Tezos_Util.unwrapResult(decodeRec(a));
                                                      }), param);
                                        }), param);
                          }), json);
                    val$2 = Json_decode.optional((function (param) {
                            return Json_decode.field("annots", (function (param) {
                                          return Json_decode.list(Json_decode.string, param);
                                        }), param);
                          }), json);
                    exit$3 = 5;
                  }
                  catch (raw_error){
                    var error = Caml_js_exceptions.internalToOCamlException(raw_error);
                    if (error.RE_EXN_ID === Json_decode.DecodeError) {
                      return {
                              TAG: /* Error */1,
                              _0: error._1
                            };
                    }
                    throw error;
                  }
                  if (exit$3 === 5) {
                    return {
                            TAG: /* Ok */0,
                            _0: {
                              TAG: /* SingleExpression */4,
                              _0: val,
                              _1: val$1,
                              _2: val$2
                            }
                          };
                  }
                  
                } else {
                  throw _error$3;
                }
              }
              if (exit$2 === 4) {
                return {
                        TAG: /* Ok */0,
                        _0: {
                          TAG: /* BytesExpression */2,
                          _0: v$3
                        }
                      };
              }
              
            } else {
              throw _error$2;
            }
          }
          if (exit$1 === 3) {
            return {
                    TAG: /* Ok */0,
                    _0: {
                      TAG: /* StringExpression */1,
                      _0: v$2
                    }
                  };
          }
          
        } else {
          throw _error$1;
        }
      }
      if (exit === 2) {
        var i = Tezos_Util.bigintOfString(v$1);
        if (i !== undefined) {
          return {
                  TAG: /* Ok */0,
                  _0: {
                    TAG: /* IntExpression */0,
                    _0: i
                  }
                };
        } else {
          return {
                  TAG: /* Error */1,
                  _0: "Expected string encoded int."
                };
        }
      }
      
    } else {
      throw _error;
    }
  }
  return {
          TAG: /* Ok */0,
          _0: {
            TAG: /* Expressions */3,
            _0: v
          }
        };
}

var decode = decodeRec;

exports.expressions = expressions;
exports.singleExpression = singleExpression;
exports.getIntExpression = getIntExpression;
exports.getStringExpression = getStringExpression;
exports.encodeRec = encodeRec;
exports.encode = encode;
exports.decodeRec = decodeRec;
exports.decode = decode;
/* No side effect */
