# tezos-reasonml

tezos-reasonml is BuckleScript/ReasonML/ReScript library for interacting with the 
[Tezos RPC](https://tezos.gitlab.io/api/rpc.html). It provides a set of safe 
types and functions over the values that can be sentand received from the Tezos 
RPC. This project is intended as a core layer over the Tezos RPC that can be 
used to build larger projects like dApps, wallets, Tezos bots, etc. The goal 
is to keep the scope focused on the RPC and its values without making it too large.

Currently this library is a work in progress and is open to suggestions around 
the architecture. It will be used in camlCase's [dexter-frontend](https://gitlab.com/camlcase-dev/dexter-frontend) 
project (progress can be seen in this [branch](https://gitlab.com/camlcase-dev/dexter-frontend/-/tree/add-tezos-reasonml)).

## Type safety

A central theme in [Michelson](https://tezos.gitlab.io/whitedoc/michelson.html),
the Tezos smart contract language, is compile time type safety and safe type 
conversion. tezos-reasonml emulates these ideas in a useable format.

For example, `Tezos_Contract.t` is a contstructor around a string, but it 
but not any string is a valid `Tezos_Contract.t`. A string that is a valid
`Tezos_Contract.t` should have a particular length and format. To convert
from a string, tezos-reasonml provides a function `Tezos_Contract.ofString` that
does the appropriate checks and returns `Belt.Result.t(t, string)`. 

This model of value checking is applied to all types from the Tezos RPC and 
Michelson which are bounded within the possible values of number and string 
literals. This includes `Tezos_Mutez`, `Tezos_ImplicitAccount`, etc.

## Features

(This is a work in progress and incomplete at the moment)

- `encode` and `decode` to and from the JSON format used by the Tezos RPC.

- `pack` and `unpack` to and from the optimized binary representation used by 
  the Tezos RPC and Michelson.

- `toScriptExpr` and `fromScriptExpr` for the URL encoding of types as expected
  by the big map query entrypoint.

- provide query functions for all of the Tezos RPC routes using `bs-fetch` and 
  the types provided by this package.

- a recursive type that represents Micheline as read from the RPC. This can be 
  used to send queries to entrypoints, pattern match on results from storage and
  big map queries, read operatoins: `Tezos_Expression`,
  `Tezos_Primitives`, `Tezos_PrimitiveInstruction`, `Tezos_PrimitiveType` and ``

## Mutez

The native currency on the Tezos blockchain is tez and the smallest unit is mutez.
One tez is equal to 1,000,000 mutez. The RPC and smart contracts only refer to 
values as mutez. Mutez can never be negative and they have a underlying 
representation of a [64-bit signed integer](https://tezos.gitlab.io/whitedoc/michelson.html#operations-on-mutez).

tezos-reasonml has safe constructors for mutez. It also has safe arithmetic functions
that return `option(t)` if there is underflow, overflow or division by zero.
There are also unsafe versions of these functions that will throw underflow, 
overflow or division by zero exceptions.
