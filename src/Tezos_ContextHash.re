/** 
 * $Context_hash:
 *  /* A hash of context (Base58Check-encoded) */
 *  $unistring
 */

type t =
  | ContextHash(string);

/**
 * JSON encoding and decoding
 */

let encode = (t: t): Js.Json.t => {
  switch (t) {
  | ContextHash(str) => Json.Encode.string(str)
  };
};

let decode = (json: Js.Json.t): Belt.Result.t(t, string) => {
  switch (Json.Decode.string(json)) {
  | v => Belt.Result.Ok(ContextHash(v))
  | exception (Json.Decode.DecodeError(error)) =>
    Belt.Result.Error("Tezos_ContextHash.decode failed: " ++ error)
  };
};
