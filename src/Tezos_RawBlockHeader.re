/**
 * $raw_block_header:
 *   { "level": integer ∈ [-2^31-2, 2^31+2],
 *     "proto": integer ∈ [0, 255],
 *     "predecessor": $block_hash,
 *     "timestamp": $timestamp.protocol,
 *     "validation_pass": integer ∈ [0, 255],
 *     "operations_hash": $Operation_list_list_hash,
 *     "fitness": $fitness,
 *     "context": $Context_hash,
 *     "priority": integer ∈ [0, 2^16-1],
 *     "proof_of_work_nonce": /^[a-zA-Z0-9]+$/,
 *     "seed_nonce_hash"?: $cycle_nonce,
 *     "signature": $Signature }
 */

type t = {
  level: int,
  proto: int,
  predecessor: Tezos_BlockHash.t,
  timestamp: Tezos_Timestamp.t,
  validationPass: int,
  operationsHash: Tezos_OperationListListHash.t,
  fitness: Tezos_Fitness.t,
  context: Tezos_ContextHash.t,
  priority: int,
  proofOfWorkNonce: string,
  seedNonceHash: option(Tezos_CycleNonce.t),
  signature: Tezos_Signature.t,
};

/**
 * JSON encoding and decoding
 */

let encode = (t: t): Js.Json.t => {
  Json.Encode.(
    object_(
      List.concat([
        [
          ("level", int(t.level)),
          ("proto", int(t.proto)),
          ("predecessor", Tezos_BlockHash.encode(t.predecessor)),
          ("timestamp", Tezos_Timestamp.encode(t.timestamp)),
          ("validation_pass", int(t.validationPass)),
          (
            "operations_hash",
            Tezos_OperationListListHash.encode(t.operationsHash),
          ),
          ("fitness", Tezos_Fitness.encode(t.fitness)),
          ("context", Tezos_ContextHash.encode(t.context)),
          ("priority", int(t.priority)),
          ("proof_of_work_nonce", Json.Encode.string(t.proofOfWorkNonce)),
          ("signature", Tezos_Signature.encode(t.signature)),
        ],
        switch (t.seedNonceHash) {
        | Some(seedNonceHash) => [
            ("seed_nonce_hash", Tezos_CycleNonce.encode(seedNonceHash)),
          ]
        | None => []
        },
      ]),
    )
  );
};

let decode = (json: Js.Json.t): Belt.Result.t(t, string) => {
  Json.Decode.(
    switch (
      field("level", int, json),
      field("proto", int, json),
      field(
        "predecessor",
        a => Tezos_Util.unwrapResult(Tezos_BlockHash.decode(a)),
        json,
      ),
      field(
        "timestamp",
        a => Tezos_Util.unwrapResult(Tezos_Timestamp.decode(a)),
        json,
      ),
      field("validation_pass", int, json),
      field(
        "operations_hash",
        a => Tezos_Util.unwrapResult(Tezos_OperationListListHash.decode(a)),
        json,
      ),
      field(
        "fitness",
        a => Tezos_Util.unwrapResult(Tezos_Fitness.decode(a)),
        json,
      ),
      field(
        "context",
        a => Tezos_Util.unwrapResult(Tezos_ContextHash.decode(a)),
        json,
      ),
      field("priority", int, json),
      field("proof_of_work_nonce", string, json),
      Tezos_Util.optionalField(
        "seed_nonce_hash",
        a => Tezos_Util.unwrapResult(Tezos_CycleNonce.decode(a)),
        json,
      ),
      field(
        "signature",
        a => Tezos_Util.unwrapResult(Tezos_Signature.decode(a)),
        json,
      ),
    ) {
    | (
        level,
        proto,
        predecessor,
        timestamp,
        validationPass,
        operationsHash,
        fitness,
        context,
        priority,
        proofOfWorkNonce,
        seedNonceHash,
        signature,
      ) =>
      Belt.Result.Ok({
        level,
        proto,
        predecessor,
        timestamp,
        validationPass,
        operationsHash,
        fitness,
        context,
        priority,
        proofOfWorkNonce,
        seedNonceHash,
        signature,
      })
    | exception (DecodeError(error)) => Belt.Result.Error(error)
    }
  );
};
