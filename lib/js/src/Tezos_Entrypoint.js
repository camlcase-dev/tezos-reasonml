'use strict';

var Json_decode = require("@glennsl/bs-json/lib/js/src/Json_decode.js");
var Caml_js_exceptions = require("bs-platform/lib/js/caml_js_exceptions.js");

function toString(t) {
  if (typeof t !== "number") {
    return t._0;
  }
  switch (t) {
    case /* Default */0 :
        return "default";
    case /* Root */1 :
        return "root";
    case /* Do */2 :
        return "do";
    case /* SetDelegate */3 :
        return "set_delegate";
    case /* RemoveDelegate */4 :
        return "remove_delegate";
    
  }
}

function encode(t) {
  if (typeof t !== "number") {
    return t._0;
  }
  switch (t) {
    case /* Default */0 :
        return "default";
    case /* Root */1 :
        return "root";
    case /* Do */2 :
        return "do";
    case /* SetDelegate */3 :
        return "set_delegate";
    case /* RemoveDelegate */4 :
        return "remove_delegate";
    
  }
}

function decode(json) {
  var str;
  try {
    str = Json_decode.string(json);
  }
  catch (raw_error){
    var error = Caml_js_exceptions.internalToOCamlException(raw_error);
    if (error.RE_EXN_ID === Json_decode.DecodeError) {
      return {
              TAG: /* Error */1,
              _0: error._1
            };
    }
    throw error;
  }
  switch (str) {
    case "default" :
        return {
                TAG: /* Ok */0,
                _0: /* Default */0
              };
    case "do" :
        return {
                TAG: /* Ok */0,
                _0: /* Do */2
              };
    case "remove_delegate" :
        return {
                TAG: /* Ok */0,
                _0: /* RemoveDelegate */4
              };
    case "root" :
        return {
                TAG: /* Ok */0,
                _0: /* Root */1
              };
    case "set_delegate" :
        return {
                TAG: /* Ok */0,
                _0: /* SetDelegate */3
              };
    default:
      return {
              TAG: /* Ok */0,
              _0: /* Named */{
                _0: str
              }
            };
  }
}

exports.toString = toString;
exports.encode = encode;
exports.decode = decode;
/* No side effect */
