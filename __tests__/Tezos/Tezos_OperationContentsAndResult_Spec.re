open Jest;
open Expect;

describe("Tezos_OperationContentsAndResult.Transaction.decode", () => {
  test("golden", () => {
    let json =
      Js.Json.parseExn(
        Node.Fs.readFileAsUtf8Sync(
          "__tests__/golden/operation_contents_and_result_transaction.json",
        ),
      );
    expect(
      Tezos_OperationContentsAndResult.Transaction.decode(json)
      |> Belt.Result.isOk,
    )
    |> toEqual(true);
  })
});

describe(
  "Tezos_Operation_Alpha.Contents.Result.Transaction.Metadata.decode", () => {
  test("golden", () => {
    let json =
      Js.Json.parseExn(
        Node.Fs.readFileAsUtf8Sync(
          "__tests__/golden/operation_contents_and_result_transaction_metadata.json",
        ),
      );
    expect(
      Tezos_OperationContentsAndResult.Transaction.Metadata.decode(json)
      |> Belt.Result.isOk,
    )
    |> toEqual(true);
  })
});
