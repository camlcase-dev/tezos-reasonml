open Jest;
open Expect;

describe("Tezos_OperationResult.Transaction.decode", () => {
  test("golden", () => {
    let json =
      Js.Json.parseExn(
        Node.Fs.readFileAsUtf8Sync(
          "__tests__/golden/operation_result_transaction.json",
        ),
      );
    expect(
      Tezos_OperationResult.Transaction.decode(json)
      |> Belt.Result.isOk,
    )
    |> toEqual(true);
  })
});
