type t = {
  key: Tezos_Expression.t,
  type_: Tezos_Expression.t,
};

let encode = (query: t) =>
  Json.Encode.(
    object_([
      ("key", Tezos_Expression.encode(query.key)),
      ("type", Tezos_Expression.encode(query.type_)),
    ])
  );

let decode = (json: Js.Json.t) : Belt.Result.t(t, string) =>
  switch (
    Json.Decode.{
      key:
        Tezos_Util.unwrapResult(field("key", Tezos_Expression.decode, json)),
      type_:
        Tezos_Util.unwrapResult(
          field("type", Tezos_Expression.decode, json),
        ),
    }
  ) {
  | v => Belt.Result.Ok(v)
  | exception (Json.Decode.DecodeError(err)) => Belt.Result.Error(err)
  };
