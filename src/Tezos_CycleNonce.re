/**
 * $cycle_nonce:
 *   /* A nonce hash (Base58Check-encoded) */
 *   $unistring
 */

type t =
  | CycleNonce(string);

/**
 * JSON encoding and decoding
 */

let encode = (t: t): Js.Json.t => {
  switch (t) {
  | CycleNonce(str) => Json.Encode.string(str)
  };
};

let decode = (json: Js.Json.t): Belt.Result.t(t, string) => {
  switch (Json.Decode.string(json)) {
  | v => Belt.Result.Ok(CycleNonce(v))
  | exception (Json.Decode.DecodeError(error)) =>
    Belt.Result.Error("Tezos_CycleNonce.decode failed: " ++ error)
  };
};
