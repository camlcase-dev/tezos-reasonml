/**
 * $alpha.michelson.v1.primitives:
 *  "ADD"
 *  | "IF_NONE"
 *  | "SWAP"
 *  | "set"
 *  | "nat"
 *  | "CHECK_SIGNATURE"
 *  | "IF_LEFT"
 *  | "LAMBDA"
 *  | "Elt"
 *  | "CREATE_CONTRACT"
 *  | "NEG"
 *  | "big_map"
 *  | "map"
 *  | "or"
 *  | "BLAKE2B"
 *  | "bytes"
 *  | "SHA256"
 *  | "SET_DELEGATE"
 *  | "CONTRACT"
 *  | "LSL"
 *  | "SUB"
 *  | "IMPLICIT_ACCOUNT"
 *  | "PACK"
 *  | "list"
 *  | "PAIR"
 *  | "Right"
 *  | "contract"
 *  | "GT"
 *  | "LEFT"
 *  | "STEPS_TO_QUOTA"
 *  | "storage"
 *  | "TRANSFER_TOKENS"
 *  | "CDR"
 *  | "SLICE"
 *  | "PUSH"
 *  | "False"
 *  | "SHA512"
 *  | "CHAIN_ID"
 *  | "BALANCE"
 *  | "signature"
 *  | "DUG"
 *  | "SELF"
 *  | "EMPTY_BIG_MAP"
 *  | "LSR"
 *  | "OR"
 *  | "XOR"
 *  | "lambda"
 *  | "COMPARE"
 *  | "key"
 *  | "option"
 *  | "Unit"
 *  | "Some"
 *  | "UNPACK"
 *  | "NEQ"
 *  | "INT"
 *  | "pair"
 *  | "AMOUNT"
 *  | "DIP"
 *  | "ABS"
 *  | "ISNAT"
 *  | "EXEC"
 *  | "NOW"
 *  | "LOOP"
 *  | "chain_id"
 *  | "string"
 *  | "MEM"
 *  | "MAP"
 *  | "None"
 *  | "address"
 *  | "CONCAT"
 *  | "EMPTY_SET"
 *  | "MUL"
 *  | "LOOP_LEFT"
 *  | "timestamp"
 *  | "LT"
 *  | "UPDATE"
 *  | "DUP"
 *  | "SOURCE"
 *  | "mutez"
 *  | "SENDER"
 *  | "IF_CONS"
 *  | "RIGHT"
 *  | "CAR"
 *  | "CONS"
 *  | "LE"
 *  | "NONE"
 *  | "IF"
 *  | "SOME"
 *  | "GET"
 *  | "Left"
 *  | "CAST"
 *  | "int"
 *  | "SIZE"
 *  | "key_hash"
 *  | "unit"
 *  | "DROP"
 *  | "EMPTY_MAP"
 *  | "NIL"
 *  | "DIG"
 *  | "APPLY"
 *  | "bool"
 *  | "RENAME"
 *  | "operation"
 *  | "True"
 *  | "FAILWITH"
 *  | "parameter"
 *  | "HASH_KEY"
 *  | "EQ"
 *  | "NOT"
 *  | "UNIT"
 *  | "Pair"
 *  | "ADDRESS"
 *  | "EDIV"
 *  | "CREATE_ACCOUNT"
 *  | "GE"
 *  | "ITER"
 *  | "code"
 *  | "AND"
 */

// all capital letters are PrimviteInstruction
// only first letter capital is PrimitiveData
// all lowercase letters are PrimitiveType

type t =
  | PrimitiveInstruction(Tezos_PrimitiveInstruction.t)
  | PrimitiveData(Tezos_PrimitiveData.t)
  | PrimitiveType(Tezos_PrimitiveType.t);

/**
 * Convenient functions
 */

let instr = x => PrimitiveInstruction(x);

let data = x => PrimitiveData(x);

let type_ = x => PrimitiveType(x);


/**
 * JSON encoding and decoding
 */

let encode = (primitives: t) : Js.Json.t =>
  switch (primitives) {
  | PrimitiveInstruction(primitiveInstruction) =>
    Tezos_PrimitiveInstruction.encode(primitiveInstruction)
  | PrimitiveData(primitiveData) => Tezos_PrimitiveData.encode(primitiveData)
  | PrimitiveType(primitiveType) => Tezos_PrimitiveType.encode(primitiveType)
  };

let decode = (json: Js.Json.t) : Belt.Result.t(t, string) =>
  switch (Tezos_PrimitiveInstruction.decode(json)) {
  | Belt.Result.Ok(primitiveInstruction) =>
    Belt.Result.Ok(PrimitiveInstruction(primitiveInstruction))
  | Belt.Result.Error(_) =>
    switch (Tezos_PrimitiveData.decode(json)) {
    | Belt.Result.Ok(primitiveData) =>
      Belt.Result.Ok(PrimitiveData(primitiveData))
    | Belt.Result.Error(_) =>
      switch (Tezos_PrimitiveType.decode(json)) {
      | Belt.Result.Ok(primitiveType) =>
        Belt.Result.Ok(PrimitiveType(primitiveType))
      | Belt.Result.Error(error) =>
        Belt.Result.Error("Primitives.decode: " ++ error)
      | exception (Json.Decode.DecodeError(error)) =>
        Belt.Result.Error("Primitives.decode: " ++ error)
      }
    | exception (Json.Decode.DecodeError(error)) =>
      Belt.Result.Error("Primitives.decode: " ++ error)
    }
  | exception (Json.Decode.DecodeError(error)) =>
    Belt.Result.Error("Primitives.decode: " ++ error)
  };
