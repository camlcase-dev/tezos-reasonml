open Jest;
open Expect;

describe("Tezos_BalanceUpdate.decode", () => {
  test("golden", () => {
    let json =
      Js.Json.parseExn(
        Node.Fs.readFileAsUtf8Sync(
          "__tests__/golden/balance_updates.json",
        ),
      );
    expect(
      Json.Decode.list(a => Tezos_Util.unwrapResult(Tezos_BalanceUpdate.decode(a)), json)
      |> List.length,
    )
    |> toEqual(2);
  })
});
