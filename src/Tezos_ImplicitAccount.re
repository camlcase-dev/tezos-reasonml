/**
 * An implicit account is a Tezos address that begins with tz1, tz2 or tz3. It
 * may only contain bs58 characters and is 36 characters long. An implicit
 * account can hold XTZ and sign operations. It does not have associated logic
 * with it (it is not a smart contract).
 */

type t =
  | ImplicitAccount(string);

/**
 * common values
 */

let zero = ImplicitAccount("tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU");

/**
 * of conversions
 */

let canDecodeBS58: string => bool = [%bs.raw
  {|
   function (value) {
     const bs58check = require('bs58check');
     if (bs58check.decodeUnsafe(value)) {
       return true;
     } else {
       return false;
     };
   }
  |}
];

let isImplicitAccountString = (candidate: string): bool => {
  (
    Js.String2.startsWith(candidate, "tz1")
    || Js.String2.startsWith(candidate, "tz2")
    || Js.String2.startsWith(candidate, "tz3")
  )
  && String.length(candidate) == 36
  && canDecodeBS58(candidate);
};

let ofString = (candidate: string): Belt.Result.t(t, string) =>
  if (isImplicitAccountString(candidate)) {
    Belt.Result.Ok(ImplicitAccount(candidate));
  } else {
    Belt.Result.Error(
      "Tezos_ImplicitAccount.ofString: unexpected candidate string: " ++ candidate,
    );
  };

/**
 * to conversions
 */

let toString = (t: t): string =>
  switch (t) {
  | ImplicitAccount(str) => str
  };

/**
 * pack and unpack
 */

let packRaw: string => string = [%bs.raw
  {|
   function (input) {
     const bs58check = require('bs58check');
     const elliptic  = require('elliptic');

     var prefix;
     var tz_prefix;
     if (input.startsWith("tz1")) { // ed25519_public_key_hash
       prefix = new Uint8Array([6, 161, 159]);
       tz_prefix = '00';
     } else if (input.startsWith("tz2")) { // secp256k1_public_key_hash
       prefix = new Uint8Array([6, 161, 161]);
       tz_prefix = '01';
     } else if (input.startsWith("tz3")) { // p256_public_key_hash
       prefix = new Uint8Array([6, 161, 164]);
       tz_prefix = '02';
     };

     const bytes = '00' + tz_prefix + elliptic.utils.toHex(bs58check.decode(input).slice(prefix.length));
     const len = bytes.length / 2;
     const result = [];
     result.push('050a');
     result.push(len.toString(16).padStart(8, '0'));
     result.push(bytes);
     return result.join('');
   }
|}
];

let pack = (t: t): string =>
  switch (t) {
  | ImplicitAccount(str) => packRaw(str)
  };

let unpackRaw: string => Js.Nullable.t(string) = [%bs.raw
  {|
   function (input) {
     const bs58check = require('bs58check');
     const elliptic  = require('elliptic');

     if (input.length != 56) {
       return;
     }

     const prefix = input.slice(14,16);
     var prefix_bytes;
     if (prefix === '00') {
       prefix_bytes = new Uint8Array([6, 161, 159]);
     } else if (prefix === '01') {
       prefix_bytes = new Uint8Array([6, 161, 161]);
     } else if (prefix === '02') {
       prefix_bytes = new Uint8Array([6, 161, 164]);
     } else {
       return;
     }

     const bytes = new Uint8Array(elliptic.utils.toArray(input.slice(16), 'hex'));

     const bytes_concated = new Uint8Array(prefix_bytes.length + bytes.length);
     bytes_concated.set(prefix_bytes, 0);
     bytes_concated.set(bytes, prefix_bytes.length)

     return bs58check.encode(Buffer.from(bytes_concated));
   }
|}
];

let unpack = (s: string): option(t) =>
  switch (Js.Nullable.toOption(unpackRaw(s))) {
  | Some(s) =>
    switch (ofString(s)) {
    | Belt.Result.Ok(s) => Some(s)
    | Belt.Result.Error(_) => None
    }
  | None => None
  };

/**
 * Tezos script expression
 */

let toScriptExprRaw: string => string = [%bs.raw
  {|
   function (input) {
     const blake    = require('blakejs');
     const elliptic = require('elliptic');
     const bs58check = require('bs58check');

     const prefix = new Uint8Array([13, 44, 64, 27]);

     var a = [];
     for (var i = 0, len = input.length; i < len; i+=2) {
       a.push(parseInt(input.substr(i,2),16));
     }

     const hex2buf =  new Uint8Array(a);

     const blakeHash = blake.blake2b(hex2buf, null, 32);

     const payloadAr = typeof blakeHash === 'string' ? Uint8Array.from(Buffer.from(blakeHash, 'hex')) : blakeHash;

     const n = new Uint8Array(prefix.length + payloadAr.length);
     n.set(prefix);
     n.set(payloadAr, prefix.length);

     return bs58check.encode(Buffer.from(n.buffer));
   }
|}
];

let toScriptExpr = (t: t): string =>
  switch (t) {
  | ImplicitAccount(str) => toScriptExprRaw(packRaw(str))
  };

/**
 * JSON encoding and decoding
 */

let encode = (t: t): Js.Json.t =>
  switch (t) {
  | ImplicitAccount(c) => Json.Encode.string(c)
  };

let decode = (json: Js.Json.t): Belt.Result.t(t, string) =>
  switch (Json.Decode.string(json)) {
  | v =>
    if (isImplicitAccountString(v)) {
      Belt.Result.Ok(ImplicitAccount(v));
    } else {
      Belt.Result.Error(
        "Tezos_ImplicitAccount.decode failed: string is not an implicit account: "
        ++ v,
      );
    }
  | exception (Json.Decode.DecodeError(error)) =>
    Belt.Result.Error("Tezos_ImplicitAccount.decode failed: " ++ error)
  };

/**
 * Comparable module
 */

type comparableType = t;

module Comparable =
  Belt.Id.MakeComparable({
    type t = comparableType;
    let cmp = (c0, c1) =>
      switch (c0, c1) {
      | (ImplicitAccount(str0), ImplicitAccount(str1)) =>
        Pervasives.compare(str0, str1)
      };
  });
