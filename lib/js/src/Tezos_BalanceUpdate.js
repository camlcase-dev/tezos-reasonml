'use strict';

var Tezos_Util = require("./Tezos_Util.js");
var Belt_Result = require("bs-platform/lib/js/belt_Result.js");
var Json_decode = require("@glennsl/bs-json/lib/js/src/Json_decode.js");
var Caml_js_exceptions = require("bs-platform/lib/js/caml_js_exceptions.js");

function decode(json) {
  var val;
  var val$1;
  try {
    val = Json_decode.field("contract", Json_decode.string, json);
    val$1 = Json_decode.field("change", Tezos_Util.decodeInt64String, json);
  }
  catch (raw_error){
    var error = Caml_js_exceptions.internalToOCamlException(raw_error);
    if (error.RE_EXN_ID === Json_decode.DecodeError) {
      return {
              TAG: /* Error */1,
              _0: error._1
            };
    }
    throw error;
  }
  return {
          TAG: /* Ok */0,
          _0: {
            contract: val,
            change: val$1
          }
        };
}

var Contract = {
  decode: decode
};

function decode$1(json) {
  var val;
  var val$1;
  var val$2;
  try {
    val = Json_decode.field("delegate", Json_decode.string, json);
    val$1 = Json_decode.field("cycle", Json_decode.$$int, json);
    val$2 = Json_decode.field("change", Tezos_Util.decodeInt64String, json);
  }
  catch (raw_error){
    var error = Caml_js_exceptions.internalToOCamlException(raw_error);
    if (error.RE_EXN_ID === Json_decode.DecodeError) {
      return {
              TAG: /* Error */1,
              _0: error._1
            };
    }
    throw error;
  }
  return {
          TAG: /* Ok */0,
          _0: {
            delegate: val,
            cycle: val$1,
            change: val$2
          }
        };
}

var Rewards = {
  decode: decode$1
};

function decode$2(json) {
  var val;
  var val$1;
  var val$2;
  try {
    val = Json_decode.field("delegate", Json_decode.string, json);
    val$1 = Json_decode.field("cycle", Json_decode.$$int, json);
    val$2 = Json_decode.field("change", Tezos_Util.decodeInt64String, json);
  }
  catch (raw_error){
    var error = Caml_js_exceptions.internalToOCamlException(raw_error);
    if (error.RE_EXN_ID === Json_decode.DecodeError) {
      return {
              TAG: /* Error */1,
              _0: error._1
            };
    }
    throw error;
  }
  return {
          TAG: /* Ok */0,
          _0: {
            delegate: val,
            cycle: val$1,
            change: val$2
          }
        };
}

var Fees = {
  decode: decode$2
};

function decode$3(json) {
  var val;
  var val$1;
  var val$2;
  try {
    val = Json_decode.field("delegate", Json_decode.string, json);
    val$1 = Json_decode.field("cycle", Json_decode.$$int, json);
    val$2 = Json_decode.field("change", Tezos_Util.decodeInt64String, json);
  }
  catch (raw_error){
    var error = Caml_js_exceptions.internalToOCamlException(raw_error);
    if (error.RE_EXN_ID === Json_decode.DecodeError) {
      return {
              TAG: /* Error */1,
              _0: error._1
            };
    }
    throw error;
  }
  return {
          TAG: /* Ok */0,
          _0: {
            delegate: val,
            cycle: val$1,
            change: val$2
          }
        };
}

var Deposits = {
  decode: decode$3
};

function decode$4(json) {
  var exit = 0;
  var kind;
  try {
    kind = Json_decode.field("kind", Json_decode.string, json);
    exit = 1;
  }
  catch (raw_error){
    var error = Caml_js_exceptions.internalToOCamlException(raw_error);
    if (error.RE_EXN_ID === Json_decode.DecodeError) {
      return {
              TAG: /* Error */1,
              _0: error._1
            };
    }
    throw error;
  }
  if (exit === 1) {
    switch (kind) {
      case "contract" :
          return Belt_Result.map(decode(json), (function (a) {
                        return {
                                TAG: /* Contract */0,
                                _0: a
                              };
                      }));
      case "freezer" :
          var exit$1 = 0;
          var category;
          try {
            category = Json_decode.field("category", Json_decode.string, json);
            exit$1 = 2;
          }
          catch (raw_error$1){
            var error$1 = Caml_js_exceptions.internalToOCamlException(raw_error$1);
            if (error$1.RE_EXN_ID === Json_decode.DecodeError) {
              return {
                      TAG: /* Error */1,
                      _0: error$1._1
                    };
            }
            throw error$1;
          }
          if (exit$1 === 2) {
            switch (category) {
              case "deposits" :
                  return Belt_Result.map(decode$3(json), (function (a) {
                                return {
                                        TAG: /* Deposits */3,
                                        _0: a
                                      };
                              }));
              case "fees" :
                  return Belt_Result.map(decode$2(json), (function (a) {
                                return {
                                        TAG: /* Fees */2,
                                        _0: a
                                      };
                              }));
              case "rewards" :
                  return Belt_Result.map(decode$1(json), (function (a) {
                                return {
                                        TAG: /* Rewards */1,
                                        _0: a
                                      };
                              }));
              default:
                return {
                        TAG: /* Error */1,
                        _0: "Unexpected category: " + category
                      };
            }
          }
          break;
      default:
        return {
                TAG: /* Error */1,
                _0: "Unexpected kind: " + kind
              };
    }
  }
  
}

exports.Contract = Contract;
exports.Rewards = Rewards;
exports.Fees = Fees;
exports.Deposits = Deposits;
exports.decode = decode$4;
/* No side effect */
