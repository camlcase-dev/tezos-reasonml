type route =
  | Balance(string, string, string)
  | BigMap(string, string, Tezos_BigMapId.t, string)
  | HeadBlockId(string)
  | Block(string, string)
  | Storage(string, string, string)
  | Delegate(string, string, string)
  | NetworkVersion;

let toRoute = (baseUrl: string, route: route) =>
  baseUrl
  ++ (
    switch (route) {
    | Balance(chainId, blockId, contract) =>
      "/chains/"
      ++ chainId
      ++ "/blocks/"
      ++ blockId
      ++ "/context/contracts/"
      ++ contract
      ++ "/balance"
    | BigMap(chainId, blockId, BigMapId(bigMapId), scriptExpr) =>
      "/chains/"
      ++ chainId
      ++ "/blocks/"
      ++ blockId
      ++ "/context/big_maps/"
      ++ string_of_int(bigMapId)
      ++ "/"
      ++ scriptExpr
    | HeadBlockId(chainId) => "/chains/" ++ chainId ++ "/blocks"
    | Block(chainId, blockId) =>
      "/chains/" ++ chainId ++ "/blocks/" ++ blockId
    | Storage(chainId, blockId, contract) =>
      "/chains/"
      ++ chainId
      ++ "/blocks/"
      ++ blockId
      ++ "/context/contracts/"
      ++ contract
      ++ "/storage"

    | Delegate(chainId, blockId, contract) =>
      "/chains/"
      ++ chainId
      ++ "/blocks/"
      ++ blockId
      ++ "/context/contracts/"
      ++ contract
      ++ "/delegate"
    | NetworkVersion => "/network/version"
    }
  );

let getBalance =
    (baseUrl, chainId, blockId, contract)
    : Js.Promise.t(Belt.Result.t(Tezos_Mutez.t, string)) => {
  let url = toRoute(baseUrl, Balance(chainId, blockId, contract));
  Js.Promise.(
    Fetch.fetchWithInit(
      url,
      Fetch.RequestInit.make(
        ~method_=Get,
        ~headers=
          Fetch.HeadersInit.makeWithArray([|
            ("Accept", "application/json"),
          |]),
        ~credentials=Omit,
        (),
      ),
    )
    |> then_(Fetch.Response.json)
    |> then_(json => resolve(Tezos_Mutez.decode(json)))
    |> catch(error => {
        Js.log(error);
         resolve(
           Belt.Result.Error("Tezos_RPC.getBalance promise error."),
         );
       })
  );
};

/**
 * Three things can occur:
 * the key has an entry in the big map, it returns the value with a 200 status
 * the key does not have an entry in the big map, it returns nothing with a 404 status
 * a network error or query (incorrect key or format) error.
 */
let getBigMapValue =
    (
      baseUrl,
      chainId,
      blockId,
      bigMapId: Tezos_BigMapId.t,
      scriptExpr: string,
    )
    : Js.Promise.t(Belt.Result.t(option(Tezos_Expression.t), string)) => {
  let url = toRoute(baseUrl, BigMap(chainId, blockId, bigMapId, scriptExpr));
  Js.Promise.(
    Fetch.fetchWithInit(
      url,
      Fetch.RequestInit.make(
        ~method_=Get,
        ~mode=CORS,
        ~headers=
          Fetch.HeadersInit.makeWithArray([|
            ("Accept", "application/json"),
          |]),
        ~credentials=Omit,
        (),
      ),
    )
    |> then_(response => {
         let status = Fetch.Response.status(response);
         if (status >= 200 && status < 300) {
           Fetch.Response.json(response)
           |> then_(json => {
                (
                  switch (Tezos_Expression.decode(json)) {
                  | Belt.Result.Ok(expression) =>
                    Belt.Result.Ok(Some(expression))
                  | Belt.Result.Error(err) => Belt.Result.Error(err)
                  }
                )
                |> resolve
              });
         } else if (status == 404) {
           resolve(Belt.Result.Ok(None));
         } else {
           resolve(
             Belt.Result.Error(
               "Tezos_RPC.getBigMapValue failed. Promise error.",
             ),
           );
         };
       })
    |> catch(error => {
         Js.Console.log(error);
         resolve(
           Belt.Result.Error(
             "Tezos_RPC.getBigMapValue failed. Promise error.",
           ),
         );
       })
  );
};

let getFirst = (xss: list(list(string))): option(string) => {
  switch (Belt.List.head(xss)) {
  | Some(xs) => Belt.List.head(xs)
  | None => None
  };
};

let getHeadBlockId =
    (baseUrl, chainId): Js.Promise.t(Belt.Result.t(string, string)) => {
  let url = toRoute(baseUrl, HeadBlockId(chainId));
  Js.Promise.(
    Fetch.fetchWithInit(
      url,
      Fetch.RequestInit.make(
        ~method_=Get,
        ~headers=
          Fetch.HeadersInit.makeWithArray([|
            ("Accept", "application/json"),
          |]),
        ~credentials=Omit,
        (),
      ),
    )
    |> then_(Fetch.Response.json)
    |> then_(json => {
         switch (Json.Decode.(list(a => list(string, a), json))) {
         | blocks =>
           {
             switch (getFirst(blocks)) {
             | Some(block) => Belt.Result.Ok(block)
             | None =>
               Belt.Result.Error(
                 "Tezos_RPC.getHeadBlockId return an empty list.",
               )
             };
           }
           |> resolve

         | exception (Json.Decode.DecodeError(err)) =>
           Belt.Result.Error(err) |> resolve
         }
       })
    |> catch(error => {
         Js.Console.log(error);
         resolve(
           Belt.Result.Error(
             "Tezos_RPC.getHeadBlockId failed. Promise error.",
           ),
         );
       })
  );
};

let getBlock =
    (baseUrl, chainId, blockId)
    : Js.Promise.t(Belt.Result.t(Tezos_Block.t, string)) => {
  let url = toRoute(baseUrl, Block(chainId, blockId));
  Js.Promise.(
    Fetch.fetchWithInit(
      url,
      Fetch.RequestInit.make(
        ~method_=Get,
        ~headers=
          Fetch.HeadersInit.makeWithArray([|
            ("Accept", "application/json"),
          |]),
        ~credentials=Omit,
        (),
      ),
    )
    |> then_(Fetch.Response.json)
    |> then_(json => Tezos_Block.decode(json) |> resolve)
    |> catch(error => {
         Js.Console.log(error);
         resolve(
           Belt.Result.Error("Tezos_RPC.getBlock failed. Promise error."),
         );
       })
  );
};

let getHeadBlock =
    (baseUrl, chainId): Js.Promise.t(Belt.Result.t(Tezos_Block.t, string)) => {
  getHeadBlockId(baseUrl, chainId)
  |> Js.Promise.then_(result =>
       switch (result) {
       | Belt.Result.Ok(blockId) => getBlock(baseUrl, chainId, blockId)
       | Belt.Result.Error(err) =>
         Belt.Result.Error(err) |> Js.Promise.resolve
       }
     );
};

let getTimestamp =
    (baseUrl): Js.Promise.t(Belt.Result.t(Tezos_Timestamp.t, string)) => {
  getHeadBlock(baseUrl, "main")
  |> Js.Promise.then_(result =>
       switch (result) {
       | Belt.Result.Ok((block: Tezos_Block.t)) =>
         Belt.Result.Ok(block.header.timestamp) |> Js.Promise.resolve
       | Belt.Result.Error(err) =>
         Belt.Result.Error(err) |> Js.Promise.resolve
       }
     );
};

let getStorage =
    (baseUrl, chainId, blockId, contract)
    : Js.Promise.t(Belt.Result.t(Tezos_Expression.t, string)) => {
  let url = toRoute(baseUrl, Storage(chainId, blockId, contract));
  Js.Promise.(
    Fetch.fetchWithInit(
      url,
      Fetch.RequestInit.make(
        ~method_=Get,
        ~headers=
          Fetch.HeadersInit.makeWithArray([|
            ("Accept", "application/json"),
          |]),
        ~credentials=Omit,
        (),
      ),
    )
    |> then_(Fetch.Response.json)
    |> then_(json => resolve(Tezos_Expression.decode(json)))
    |> catch(error => {
         Js.Console.log(error);
         resolve(
           Belt.Result.Error("Tezos_RPC.getStorage failed. Promise error."),
         );
       })
  );
};

let getDelegate =
    (baseUrl, chainId, blockId, contract)
    : Js.Promise.t(Belt.Result.t(option(Tezos_Address.t), string)) => {
  let url = toRoute(baseUrl, Delegate(chainId, blockId, contract));
  Js.Promise.(
    Fetch.fetchWithInit(
      url,
      Fetch.RequestInit.make(
        ~method_=Get,
        ~headers=
          Fetch.HeadersInit.makeWithArray([|
            ("Accept", "application/json"),
          |]),
        ~credentials=Omit,
        (),
      ),
    )
    |> then_(response => {
         let status = Fetch.Response.status(response);
         if (status == 404) {
           Belt.Result.Ok(None) |> resolve;
         } else if (status >= 200 && status < 300) {
           Fetch.Response.json(response)
           |> then_(json =>
                (
                  switch (Tezos_Address.decode(json)) {
                  | Belt.Result.Ok(ok) => Belt.Result.Ok(Some(ok))
                  | Belt.Result.Error(error) => Belt.Result.Error(error)
                  }
                )
                |> resolve
              );
         } else {
           Belt.Result.Error(
             "Tezos_RPC.getDelegate failed. Unexpected network status:  "
             ++ string_of_int(status),
           )
           |> resolve;
         };
       })
    |> catch(error => {
         Js.Console.log(error);
         resolve(
           Belt.Result.Error("Tezos_RPC.getDelegate failed. Promise error."),
         );
       })
  );
};

let getNetworkVersion = (baseUrl): Js.Promise.t(Belt.Result.t(string, string)) => {
  let url = toRoute(baseUrl, NetworkVersion);
  Js.Promise.(
    Fetch.fetchWithInit(
      url,
      Fetch.RequestInit.make(
        ~method_=Get,
        ~headers=
          Fetch.HeadersInit.makeWithArray([|
            ("Accept", "application/json"),
          |]),
        ~credentials=Omit,
        (),
      ),
    )
    |> then_(Fetch.Response.json)
    |> then_(json => {
         Json.Decode.(
           switch (field("chain_name", string, json)) {
           | chainName => Belt.Result.Ok(chainName)
           | exception (DecodeError(error)) => Belt.Result.Error(error)
           }
         ) |> resolve
       })
  );
};
