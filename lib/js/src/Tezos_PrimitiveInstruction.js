'use strict';

var Json_decode = require("@glennsl/bs-json/lib/js/src/Json_decode.js");
var Caml_js_exceptions = require("bs-platform/lib/js/caml_js_exceptions.js");

function encode(primitiveInstruction) {
  switch (primitiveInstruction) {
    case /* Add */0 :
        return "ADD";
    case /* Le */1 :
        return "LE";
    case /* Unit */2 :
        return "UNIT";
    case /* Compare */3 :
        return "COMPARE";
    case /* Lambda */4 :
        return "LAMBDA";
    case /* Loop */5 :
        return "LOOP";
    case /* ImplicitAccount */6 :
        return "IMPLICIT_ACCOUNT";
    case /* None */7 :
        return "NONE";
    case /* Blake2B */8 :
        return "BLAKE2B";
    case /* Sha256 */9 :
        return "SHA256";
    case /* Xor */10 :
        return "XOR";
    case /* Rename */11 :
        return "RENAME";
    case /* Map */12 :
        return "MAP";
    case /* SetDelegate */13 :
        return "SET_DELEGATE";
    case /* Dip */14 :
        return "DIP";
    case /* Pack */15 :
        return "PACK";
    case /* Size */16 :
        return "SIZE";
    case /* IfCons */17 :
        return "IF_CONS";
    case /* Lsr */18 :
        return "LSR";
    case /* TransferTokens */19 :
        return "TRANSFER_TOKENS";
    case /* Update */20 :
        return "UPDATE";
    case /* Cdr */21 :
        return "CDR";
    case /* Swap */22 :
        return "SWAP";
    case /* Some */23 :
        return "SOME";
    case /* Sha512 */24 :
        return "SHA512";
    case /* CheckSignature */25 :
        return "CHECK_SIGNATURE";
    case /* Balance */26 :
        return "BALANCE";
    case /* EmptySet */27 :
        return "EMPTY_SET";
    case /* Sub */28 :
        return "SUB";
    case /* Mem */29 :
        return "MEM";
    case /* Right */30 :
        return "RIGHT";
    case /* Address */31 :
        return "ADDRESS";
    case /* Concat */32 :
        return "CONTACT";
    case /* Unpack */33 :
        return "UNPACK";
    case /* Not */34 :
        return "NOT";
    case /* Left */35 :
        return "LEFT";
    case /* Amount */36 :
        return "AMOUNT";
    case /* Drop */37 :
        return "DROP";
    case /* Abs */38 :
        return "ABS";
    case /* Ge */39 :
        return "GE";
    case /* Push */40 :
        return "PUSH";
    case /* Lt */41 :
        return "LT";
    case /* Neq */42 :
        return "NEQ";
    case /* Neg */43 :
        return "NEG";
    case /* Con */44 :
        return "CON";
    case /* Exec */45 :
        return "EXEC";
    case /* Apply */46 :
        return "APPLY";
    case /* Cons */47 :
        return "CONS";
    case /* Nil */48 :
        return "NIL";
    case /* Isnat */49 :
        return "ISNAT";
    case /* Mul */50 :
        return "MUL";
    case /* LoopLeft */51 :
        return "LOOP_LEFT";
    case /* Ediv */52 :
        return "EDIV";
    case /* Slice */53 :
        return "SLICE";
    case /* StepsToQuota */54 :
        return "STEPS_TO_QUOTA";
    case /* Int */55 :
        return "INT";
    case /* Source */56 :
        return "SOURCE";
    case /* Car */57 :
        return "CAR";
    case /* CreateAccount */58 :
        return "CREATE_ACCOUNT";
    case /* Lsl */59 :
        return "LSL";
    case /* Or */60 :
        return "OR";
    case /* IfNone */61 :
        return "IF_NONE";
    case /* Self */62 :
        return "SELF";
    case /* If */63 :
        return "IF";
    case /* Sender */64 :
        return "SENDER";
    case /* Dup */65 :
        return "DUP";
    case /* Eq */66 :
        return "EQ";
    case /* Now */67 :
        return "NOW";
    case /* Get */68 :
        return "GET";
    case /* Gt */69 :
        return "GT";
    case /* IfLeft */70 :
        return "IF_LEFT";
    case /* Failwith */71 :
        return "FAILWITH";
    case /* Pair */72 :
        return "PAIR";
    case /* Iter */73 :
        return "ITER";
    case /* Cast */74 :
        return "CAST";
    case /* EmptyMap */75 :
        return "EMPTY_MAP";
    case /* CreateContract */76 :
        return "CREATE_CONTRACT";
    case /* HasKey */77 :
        return "HAS_KEY";
    case /* Contract */78 :
        return "CONTRACT";
    case /* And */79 :
        return "AND";
    
  }
}

function decode(json) {
  var str;
  try {
    str = Json_decode.string(json);
  }
  catch (raw_error){
    var error = Caml_js_exceptions.internalToOCamlException(raw_error);
    if (error.RE_EXN_ID === Json_decode.DecodeError) {
      return {
              TAG: /* Error */1,
              _0: "PrimitiveInstruction.decode: " + error._1
            };
    }
    throw error;
  }
  switch (str) {
    case "ABS" :
        return {
                TAG: /* Ok */0,
                _0: /* Abs */38
              };
    case "ADD" :
        return {
                TAG: /* Ok */0,
                _0: /* Add */0
              };
    case "ADDRESS" :
        return {
                TAG: /* Ok */0,
                _0: /* Address */31
              };
    case "AMOUNT" :
        return {
                TAG: /* Ok */0,
                _0: /* Amount */36
              };
    case "AND" :
        return {
                TAG: /* Ok */0,
                _0: /* And */79
              };
    case "APPLY" :
        return {
                TAG: /* Ok */0,
                _0: /* Apply */46
              };
    case "BALANCE" :
        return {
                TAG: /* Ok */0,
                _0: /* Balance */26
              };
    case "BLAKE2B" :
        return {
                TAG: /* Ok */0,
                _0: /* Blake2B */8
              };
    case "CAR" :
        return {
                TAG: /* Ok */0,
                _0: /* Car */57
              };
    case "CAST" :
        return {
                TAG: /* Ok */0,
                _0: /* Cast */74
              };
    case "CDR" :
        return {
                TAG: /* Ok */0,
                _0: /* Cdr */21
              };
    case "CHECK_SIGNATURE" :
        return {
                TAG: /* Ok */0,
                _0: /* CheckSignature */25
              };
    case "COMPARE" :
        return {
                TAG: /* Ok */0,
                _0: /* Compare */3
              };
    case "CON" :
        return {
                TAG: /* Ok */0,
                _0: /* Con */44
              };
    case "CONCAT" :
        return {
                TAG: /* Ok */0,
                _0: /* Concat */32
              };
    case "CONS" :
        return {
                TAG: /* Ok */0,
                _0: /* Cons */47
              };
    case "CONTRACT" :
        return {
                TAG: /* Ok */0,
                _0: /* Contract */78
              };
    case "CREATE_ACCOUNT" :
        return {
                TAG: /* Ok */0,
                _0: /* CreateAccount */58
              };
    case "CREATE_CONTRACT" :
        return {
                TAG: /* Ok */0,
                _0: /* CreateContract */76
              };
    case "DIP" :
        return {
                TAG: /* Ok */0,
                _0: /* Dip */14
              };
    case "DROP" :
        return {
                TAG: /* Ok */0,
                _0: /* Drop */37
              };
    case "DUP" :
        return {
                TAG: /* Ok */0,
                _0: /* Dup */65
              };
    case "EDIV" :
        return {
                TAG: /* Ok */0,
                _0: /* Ediv */52
              };
    case "EMPTY_MAP" :
        return {
                TAG: /* Ok */0,
                _0: /* EmptyMap */75
              };
    case "EMPTY_SET" :
        return {
                TAG: /* Ok */0,
                _0: /* EmptySet */27
              };
    case "EQ" :
        return {
                TAG: /* Ok */0,
                _0: /* Eq */66
              };
    case "EXEC" :
        return {
                TAG: /* Ok */0,
                _0: /* Exec */45
              };
    case "FAILWITH" :
        return {
                TAG: /* Ok */0,
                _0: /* Failwith */71
              };
    case "GET" :
        return {
                TAG: /* Ok */0,
                _0: /* Get */68
              };
    case "GT" :
        return {
                TAG: /* Ok */0,
                _0: /* Gt */69
              };
    case "HAS_KEY" :
        return {
                TAG: /* Ok */0,
                _0: /* HasKey */77
              };
    case "IF" :
        return {
                TAG: /* Ok */0,
                _0: /* If */63
              };
    case "IF_CONS" :
        return {
                TAG: /* Ok */0,
                _0: /* IfCons */17
              };
    case "IF_LEFT" :
        return {
                TAG: /* Ok */0,
                _0: /* IfLeft */70
              };
    case "IF_NONE" :
        return {
                TAG: /* Ok */0,
                _0: /* IfNone */61
              };
    case "IMPLICIT_ACCOUNT" :
        return {
                TAG: /* Ok */0,
                _0: /* ImplicitAccount */6
              };
    case "INT" :
        return {
                TAG: /* Ok */0,
                _0: /* Int */55
              };
    case "ISNAT" :
        return {
                TAG: /* Ok */0,
                _0: /* Isnat */49
              };
    case "ITER" :
        return {
                TAG: /* Ok */0,
                _0: /* Iter */73
              };
    case "LAMBDA" :
        return {
                TAG: /* Ok */0,
                _0: /* Lambda */4
              };
    case "LE" :
        return {
                TAG: /* Ok */0,
                _0: /* Le */1
              };
    case "LEFT" :
        return {
                TAG: /* Ok */0,
                _0: /* Left */35
              };
    case "LOOP" :
        return {
                TAG: /* Ok */0,
                _0: /* Loop */5
              };
    case "LOOP_LEFT" :
        return {
                TAG: /* Ok */0,
                _0: /* LoopLeft */51
              };
    case "LSL" :
        return {
                TAG: /* Ok */0,
                _0: /* Lsl */59
              };
    case "LSR" :
        return {
                TAG: /* Ok */0,
                _0: /* Lsr */18
              };
    case "LT" :
        return {
                TAG: /* Ok */0,
                _0: /* Lt */41
              };
    case "MAP" :
        return {
                TAG: /* Ok */0,
                _0: /* Map */12
              };
    case "MEM" :
        return {
                TAG: /* Ok */0,
                _0: /* Mem */29
              };
    case "MUL" :
        return {
                TAG: /* Ok */0,
                _0: /* Mul */50
              };
    case "NEG" :
        return {
                TAG: /* Ok */0,
                _0: /* Neg */43
              };
    case "NEQ" :
        return {
                TAG: /* Ok */0,
                _0: /* Neq */42
              };
    case "NIL" :
        return {
                TAG: /* Ok */0,
                _0: /* Nil */48
              };
    case "NONE" :
        return {
                TAG: /* Ok */0,
                _0: /* None */7
              };
    case "NOT" :
        return {
                TAG: /* Ok */0,
                _0: /* Not */34
              };
    case "NOW" :
        return {
                TAG: /* Ok */0,
                _0: /* Now */67
              };
    case "OR" :
        return {
                TAG: /* Ok */0,
                _0: /* Or */60
              };
    case "PACK" :
        return {
                TAG: /* Ok */0,
                _0: /* Pack */15
              };
    case "PAIR" :
        return {
                TAG: /* Ok */0,
                _0: /* Pair */72
              };
    case "PUSH" :
        return {
                TAG: /* Ok */0,
                _0: /* Push */40
              };
    case "RENAME" :
        return {
                TAG: /* Ok */0,
                _0: /* Rename */11
              };
    case "RIGHT" :
        return {
                TAG: /* Ok */0,
                _0: /* Right */30
              };
    case "SELF" :
        return {
                TAG: /* Ok */0,
                _0: /* Self */62
              };
    case "SENDER" :
        return {
                TAG: /* Ok */0,
                _0: /* Sender */64
              };
    case "SET_DELEGATE" :
        return {
                TAG: /* Ok */0,
                _0: /* SetDelegate */13
              };
    case "SHA256" :
        return {
                TAG: /* Ok */0,
                _0: /* Sha256 */9
              };
    case "SHA512" :
        return {
                TAG: /* Ok */0,
                _0: /* Sha512 */24
              };
    case "SIZE" :
        return {
                TAG: /* Ok */0,
                _0: /* Size */16
              };
    case "SLICE" :
        return {
                TAG: /* Ok */0,
                _0: /* Slice */53
              };
    case "SOME" :
        return {
                TAG: /* Ok */0,
                _0: /* Some */23
              };
    case "SOURCE" :
        return {
                TAG: /* Ok */0,
                _0: /* Source */56
              };
    case "STEPS_TO_QUOTA" :
        return {
                TAG: /* Ok */0,
                _0: /* StepsToQuota */54
              };
    case "SUB" :
        return {
                TAG: /* Ok */0,
                _0: /* Sub */28
              };
    case "SWAP" :
        return {
                TAG: /* Ok */0,
                _0: /* Swap */22
              };
    case "TE" :
        return {
                TAG: /* Ok */0,
                _0: /* Ge */39
              };
    case "TRANSFER_TOKENS" :
        return {
                TAG: /* Ok */0,
                _0: /* TransferTokens */19
              };
    case "UNIT" :
        return {
                TAG: /* Ok */0,
                _0: /* Unit */2
              };
    case "UNPACK" :
        return {
                TAG: /* Ok */0,
                _0: /* Unpack */33
              };
    case "UPDATE" :
        return {
                TAG: /* Ok */0,
                _0: /* Update */20
              };
    case "XOR" :
        return {
                TAG: /* Ok */0,
                _0: /* Xor */10
              };
    default:
      return {
              TAG: /* Error */1,
              _0: "PrimitiveInstruction.decode: " + str
            };
  }
}

exports.encode = encode;
exports.decode = decode;
/* No side effect */
