'use strict';

var Fs = require("fs");
var Jest = require("@glennsl/bs-jest/lib/js/src/jest.js");
var Bigint = require("bs-zarith/lib/js/src/Bigint.js");
var Json_decode = require("@glennsl/bs-json/lib/js/src/Json_decode.js");
var Tezos_Parameters = require("../../src/Tezos_Parameters.js");

Jest.describe("Tezos_Parameters.decode", (function (param) {
        Jest.test("golden", (function (param) {
                var json = JSON.parse(Fs.readFileSync("__tests__/golden/parameters.json", "utf8"));
                return Jest.Expect.toEqual({
                            TAG: /* Ok */0,
                            _0: {
                              entrypoint: /* Named */{
                                _0: "xtzToToken"
                              },
                              value: {
                                TAG: /* SingleExpression */4,
                                _0: {
                                  TAG: /* PrimitiveData */1,
                                  _0: /* Pair */8
                                },
                                _1: {
                                  hd: {
                                    TAG: /* StringExpression */1,
                                    _0: "tz1gV3LBv1XmVEkdeRTjuUdurvCf8P85eALM"
                                  },
                                  tl: {
                                    hd: {
                                      TAG: /* SingleExpression */4,
                                      _0: {
                                        TAG: /* PrimitiveData */1,
                                        _0: /* Pair */8
                                      },
                                      _1: {
                                        hd: {
                                          TAG: /* IntExpression */0,
                                          _0: Bigint.of_int(18440679)
                                        },
                                        tl: {
                                          hd: {
                                            TAG: /* StringExpression */1,
                                            _0: "2020-10-11T01:20:22.807Z"
                                          },
                                          tl: /* [] */0
                                        }
                                      },
                                      _2: undefined
                                    },
                                    tl: /* [] */0
                                  }
                                },
                                _2: undefined
                              }
                            }
                          }, Jest.Expect.expect(Tezos_Parameters.decode(json)));
              }));
        return Jest.test("golden", (function (param) {
                      var json = JSON.parse(Fs.readFileSync("__tests__/golden/parameters_as_string.json", "utf8"));
                      return Jest.Expect.toEqual({
                                  TAG: /* Ok */0,
                                  _0: {
                                    entrypoint: /* Named */{
                                      _0: "xtzToToken"
                                    },
                                    value: {
                                      TAG: /* SingleExpression */4,
                                      _0: {
                                        TAG: /* PrimitiveData */1,
                                        _0: /* Pair */8
                                      },
                                      _1: {
                                        hd: {
                                          TAG: /* StringExpression */1,
                                          _0: "tz1gV3LBv1XmVEkdeRTjuUdurvCf8P85eALM"
                                        },
                                        tl: {
                                          hd: {
                                            TAG: /* SingleExpression */4,
                                            _0: {
                                              TAG: /* PrimitiveData */1,
                                              _0: /* Pair */8
                                            },
                                            _1: {
                                              hd: {
                                                TAG: /* IntExpression */0,
                                                _0: Bigint.of_int(18440679)
                                              },
                                              tl: {
                                                hd: {
                                                  TAG: /* StringExpression */1,
                                                  _0: "2020-10-11T01:20:22.807Z"
                                                },
                                                tl: /* [] */0
                                              }
                                            },
                                            _2: undefined
                                          },
                                          tl: /* [] */0
                                        }
                                      },
                                      _2: undefined
                                    }
                                  }
                                }, Jest.Expect.expect(Tezos_Parameters.decode(JSON.parse(Json_decode.string(json)))));
                    }));
      }));

/*  Not a pure module */
