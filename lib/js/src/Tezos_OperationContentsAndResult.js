'use strict';

var Tezos_Util = require("./Tezos_Util.js");
var Belt_Result = require("bs-platform/lib/js/belt_Result.js");
var Json_decode = require("@glennsl/bs-json/lib/js/src/Json_decode.js");
var Tezos_Mutez = require("./Tezos_Mutez.js");
var Tezos_Parameters = require("./Tezos_Parameters.js");
var Caml_js_exceptions = require("bs-platform/lib/js/caml_js_exceptions.js");
var Tezos_BalanceUpdate = require("./Tezos_BalanceUpdate.js");
var Tezos_OperationResult = require("./Tezos_OperationResult.js");
var Tezos_InternalOperationResult = require("./Tezos_InternalOperationResult.js");

function decode(json) {
  var val;
  var val$1;
  var val$2;
  try {
    val = Json_decode.field("balance_updates", (function (a) {
            return Json_decode.list((function (b) {
                          return Tezos_Util.unwrapResult(Tezos_BalanceUpdate.decode(b));
                        }), a);
          }), json);
    val$1 = Json_decode.field("operation_result", (function (a) {
            return Tezos_Util.unwrapResult(Tezos_OperationResult.Transaction.decode(a));
          }), json);
    val$2 = Json_decode.optional((function (param) {
            return Json_decode.field("internal_operation_results", (function (a) {
                          return Json_decode.list((function (b) {
                                        return Tezos_Util.unwrapResult(Tezos_InternalOperationResult.decode(b));
                                      }), a);
                        }), param);
          }), json);
  }
  catch (raw_error){
    var error = Caml_js_exceptions.internalToOCamlException(raw_error);
    if (error.RE_EXN_ID === Json_decode.DecodeError) {
      return {
              TAG: /* Error */1,
              _0: error._1
            };
    }
    throw error;
  }
  return {
          TAG: /* Ok */0,
          _0: {
            balanceUpdates: val,
            operationResult: val$1,
            internalOperationResults: val$2
          }
        };
}

var Metadata = {
  decode: decode
};

function decode$1(json) {
  var val;
  var val$1;
  var val$2;
  var val$3;
  var val$4;
  var val$5;
  var val$6;
  var val$7;
  var val$8;
  try {
    val = Json_decode.field("source", Json_decode.string, json);
    val$1 = Json_decode.field("fee", (function (a) {
            return Tezos_Util.unwrapResult(Tezos_Mutez.decode(a));
          }), json);
    val$2 = Json_decode.field("counter", (function (a) {
            return Tezos_Util.unwrapResult(Tezos_Util.bigintDecode(a));
          }), json);
    val$3 = Json_decode.field("gas_limit", (function (a) {
            return Tezos_Util.unwrapResult(Tezos_Util.bigintDecode(a));
          }), json);
    val$4 = Json_decode.field("storage_limit", (function (a) {
            return Tezos_Util.unwrapResult(Tezos_Util.bigintDecode(a));
          }), json);
    val$5 = Json_decode.field("amount", (function (a) {
            return Tezos_Util.unwrapResult(Tezos_Mutez.decode(a));
          }), json);
    val$6 = Json_decode.field("destination", Json_decode.string, json);
    val$7 = Json_decode.optional((function (param) {
            return Json_decode.field("parameters", (function (a) {
                          return Tezos_Util.unwrapResult(Tezos_Parameters.decode(a));
                        }), param);
          }), json);
    val$8 = Json_decode.field("metadata", (function (a) {
            return Tezos_Util.unwrapResult(decode(a));
          }), json);
  }
  catch (raw_error){
    var error = Caml_js_exceptions.internalToOCamlException(raw_error);
    if (error.RE_EXN_ID === Json_decode.DecodeError) {
      return {
              TAG: /* Error */1,
              _0: error._1
            };
    }
    throw error;
  }
  return {
          TAG: /* Ok */0,
          _0: {
            source: val,
            fee: val$1,
            counter: val$2,
            gasLimit: val$3,
            storageLimit: val$4,
            amount: val$5,
            destination: val$6,
            parameters: val$7,
            metadata: val$8
          }
        };
}

var Transaction = {
  Metadata: Metadata,
  decode: decode$1
};

function decode$2(json) {
  var kind;
  try {
    kind = Json_decode.field("kind", Json_decode.string, json);
  }
  catch (raw_error){
    var error = Caml_js_exceptions.internalToOCamlException(raw_error);
    if (error.RE_EXN_ID === Json_decode.DecodeError) {
      return {
              TAG: /* Error */1,
              _0: error._1
            };
    }
    throw error;
  }
  switch (kind) {
    case "activate_account" :
        return {
                TAG: /* Ok */0,
                _0: /* ActivateAccount */4
              };
    case "ballot" :
        return {
                TAG: /* Ok */0,
                _0: /* Ballot */6
              };
    case "delegation" :
        return {
                TAG: /* Ok */0,
                _0: /* Delegation */9
              };
    case "double_baking_evidence" :
        return {
                TAG: /* Ok */0,
                _0: /* DoubleBakingEvidence */3
              };
    case "double_endorsement_evidence" :
        return {
                TAG: /* Ok */0,
                _0: /* DoubleEndorsementEvidence */2
              };
    case "endorsement" :
        return {
                TAG: /* Ok */0,
                _0: /* Endorsement */0
              };
    case "origination" :
        return {
                TAG: /* Ok */0,
                _0: /* Origination */8
              };
    case "proposals" :
        return {
                TAG: /* Ok */0,
                _0: /* Proposals */5
              };
    case "reveal" :
        return {
                TAG: /* Ok */0,
                _0: /* Reveal */7
              };
    case "seed_nonce_revelation" :
        return {
                TAG: /* Ok */0,
                _0: /* SeedNonceRevelation */1
              };
    case "transaction" :
        return Belt_Result.map(decode$1(json), (function (a) {
                      return /* Transaction */{
                              _0: a
                            };
                    }));
    default:
      return {
              TAG: /* Error */1,
              _0: "Unexpected kind: " + kind
            };
  }
}

exports.Transaction = Transaction;
exports.decode = decode$2;
/* No side effect */
