'use strict';

var Json_decode = require("@glennsl/bs-json/lib/js/src/Json_decode.js");
var Caml_js_exceptions = require("bs-platform/lib/js/caml_js_exceptions.js");

function encode(t) {
  return t._0;
}

function decode(json) {
  var v;
  try {
    v = Json_decode.string(json);
  }
  catch (raw_error){
    var error = Caml_js_exceptions.internalToOCamlException(raw_error);
    if (error.RE_EXN_ID === Json_decode.DecodeError) {
      return {
              TAG: /* Error */1,
              _0: "Tezos_Signature.decode failed: " + error._1
            };
    }
    throw error;
  }
  return {
          TAG: /* Ok */0,
          _0: /* Signature */{
            _0: v
          }
        };
}

exports.encode = encode;
exports.decode = decode;
/* No side effect */
