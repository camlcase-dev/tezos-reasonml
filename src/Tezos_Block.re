/**
 * { "protocol": "ProtoALphaALphaALphaALphaALphaALphaALphaALphaDdp3zK",
 *   "chain_id": $Chain_id,
 *   "hash": $block_hash,
 *   "header": $raw_block_header,
 *   "metadata"?: $block_header_metadata,
 *   "operations": [ [ $operation ... ] ... ] }
 */

type t = {
  chainId: Tezos_ChainId.t,
  hash: Tezos_BlockHash.t,
  header: Tezos_RawBlockHeader.t,
  /* metadata: option(Tezos_BlockHeaderMetadata.t), */  
  operations: list(list(Tezos_Operation.t)),
};

/**
 * JSON
 */

let decode = (json: Js.Json.t): Belt.Result.t(t, string) => {
  Json.Decode.(
    switch (
      field(
        "chain_id",
        a => Tezos_Util.unwrapResult(Tezos_ChainId.decode(a)),
        json,
      ),
      field(
        "hash",
        a => Tezos_Util.unwrapResult(Tezos_BlockHash.decode(a)),
        json,
      ),
      field(
        "header",
        a => Tezos_Util.unwrapResult(Tezos_RawBlockHeader.decode(a)),
        json,
      ),
      field(
        "operations",
        a =>
          list(
            b =>
              list(
                c => Tezos_Util.unwrapResult(Tezos_Operation.decode(c)),
                b,
              ),
            a,
          ),
        json,
      ),
    ) {
    | (chainId, hash, header, operations) =>
      Belt.Result.Ok({chainId, hash, header, operations})
    | exception (DecodeError(error)) => Belt.Result.Error(error)
    }
  );
};
