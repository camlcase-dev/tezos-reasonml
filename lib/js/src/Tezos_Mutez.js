'use strict';

var Int64 = require("bs-platform/lib/js/int64.js");
var Bigint = require("bs-zarith/lib/js/src/Bigint.js");
var Caml_int64 = require("bs-platform/lib/js/caml_int64.js");
var Tezos_Util = require("./Tezos_Util.js");
var Json_decode = require("@glennsl/bs-json/lib/js/src/Json_decode.js");
var Caml_exceptions = require("bs-platform/lib/js/caml_exceptions.js");
var Caml_js_exceptions = require("bs-platform/lib/js/caml_js_exceptions.js");

var zero = /* Mutez */{
  _0: Int64.zero
};

var one = /* Mutez */{
  _0: Int64.one
};

function succ(t) {
  return /* Mutez */{
          _0: Caml_int64.add(t._0, Caml_int64.one)
        };
}

function ofInt($$int) {
  if ($$int >= 0) {
    return {
            TAG: /* Ok */0,
            _0: /* Mutez */{
              _0: Caml_int64.of_int32($$int)
            }
          };
  } else {
    return {
            TAG: /* Error */1,
            _0: "ofInt: Expected non-negative int."
          };
  }
}

function ofBigint(bigint) {
  if (Bigint.geq(bigint, Bigint.zero)) {
    return {
            TAG: /* Ok */0,
            _0: /* Mutez */{
              _0: Bigint.to_int64(bigint)
            }
          };
  } else {
    return {
            TAG: /* Error */1,
            _0: "ofBigint: Expected non-negative bigint."
          };
  }
}

function ofInt64($$int) {
  if (Int64.compare($$int, Int64.zero) >= 0) {
    return {
            TAG: /* Ok */0,
            _0: /* Mutez */{
              _0: $$int
            }
          };
  } else {
    return {
            TAG: /* Error */1,
            _0: "ofInt64: Expected non-negative int64."
          };
  }
}

function ofFloat(f) {
  var i = ofInt64(Caml_int64.of_float(f));
  if (i.TAG) {
    return {
            TAG: /* Error */1,
            _0: "ofFloat: Expected non-negative float."
          };
  } else {
    return {
            TAG: /* Ok */0,
            _0: i._0
          };
  }
}

function ofString(string) {
  var int64 = Tezos_Util.int64OfString(string);
  if (int64 === undefined) {
    return {
            TAG: /* Error */1,
            _0: "ofString: Expected a non-negative int64 encoded as a string."
          };
  }
  var mutez = ofInt64(int64);
  if (mutez.TAG) {
    return {
            TAG: /* Error */1,
            _0: "ofString: Expected a non-negative int64."
          };
  } else {
    return {
            TAG: /* Ok */0,
            _0: mutez._0
          };
  }
}

function ofTezString(string) {
  var $$float = Tezos_Util.floatOfString(string);
  if ($$float === undefined) {
    return {
            TAG: /* Error */1,
            _0: "ofTezString: expected a non-negative float with precision up to 10^-6"
          };
  }
  var decimalsCount = Tezos_Util.getDecimalsCount(string);
  if (decimalsCount > 6) {
    return {
            TAG: /* Error */1,
            _0: "ofTezString: expected a non-negative float with precision up to 10^-6, received: " + string
          };
  }
  var mutezFloat = $$float * 1000000;
  var int64 = Caml_int64.of_float(mutezFloat);
  var mutez = ofInt64(int64);
  if (mutez.TAG) {
    return {
            TAG: /* Error */1,
            _0: "ofString: expected a non-negative int64 value."
          };
  } else {
    return {
            TAG: /* Ok */0,
            _0: mutez._0
          };
  }
}

function toInt64(t) {
  return t._0;
}

function toBigint(t) {
  return Bigint.of_int64(t._0);
}

function toFloat(t) {
  return Caml_int64.to_float(t._0);
}

function toTezFloat(t) {
  return Caml_int64.to_float(t._0) / 1000000.0;
}

function toString(t) {
  return Int64.to_string(t._0);
}

function toTezString(t) {
  return (Caml_int64.to_float(t._0) / 1000000.0).toString();
}

var addCommas = (function (s) {
      var sp = s.split(".");
      var l = sp[0].replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
      if (sp.length > 1) {
        return l.concat(".", sp[1]);
      } else {
        return l;
      }
    });

function toTezStringWithCommas(t) {
  return addCommas(toTezString(t));
}

function add(x, y) {
  var z = ofInt64(Caml_int64.add(x._0, y._0));
  if (z.TAG) {
    return ;
  } else {
    return z._0;
  }
}

function sub(x, y) {
  var z = ofInt64(Caml_int64.sub(x._0, y._0));
  if (z.TAG) {
    return ;
  } else {
    return z._0;
  }
}

function mul(x, y) {
  var y$1 = y._0;
  var x$1 = x._0;
  var result = Caml_int64.mul(x$1, y$1);
  if (Caml_int64.neq(x$1, Int64.zero) && Caml_int64.neq(y$1, Int64.zero) && (Caml_int64.lt(result, x$1) || Caml_int64.lt(result, y$1))) {
    return ;
  }
  var z = ofInt64(Caml_int64.mul(x$1, y$1));
  if (z.TAG) {
    return ;
  } else {
    return z._0;
  }
}

function div(x, y) {
  var y$1 = y._0;
  if (Int64.compare(y$1, Int64.zero) <= 0) {
    return ;
  }
  var z = ofInt64(Caml_int64.div(x._0, y$1));
  if (z.TAG) {
    return ;
  } else {
    return z._0;
  }
}

var Overflow = Caml_exceptions.create("Tezos_Mutez.Overflow");

var Underflow = Caml_exceptions.create("Tezos_Mutez.Underflow");

var DivByZero = Caml_exceptions.create("Tezos_Mutez.DivByZero");

function addUnsafe(x, y) {
  var z = add(x, y);
  if (z !== undefined) {
    return z;
  }
  throw {
        RE_EXN_ID: Overflow,
        _1: undefined,
        Error: new Error()
      };
}

function subUnsafe(x, y) {
  var z = sub(x, y);
  if (z !== undefined) {
    return z;
  }
  throw {
        RE_EXN_ID: Underflow,
        _1: undefined,
        Error: new Error()
      };
}

function mulUnsafe(x, y) {
  var z = mul(x, y);
  if (z !== undefined) {
    return z;
  }
  throw {
        RE_EXN_ID: Overflow,
        _1: undefined,
        Error: new Error()
      };
}

function divUnsafe(x, y) {
  var z = div(x, y);
  if (z !== undefined) {
    return z;
  }
  throw {
        RE_EXN_ID: DivByZero,
        _1: undefined,
        Error: new Error()
      };
}

function compare(x, y) {
  return Int64.compare(x._0, y._0);
}

function equal(x, y) {
  return compare(x, y) === 0;
}

function leq(x, y) {
  return compare(x, y) < 1;
}

function geq(x, y) {
  return compare(x, y) > -1;
}

function gt(x, y) {
  return compare(x, y) > 0;
}

function lt(x, y) {
  return compare(x, y) < 0;
}

function min(x, y) {
  var y$1 = y._0;
  var x$1 = x._0;
  if (Int64.compare(x$1, y$1) < 0) {
    return /* Mutez */{
            _0: x$1
          };
  } else {
    return /* Mutez */{
            _0: y$1
          };
  }
}

function max(x, y) {
  var y$1 = y._0;
  var x$1 = x._0;
  if (Int64.compare(x$1, y$1) > 0) {
    return /* Mutez */{
            _0: x$1
          };
  } else {
    return /* Mutez */{
            _0: y$1
          };
  }
}

function encode(t) {
  return Int64.to_string(t._0);
}

function decode(json) {
  var v;
  try {
    v = Json_decode.string(json);
  }
  catch (raw_error){
    var error = Caml_js_exceptions.internalToOCamlException(raw_error);
    if (error.RE_EXN_ID === Json_decode.DecodeError) {
      return {
              TAG: /* Error */1,
              _0: "Mutez.decode failed: " + error._1
            };
    }
    throw error;
  }
  var $$int = Tezos_Util.int64OfString(v);
  if ($$int !== undefined) {
    if (Int64.compare($$int, Int64.zero) >= 0) {
      return {
              TAG: /* Ok */0,
              _0: /* Mutez */{
                _0: $$int
              }
            };
    } else {
      return {
              TAG: /* Error */1,
              _0: "Mutez.decode failed: " + v
            };
    }
  } else {
    return {
            TAG: /* Error */1,
            _0: "Mutez.decode failed: " + v
          };
  }
}

var oneTez = /* Mutez */{
  _0: Caml_int64.mk(1000000, 0)
};

var minBound = /* Mutez */{
  _0: Caml_int64.zero
};

var maxBound = /* Mutez */{
  _0: Caml_int64.max_int
};

exports.zero = zero;
exports.one = one;
exports.succ = succ;
exports.oneTez = oneTez;
exports.minBound = minBound;
exports.maxBound = maxBound;
exports.ofInt = ofInt;
exports.ofBigint = ofBigint;
exports.ofInt64 = ofInt64;
exports.ofFloat = ofFloat;
exports.ofString = ofString;
exports.ofTezString = ofTezString;
exports.toInt64 = toInt64;
exports.toBigint = toBigint;
exports.toFloat = toFloat;
exports.toTezFloat = toTezFloat;
exports.toString = toString;
exports.toTezString = toTezString;
exports.addCommas = addCommas;
exports.toTezStringWithCommas = toTezStringWithCommas;
exports.add = add;
exports.sub = sub;
exports.mul = mul;
exports.div = div;
exports.Overflow = Overflow;
exports.Underflow = Underflow;
exports.DivByZero = DivByZero;
exports.addUnsafe = addUnsafe;
exports.subUnsafe = subUnsafe;
exports.mulUnsafe = mulUnsafe;
exports.divUnsafe = divUnsafe;
exports.compare = compare;
exports.equal = equal;
exports.leq = leq;
exports.geq = geq;
exports.gt = gt;
exports.lt = lt;
exports.min = min;
exports.max = max;
exports.encode = encode;
exports.decode = decode;
/* No side effect */
