/**
 * $alpha.operation_metadata.alpha.balance_updates:
 *   [ { "kind": "contract",
 *       "contract": $alpha.contract_id,
 *       "change": $int64 }
 *     || { "kind": "freezer",
 *          "category": "rewards",
 *          "delegate": $Signature.Public_key_hash,
 *          "cycle": integer ∈ [-2^31-2, 2^31+2],
 *          "change": $int64 }
 *     || { "kind": "freezer",
 *          "category": "fees",
 *          "delegate": $Signature.Public_key_hash,
 *          "cycle": integer ∈ [-2^31-2, 2^31+2],
 *          "change": $int64 }
 *     || { "kind": "freezer",
 *          "category": "deposits",
 *          "delegate": $Signature.Public_key_hash,
 *          "cycle": integer ∈ [-2^31-2, 2^31+2],
 *          "change": $int64 } ... ]
 *
 * $int64:
 *   /* 64 bit integers
 *      Decimal representation of 64 bit integers */
 *   string
 */

module Contract = {
  type t = {
    contract: string,
    change: Int64.t,
  };

  let decode = (json: Js.Json.t): Belt.Result.t(t, string) => {
    Json.Decode.(
      switch (
        field("contract", string, json),
        field("change", Tezos_Util.decodeInt64String, json),
      ) {
      | (contract, change) => Belt.Result.Ok({contract, change})
      | exception (DecodeError(error)) => Belt.Result.Error(error)
      }
    );
  };
};

module Rewards = {
  type t = {
    delegate: string,
    cycle: int,
    change: Int64.t,
  };

  let decode = (json: Js.Json.t): Belt.Result.t(t, string) => {
    Json.Decode.(
      switch (
        field("delegate", string, json),
        field("cycle", int, json),
        field("change", Tezos_Util.decodeInt64String, json),
      ) {
      | (delegate, cycle, change) =>
        Belt.Result.Ok({delegate, cycle, change})
      | exception (DecodeError(error)) => Belt.Result.Error(error)
      }
    );
  };
};

module Fees = {
  type t = {
    delegate: string,
    cycle: int,
    change: Int64.t,
  };

  let decode = (json: Js.Json.t): Belt.Result.t(t, string) => {
    Json.Decode.(
      switch (
        field("delegate", string, json),
        field("cycle", int, json),
        field("change", Tezos_Util.decodeInt64String, json),
      ) {
      | (delegate, cycle, change) =>
        Belt.Result.Ok({delegate, cycle, change})
      | exception (DecodeError(error)) => Belt.Result.Error(error)
      }
    );
  };
};

module Deposits = {
  type t = {
    delegate: string,
    cycle: int,
    change: Int64.t,
  };

  let decode = (json: Js.Json.t): Belt.Result.t(t, string) => {
    Json.Decode.(
      switch (
        field("delegate", string, json),
        field("cycle", int, json),
        field("change", Tezos_Util.decodeInt64String, json),
      ) {
      | (delegate, cycle, change) =>
        Belt.Result.Ok({delegate, cycle, change})
      | exception (DecodeError(error)) => Belt.Result.Error(error)
      }
    );
  };
};

type t =
  | Contract(Contract.t)
  | Rewards(Rewards.t)
  | Fees(Fees.t)
  | Deposits(Deposits.t);

/**
 * JSON 
 */

let decode = (json: Js.Json.t): Belt.Result.t(t, string) => {
  Json.Decode.(
    switch (field("kind", string, json)) {
    | "contract" => Belt.Result.map(Contract.decode(json), a => Contract(a))

    | "freezer" =>
      switch (field("category", string, json)) {
      | "rewards" => Belt.Result.map(Rewards.decode(json), a => Rewards(a))
      | "fees" => Belt.Result.map(Fees.decode(json), a => Fees(a))
      | "deposits" =>
        Belt.Result.map(Deposits.decode(json), a => Deposits(a))
      | category => Belt.Result.Error("Unexpected category: " ++ category)
      | exception (DecodeError(error)) => Belt.Result.Error(error)
      }
    | kind => Belt.Result.Error("Unexpected kind: " ++ kind)
    | exception (DecodeError(error)) => Belt.Result.Error(error)
    }
  );
};
