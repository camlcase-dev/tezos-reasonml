'use strict';

var Fs = require("fs");
var Jest = require("@glennsl/bs-jest/lib/js/src/jest.js");
var Belt_Result = require("bs-platform/lib/js/belt_Result.js");
var Tezos_OperationResult = require("../../src/Tezos_OperationResult.js");

Jest.describe("Tezos_OperationResult.Transaction.decode", (function (param) {
        return Jest.test("golden", (function (param) {
                      var json = JSON.parse(Fs.readFileSync("__tests__/golden/operation_result_transaction.json", "utf8"));
                      return Jest.Expect.toEqual(true, Jest.Expect.expect(Belt_Result.isOk(Tezos_OperationResult.Transaction.decode(json))));
                    }));
      }));

/*  Not a pure module */
