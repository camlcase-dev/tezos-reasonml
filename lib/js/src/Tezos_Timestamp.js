'use strict';

var Moment = require("moment");
var MomentRe = require("bs-moment/lib/js/src/MomentRe.js");
var Belt_Option = require("bs-platform/lib/js/belt_Option.js");
var Json_decode = require("@glennsl/bs-json/lib/js/src/Json_decode.js");
var Caml_js_exceptions = require("bs-platform/lib/js/caml_js_exceptions.js");

function now(param) {
  return /* Timestamp */{
          _0: Moment()
        };
}

function minutesFromNow(minutes) {
  var now = Moment();
  return /* Timestamp */{
          _0: MomentRe.Moment.add(Moment.duration(minutes, "minutes"), now)
        };
}

function hourFromNow(param) {
  var now = Moment();
  return /* Timestamp */{
          _0: MomentRe.Moment.add(Moment.duration(2, "hours"), now)
        };
}

function ofString(str) {
  return /* Timestamp */{
          _0: Moment(str)
        };
}

function toString(t) {
  return Belt_Option.getWithDefault(MomentRe.Moment.toJSON(t._0), "");
}

function formatUTC(t) {
  return t._0.utc().format("YYYY-MM-DD (HH:mm:ss UTC)");
}

function encode(t) {
  return Belt_Option.getWithDefault(MomentRe.Moment.toJSON(t._0), "");
}

function decode(json) {
  var v;
  try {
    v = Json_decode.string(json);
  }
  catch (raw_error){
    var error = Caml_js_exceptions.internalToOCamlException(raw_error);
    if (error.RE_EXN_ID === Json_decode.DecodeError) {
      return {
              TAG: /* Error */1,
              _0: error._1
            };
    }
    throw error;
  }
  return {
          TAG: /* Ok */0,
          _0: /* Timestamp */{
            _0: Moment(v)
          }
        };
}

exports.now = now;
exports.minutesFromNow = minutesFromNow;
exports.hourFromNow = hourFromNow;
exports.ofString = ofString;
exports.toString = toString;
exports.formatUTC = formatUTC;
exports.encode = encode;
exports.decode = decode;
/* moment Not a pure module */
