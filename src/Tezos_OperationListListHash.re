/**
 * $Operation_list_list_hash:
 *   /* A list of list of operations (Base58Check-encoded) */
 *   $unistring
 */

type t =
  | OperationListListHash(string);

/**
 * JSON encoding and decoding
 */

let encode = (t: t): Js.Json.t => {
  switch (t) {
  | OperationListListHash(str) => Json.Encode.string(str)
  };
};

let decode = (json: Js.Json.t): Belt.Result.t(t, string) => {
  switch (Json.Decode.string(json)) {
  | v => Belt.Result.Ok(OperationListListHash(v))
  | exception (Json.Decode.DecodeError(error)) =>
    Belt.Result.Error("Tezos_OperationListListHash.decode failed: " ++ error)
  };
};
