type t =
  | Mutez(Int64.t);

/**
 * common values
 */

let zero = Mutez(Int64.zero);

let one = Mutez(Int64.one);

let succ = t =>
  switch (t) {
  | Mutez(t) => Mutez(Int64.add(t, 1L))
  };

/* one tez is equivalent to 1,000,000 mutez */
let oneTez = Mutez(1000000L);

let minBound = Mutez(0L);

let maxBound = Mutez(9223372036854775807L);

/**
 * of conversions
 */

let ofInt = (int: int): Belt.Result.t(t, string) =>
  if (int >= 0) {
    Belt.Result.Ok(Mutez(Int64.of_int(int)));
  } else {
    Belt.Result.Error("ofInt: Expected non-negative int.");
  };

let ofBigint = (bigint: Bigint.t): Belt.Result.t(t, string) =>
  if (Bigint.geq(bigint, Bigint.zero)) {
    Belt.Result.Ok(Mutez(Bigint.to_int64(bigint)));
  } else {
    Belt.Result.Error("ofBigint: Expected non-negative bigint.");
  };

let ofInt64 = (int: Int64.t): Belt.Result.t(t, string) =>
  if (Int64.compare(int, Int64.zero) >= 0) {
    Belt.Result.Ok(Mutez(int));
  } else {
    Belt.Result.Error("ofInt64: Expected non-negative int64.");
  };

let ofFloat = (f: float): Belt.Result.t(t, string) => {
  switch (ofInt64(Int64.of_float(f))) {
  | Belt.Result.Ok(i) => Belt.Result.Ok(i)
  | Belt.Result.Error(_) =>
    Belt.Result.Error("ofFloat: Expected non-negative float.")
  };
};

let ofString = (string: string): Belt.Result.t(t, string) =>
  switch (Tezos_Util.int64OfString(string)) {
  | Some(int64) =>
    switch (ofInt64(int64)) {
    | Belt.Result.Ok(mutez) => Belt.Result.Ok(mutez)
    | Belt.Result.Error(_error) =>
      Belt.Result.Error("ofString: Expected a non-negative int64.")
    }
  | None =>
    Belt.Result.Error(
      "ofString: Expected a non-negative int64 encoded as a string.",
    )
  };

let ofTezString = (string: string): Belt.Result.t(t, string) =>
  switch (Tezos_Util.floatOfString(string)) {
  | Some(float) =>
    let decimalsCount = Tezos_Util.getDecimalsCount(string);

    if (decimalsCount <= 6) {
      let mutezFloat = float *. 1000000.;

      let int64 = Int64.of_float(mutezFloat);
      switch (ofInt64(int64)) {
      | Belt.Result.Ok(mutez) => Belt.Result.Ok(mutez)
      | Belt.Result.Error(_error) =>
        Belt.Result.Error("ofString: expected a non-negative int64 value.")
      };
    } else {
      Belt.Result.Error(
        "ofTezString: expected a non-negative float with precision up to 10^-6, received: "
        ++ string,
      );
    };
  | None =>
    Belt.Result.Error(
      "ofTezString: expected a non-negative float with precision up to 10^-6",
    )
  };

/**
 * to conversions
 * unless otherwise stated, all outputs are to mutez and not to tez.
 */

let toInt64 = (t: t): Int64.t =>
  switch (t) {
  | Mutez(int) => int
  };

let toBigint = (t: t): Bigint.t =>
  switch (t) {
  | Mutez(int) => Bigint.of_int64(int)
  };

/* the output value has zero decimal points */
let toFloat = (t: t): float =>
  switch (t) {
  | Mutez(int) => Int64.to_float(int)
  };

/* the output value has six decimal points */
let toTezFloat = (t: t): float =>
  switch (t) {
  | Mutez(int) => Int64.to_float(int) /. 1000000.0
  };

/* convert to a Mutez string */
let toString = (t: t): string =>
  switch (t) {
  | Mutez(int) => Int64.to_string(int)
  };

/* convert to a Tez string */
let toTezString = (t: t): string =>
  switch (t) {
  | Mutez(int) => Js.Float.toString(Int64.to_float(int) /. 1000000.0)
  };

let addCommas: string => string = [%bs.raw
  {|
    function (s) {
      var sp = s.split(".");
      var l = sp[0].replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
      if (sp.length > 1) {
        return l.concat(".", sp[1]);
      } else {
        return l;
      }
    }
     |}
];

let toTezStringWithCommas = (t: t): string => addCommas(toTezString(t));

/**
 * arithmetic
 */

let add = (x, y): option(t) =>
  switch (x, y) {
  | (Mutez(x), Mutez(y)) =>
    switch (ofInt64(Int64.add(x, y))) {
    | Belt.Result.Ok(z) => Some(z)
    | Belt.Result.Error(_) => None
    }
  };

let sub = (x, y): option(t) =>
  switch (x, y) {
  | (Mutez(x), Mutez(y)) =>
    switch (ofInt64(Int64.sub(x, y))) {
    | Belt.Result.Ok(z) => Some(z)
    | Belt.Result.Error(_) => None
    }
  };

let mul = (x, y): option(t) =>
  switch (x, y) {
  | (Mutez(x), Mutez(y)) =>
    let result = Int64.mul(x, y);
    // Int64 will produce a smaller number if it has gone over the max
    if (x != Int64.zero && y != Int64.zero && (result < x || result < y)) {
      None;
    } else {
      switch (ofInt64(Int64.mul(x, y))) {
      | Belt.Result.Ok(z) => Some(z)
      | Belt.Result.Error(_) => None
      };
    };
  };

let div = (x, y) =>
  switch (x, y) {
  | (Mutez(x), Mutez(y)) =>
    if (Int64.compare(y, Int64.zero) <= 0) {
      None;
    } else {
      switch (ofInt64(Int64.div(x, y))) {
      | Belt.Result.Ok(z) => Some(z)
      | Belt.Result.Error(_) => None
      };
    }
  };

/**
 * unsafe arithmetic
 * possibly overflow, underflow or divide by zero
 */

exception Overflow(unit);
exception Underflow(unit);
exception DivByZero(unit);

let addUnsafe = (x, y) => {
  switch (add(x, y)) {
  | Some(z) => z
  | None => raise(Overflow())
  };
};

let subUnsafe = (x, y) => {
  switch (sub(x, y)) {
  | Some(z) => z
  | None => raise(Underflow())
  };
};

let mulUnsafe = (x, y) => {
  switch (mul(x, y)) {
  | Some(z) => z
  | None => raise(Overflow())
  };
};

let divUnsafe = (x, y) => {
  switch (div(x, y)) {
  | Some(z) => z
  | None => raise(DivByZero())
  };
};

/**
 * comparison
 */

let compare = (x, y) =>
  switch (x, y) {
  | (Mutez(x), Mutez(y)) => Int64.compare(x, y)
  };

let equal = (x, y) => compare(x, y) == 0;

let leq = (x, y) => compare(x, y) < 1;

let geq = (x, y) => compare(x, y) > (-1);

let gt = (x, y) => compare(x, y) > 0;

let lt = (x, y) => compare(x, y) < 0;

let min = (x, y): t =>
  switch (x, y) {
  | (Mutez(x), Mutez(y)) =>
    if (Int64.compare(x, y) < 0) {
      Mutez(x);
    } else {
      Mutez(y);
    }
  };

let max = (x, y): t =>
  switch (x, y) {
  | (Mutez(x), Mutez(y)) =>
    if (Int64.compare(x, y) > 0) {
      Mutez(x);
    } else {
      Mutez(y);
    }
  };

/**
 * JSON encoding and decoding
 */

let encode = (t: t): Js.Json.t =>
  switch (t) {
  | Mutez(m) => Json.Encode.string(Int64.to_string(m))
  };

let decode = (json: Js.Json.t) =>
  switch (Json.Decode.string(json)) {
  | v =>
    switch (Tezos_Util.int64OfString(v)) {
    | Some(int) =>
      if (Int64.compare(int, Int64.zero) >= 0) {
        Belt.Result.Ok(Mutez(int));
      } else {
        Belt.Result.Error("Mutez.decode failed: " ++ v);
      }
    | None => Belt.Result.Error("Mutez.decode failed: " ++ v)
    }
  | exception (Json.Decode.DecodeError(error)) =>
    Belt.Result.Error("Mutez.decode failed: " ++ error)
  };
