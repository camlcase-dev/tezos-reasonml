/**
 * $Chain_id:
 *   /* Network identifier (Base58Check-encoded) */
 *   $unistring
 */

type t =
  | ChainId(string);

/**
 * JSON encoding and decoding
 */

let encode = (t: t): Js.Json.t => {
  switch (t) {
  | ChainId(str) => Json.Encode.string(str)
  };
};

let decode = (json: Js.Json.t): Belt.Result.t(t, string) => {
  switch (Json.Decode.string(json)) {
  | v => Belt.Result.Ok(ChainId(v))
  | exception (Json.Decode.DecodeError(error)) =>
    Belt.Result.Error("Tezos_ChainId.decode failed: " ++ error)
  };
};
