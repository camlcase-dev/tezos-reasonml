/**
 * RFC3339 timestamp
 */

type t =
  | Timestamp(MomentRe.Moment.t);

/**
 * common values
 */

let now = () => Timestamp(MomentRe.momentNow());

let minutesFromNow = (minutes: int) => {
  let now = MomentRe.momentNow();
  Timestamp(
    MomentRe.Moment.add(
      ~duration=MomentRe.duration(minutes |> float_of_int, `minutes),
      now,
    ),
  );
};

let hourFromNow = () => {
  let now = MomentRe.momentNow();
  Timestamp(MomentRe.Moment.add(~duration=MomentRe.duration(2., `hours), now));
};

/**
 * conversions
 */

let ofString = (str: string): t =>
  Timestamp(MomentRe.momentDefaultFormat(str));

let toString = (t: t): string =>
  switch (t) {
  | Timestamp(moment) =>
    Belt.Option.getWithDefault(MomentRe.Moment.toJSON(moment), "")
  };

let formatUTC = (t: t): string =>
  switch (t) {
  | Timestamp(moment) =>
    MomentRe.Moment.format(
      "YYYY-MM-DD (HH:mm:ss UTC)",
      MomentRe.Moment.defaultUtc(moment),
    )
  };

/**
 * JSON encoding and decoding
 */

let encode = (t: t): Js.Json.t =>
  switch (t) {
  | Timestamp(moment) =>
    Json.Encode.string(
      Belt.Option.getWithDefault(MomentRe.Moment.toJSON(moment), ""),
    )
  };

let decode = (json: Js.Json.t): Belt.Result.t(t, string) =>
  switch (Json.Decode.string(json)) {
  | v => Belt.Result.Ok(Timestamp(MomentRe.momentDefaultFormat(v)))
  | exception (Json.Decode.DecodeError(error)) => Belt.Result.Error(error)
  };
