/**
 * $fitness:
 *  /* Block fitness
 *     The fitness, or score, of a block, that allow the Tezos to decide
 *     which chain is the best. A fitness value is a list of byte sequences.
 *     They are compared as follows: shortest lists are smaller; lists of the
 *     same length are compared according to the lexicographical order. */
 *  [ /^[a-zA-Z0-9]+$/ ... ]
 */

type t =
  | Fitness(list(string));

/**
 * JSON encoding and decoding
 */

let encode = (t: t): Js.Json.t => {
  switch (t) {
  | Fitness(strs) => Json.Encode.(list(string, strs))
  };
};

let decode = (json: Js.Json.t): Belt.Result.t(t, string) => {
  switch (Json.Decode.(list(string,json))) {
  | v => Belt.Result.Ok(Fitness(v))
  | exception (Json.Decode.DecodeError(error)) =>
    Belt.Result.Error("Tezos_Fitness.decode failed: " ++ error)
  };
};
