/**
 * An address is a Tezos implicit account or a Tezos contract.
 */

type t =
  | ImplicitAccount(Tezos_ImplicitAccount.t)
  | Contract(Tezos_Contract.t);

/**
 * common values
 */

let zero = ImplicitAccount(Tezos_ImplicitAccount.zero);

/**
 * of conversions
 */

let ofString = (candidate: string): Belt.Result.t(t, string) =>
  switch (Tezos_Contract.ofString(candidate)) {
  | Belt.Result.Ok(c) => Belt.Result.Ok(Contract(c))
  | _ =>
    switch (Tezos_ImplicitAccount.ofString(candidate)) {
    | Belt.Result.Ok(c) => Belt.Result.Ok(ImplicitAccount(c))
    | _ =>
      Belt.Result.Error(
        "Address.ofString: unexpected candidate string: " ++ candidate,
      )
    }
  };

let ofContract = (c: Tezos_Contract.t) => Contract(c);

let ofImplicitAccount = (c: Tezos_ImplicitAccount.t) => ImplicitAccount(c);

/**
 * to conversions
 */

let toString = (t: t): string =>
  switch (t) {
  | ImplicitAccount(t) => Tezos_ImplicitAccount.toString(t)
  | Contract(t) => Tezos_Contract.toString(t)
  };

/**
 * pack and unpack
 */

let pack = (t: t): string =>
  switch (t) {
  | ImplicitAccount(t) => Tezos_ImplicitAccount.pack(t)
  | Contract(t) => Tezos_Contract.pack(t)
  };

let unpack = (str: string): option(t) =>
  if (Js.String.startsWith("tz1", str)
      || Js.String.startsWith("tz2", str)
      || Js.String.startsWith("tz3", str)) {
    Belt.Option.map(Tezos_ImplicitAccount.unpack(str), a => ImplicitAccount(a));
  } else {
    Belt.Option.map(Tezos_Contract.unpack(str), a => Contract(a));
  };

/**
 * Tezos script expression
 */

let toScriptExpr = (t: t): string =>
  switch (t) {
  | ImplicitAccount(t) => Tezos_ImplicitAccount.toScriptExpr(t)
  | Contract(t) => Tezos_Contract.toScriptExpr(t)
  };

/**
 * JSON encoding and decoding
 */

let encode = (t: t): Js.Json.t =>
  switch (t) {
  | ImplicitAccount(t) => Tezos_ImplicitAccount.encode(t)
  | Contract(t) => Tezos_Contract.encode(t)
  };

let decode = (json: Js.Json.t): Belt.Result.t(t, string) =>
  switch (Tezos_Contract.decode(json)) {
  | Belt.Result.Ok(c) => Belt.Result.Ok(Contract(c))
  | Belt.Result.Error(_) =>
    switch (Tezos_ImplicitAccount.decode(json)) {
    | Belt.Result.Ok(c) => Belt.Result.Ok(ImplicitAccount(c))
    | Belt.Result.Error(_) =>
      Belt.Result.Error("Address.decode failed: string is not an address.")
    }
  };

/**
 * Comparable module
 */

type comparableType = t;

module Comparable =
  Belt.Id.MakeComparable({
    type t = comparableType;
    let cmp = (c0, c1) =>
      switch (c0, c1) {
      | (ImplicitAccount(str0), ImplicitAccount(str1)) =>
        Pervasives.compare(str0, str1)
      | (Contract(str0), Contract(str1)) => Pervasives.compare(str0, str1)
      | _ => (-1)
      };
  });
