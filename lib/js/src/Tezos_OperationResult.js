'use strict';

var Tezos_Util = require("./Tezos_Util.js");
var Belt_Result = require("bs-platform/lib/js/belt_Result.js");
var Json_decode = require("@glennsl/bs-json/lib/js/src/Json_decode.js");
var Tezos_Expression = require("./Tezos_Expression.js");
var Caml_js_exceptions = require("bs-platform/lib/js/caml_js_exceptions.js");
var Tezos_BalanceUpdate = require("./Tezos_BalanceUpdate.js");

function decode(json) {
  var val;
  var val$1;
  var val$2;
  var val$3;
  var val$4;
  var val$5;
  var val$6;
  var val$7;
  try {
    val = Json_decode.optional((function (param) {
            return Json_decode.field("storage", (function (a) {
                          return Tezos_Util.unwrapResult(Tezos_Expression.decode(a));
                        }), param);
          }), json);
    val$1 = Json_decode.optional((function (param) {
            return Json_decode.field("big_map_diff", (function (a) {
                          return Tezos_Util.unwrapResult(Tezos_Expression.decode(a));
                        }), param);
          }), json);
    val$2 = Json_decode.optional((function (param) {
            return Json_decode.field("balance_updates", (function (a) {
                          return Json_decode.list((function (b) {
                                        return Tezos_Util.unwrapResult(Tezos_BalanceUpdate.decode(b));
                                      }), a);
                        }), param);
          }), json);
    val$3 = Json_decode.optional((function (param) {
            return Json_decode.field("originated_contracts", (function (param) {
                          return Json_decode.list(Json_decode.string, param);
                        }), param);
          }), json);
    val$4 = Json_decode.optional((function (param) {
            return Json_decode.field("consumed_gas", (function (a) {
                          return Tezos_Util.unwrapResult(Tezos_Util.bigintDecode(a));
                        }), param);
          }), json);
    val$5 = Json_decode.optional((function (param) {
            return Json_decode.field("storage_size", (function (a) {
                          return Tezos_Util.unwrapResult(Tezos_Util.bigintDecode(a));
                        }), param);
          }), json);
    val$6 = Json_decode.optional((function (param) {
            return Json_decode.field("paid_storage_size_diff", (function (a) {
                          return Tezos_Util.unwrapResult(Tezos_Util.bigintDecode(a));
                        }), param);
          }), json);
    val$7 = Json_decode.optional((function (param) {
            return Json_decode.field("allocated_destination_contract", Json_decode.bool, param);
          }), json);
  }
  catch (raw_error){
    var error = Caml_js_exceptions.internalToOCamlException(raw_error);
    if (error.RE_EXN_ID === Json_decode.DecodeError) {
      return {
              TAG: /* Error */1,
              _0: error._1
            };
    }
    throw error;
  }
  return {
          TAG: /* Ok */0,
          _0: {
            storage: val,
            bigMapDiff: val$1,
            balanceUpdates: val$2,
            originatedContracts: val$3,
            consumedGas: val$4,
            storageSize: val$5,
            paidStorageSizeDiff: val$6,
            allocatedDestinationContract: val$7
          }
        };
}

var Applied = {
  decode: decode
};

function decode$1(json) {
  var status;
  try {
    status = Json_decode.field("status", Json_decode.string, json);
  }
  catch (raw_error){
    var error = Caml_js_exceptions.internalToOCamlException(raw_error);
    if (error.RE_EXN_ID === Json_decode.DecodeError) {
      return {
              TAG: /* Error */1,
              _0: error._1
            };
    }
    throw error;
  }
  switch (status) {
    case "applied" :
        return Belt_Result.map(decode(json), (function (a) {
                      return /* Applied */{
                              _0: a
                            };
                    }));
    case "backtracked" :
        return {
                TAG: /* Ok */0,
                _0: /* Backtracked */2
              };
    case "failed" :
        return {
                TAG: /* Ok */0,
                _0: /* Failed */0
              };
    case "skipped" :
        return {
                TAG: /* Ok */0,
                _0: /* Skipped */1
              };
    default:
      return {
              TAG: /* Error */1,
              _0: "Unexpected status: " + status
            };
  }
}

var Transaction = {
  Applied: Applied,
  decode: decode$1
};

exports.Transaction = Transaction;
/* No side effect */
