'use strict';

var Jest = require("@glennsl/bs-jest/lib/js/src/jest.js");
var Tezos_Address = require("../../src/Tezos_Address.js");
var Caml_exceptions = require("bs-platform/lib/js/caml_exceptions.js");

var TestError = Caml_exceptions.create("Tezos_Address_Spec.TestError");

function ofStringUnsafe(s) {
  var s$1 = Tezos_Address.ofString(s);
  if (!s$1.TAG) {
    return s$1._0;
  }
  throw {
        RE_EXN_ID: TestError,
        _1: s$1._0,
        Error: new Error()
      };
}

Jest.describe("ofString", (function (param) {
        Jest.test("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i", (function (param) {
                return Jest.Expect.toEqual({
                            TAG: /* Ok */0,
                            _0: ofStringUnsafe("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i")
                          }, Jest.Expect.expect(Tezos_Address.ofString("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i")));
              }));
        Jest.test("tz2L9474hvA2EXyNLi4u3YnqrKVf3GzeZ8tb", (function (param) {
                return Jest.Expect.toEqual({
                            TAG: /* Ok */0,
                            _0: ofStringUnsafe("tz2L9474hvA2EXyNLi4u3YnqrKVf3GzeZ8tb")
                          }, Jest.Expect.expect(Tezos_Address.ofString("tz2L9474hvA2EXyNLi4u3YnqrKVf3GzeZ8tb")));
              }));
        return Jest.test("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn", (function (param) {
                      return Jest.Expect.toEqual({
                                  TAG: /* Ok */0,
                                  _0: ofStringUnsafe("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn")
                                }, Jest.Expect.expect(Tezos_Address.ofString("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn")));
                    }));
      }));

exports.TestError = TestError;
exports.ofStringUnsafe = ofStringUnsafe;
/*  Not a pure module */
