/**
 * $micheline.alpha.michelson_v1.expression:
 *   { /* Int */
 *     "int": $bignum }
 *   || { /* String */
 *        "string": $unistring }
 *   || { /* Bytes */
 *        "bytes": /^[a-zA-Z0-9]+$/ }
 *   || [ $micheline.alpha.michelson_v1.expression ... ]
 *   /* Sequence */
 *   || { /* Generic prim (any number of args with or without annot) */
 *        "prim": $alpha.michelson.v1.primitives,
 *        "args"?: [ $micheline.alpha.michelson_v1.expression ... ],
 *        "annots"?: [ string ... ] }
 */

type t =
  | IntExpression(Bigint.t)
  | StringExpression(string)
  | BytesExpression(string)
  | Expressions(list(t))
  | SingleExpression(
      Tezos_Primitives.t,
      option(list(t)),
      option(list(string)),
    );

/**
 * Convenient functions
 */

let expressions = x => Expressions(x);

let singleExpression = (~prim, ~args=?, ~annots=?, ()) =>
  SingleExpression(prim, args, annots);

let getIntExpression = (t): option(Bigint.t) => {
  switch (t) {
  | IntExpression(i) => Some(i)
  | _ => None
  };
};

let getStringExpression = (t): option(string) => {
  switch (t) {
  | StringExpression(str) => Some(str)
  | _ => None
  };
};

/**
 * JSON encoding and decoding
 */

let rec encodeRec = (expression: t): Js.Json.t =>
  switch (expression) {
  | IntExpression(i) =>
    Json.Encode.(object_([("int", string(Bigint.to_string(i)))]))
  | StringExpression(str) =>
    Json.Encode.(object_([("string", string(str))]))
  | BytesExpression(bytes) =>
    Json.Encode.(object_([("bytes", string(bytes))]))
  | Expressions(expressions) => Json.Encode.list(encodeRec, expressions)
  | SingleExpression(prim, args, annots) =>
    let argsEncoded =
      switch (args) {
      | Some(args) => [("args", Json.Encode.list(encodeRec, args))]
      | None => []
      };
    let annotsEncoded =
      switch (annots) {
      | Some(annots) => [
          ("annots", Json.Encode.list(Json.Encode.string, annots)),
        ]
      | None => []
      };
    Json.Encode.(
      object_(
        List.concat([
          [("prim", Tezos_Primitives.encode(prim))],
          argsEncoded,
          annotsEncoded,
        ]),
      )
    );
  };

let encode = (expression: t): Js.Json.t => encodeRec(expression);

let rec decodeRec = (json: Js.Json.t): Belt.Result.t(t, string) =>
  Json.Decode.(
    switch (list(a => Tezos_Util.unwrapResult(decodeRec(a)), json)) {
    | v => Belt.Result.Ok(Expressions(v))
    | exception (DecodeError(_error)) =>
      switch (field("int", string, json)) {
      | v =>
        switch (Tezos_Util.bigintOfString(v)) {
        | Some(i) => Belt.Result.Ok(IntExpression(i))
        | None => Belt.Result.Error("Expected string encoded int.")
        }
      | exception (DecodeError(_error)) =>
        switch (field("string", string, json)) {
        | v => Belt.Result.Ok(StringExpression(v))
        | exception (DecodeError(_error)) =>
          switch (field("bytes", string, json)) {
          | v => Belt.Result.Ok(BytesExpression(v))
          | exception (DecodeError(_error)) =>
            switch (
              field(
                "prim",
                a => Tezos_Util.unwrapResult(Tezos_Primitives.decode(a)),
                json,
              ),
              optional(
                field(
                  "args",
                  list(a => Tezos_Util.unwrapResult(decodeRec(a))),
                ),
                json,
              ),
              optional(field("annots", list(string)), json),
            ) {
            | (prim, args, annots) =>
              Belt.Result.Ok(SingleExpression(prim, args, annots))
            | exception (DecodeError(error)) => Belt.Result.Error(error)
            }
          }
        }
      }
    }
  );

let decode = (json: Js.Json.t): Belt.Result.t(t, string) => decodeRec(json);
