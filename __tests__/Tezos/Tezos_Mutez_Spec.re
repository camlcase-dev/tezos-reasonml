open Jest;
open Expect;

exception TestError(string);

let ofStringUnsafe = s => {
  switch (Tezos_Mutez.ofString(s)) {
  | Belt.Result.Ok(s) => s
  | Belt.Result.Error(err) => raise @@ TestError(err)
  };
};

describe("succ", () => {
  test("0", _ => {
    expect(Tezos_Mutez.succ(Tezos_Mutez.zero)) |> toEqual(Tezos_Mutez.one)
  })
});

describe("ofInt", () => {
  test("0", _ => {
    expect(Tezos_Mutez.ofInt(0))
    |> toEqual(Belt.Result.Ok(Tezos_Mutez.zero))
  });

  test("1", _ => {
    expect(Tezos_Mutez.ofInt(1))
    |> toEqual(Belt.Result.Ok(Tezos_Mutez.one))
  });

  test("max_int", _ => {
    expect(Tezos_Mutez.ofInt(max_int))
    |> toEqual(Tezos_Mutez.ofString("2147483647"))
  });

  test("-1", _ => {
    expect(Tezos_Mutez.ofInt(-1))
    |> toEqual(Belt.Result.Error("ofInt: Expected non-negative int."))
  });
});

describe("ofInt64", () => {
  test("0", _ => {
    expect(Tezos_Mutez.ofInt64(0L))
    |> toEqual(Belt.Result.Ok(Tezos_Mutez.zero))
  });

  test("1", _ => {
    expect(Tezos_Mutez.ofInt64(1L))
    |> toEqual(Belt.Result.Ok(Tezos_Mutez.one))
  });

  test("max_int", _ => {
    expect(Tezos_Mutez.ofInt64(Int64.max_int))
    |> toEqual(Tezos_Mutez.ofString("9223372036854775807"))
  });

  test("-1", _ => {
    expect(Tezos_Mutez.ofInt64(-1L))
    |> toEqual(Belt.Result.Error("ofInt64: Expected non-negative int64."))
  });
});

describe("ofFloat", () => {
  test("0", _ => {
    expect(Tezos_Mutez.ofFloat(0.0))
    |> toEqual(Belt.Result.Ok(Tezos_Mutez.zero))
  });

  test("1", _ => {
    expect(Tezos_Mutez.ofFloat(1.0))
    |> toEqual(Belt.Result.Ok(Tezos_Mutez.one))
  });

  test("1.25", _ => {
    expect(Tezos_Mutez.ofFloat(1.25))
    |> toEqual(Belt.Result.Ok(Tezos_Mutez.one))
  });

  test("max_float", _ => {
    expect(Tezos_Mutez.ofFloat(max_float))
    |> toEqual(Tezos_Mutez.ofString("9223372036854775807"))
  });

  test("-1", _ => {
    expect(Tezos_Mutez.ofFloat(-1.0))
    |> toEqual(Belt.Result.Error("ofFloat: Expected non-negative float."))
  });
});

describe("ofString", () => {
  test("\"0\"", _ => {
    expect(Tezos_Mutez.ofString("0"))
    |> toEqual(Belt.Result.Ok(Tezos_Mutez.zero))
  });

  test("\"1\"", _ => {
    expect(Tezos_Mutez.ofString("1"))
    |> toEqual(Belt.Result.Ok(Tezos_Mutez.one))
  });

  test("\"1000000\"", _ => {
    expect(Tezos_Mutez.ofString("1000000"))
    |> toEqual(Belt.Result.Ok(Tezos_Mutez.oneTez))
  });

  test("\"-1\"", _ => {
    expect(Tezos_Mutez.ofString("-1"))
    |> toEqual(Belt.Result.Error("ofString: Expected a non-negative int64."))
  });

  test("\"abc\"", _ => {
    expect(Tezos_Mutez.ofString("abc"))
    |> toEqual(
         Belt.Result.Error(
           "ofString: Expected a non-negative int64 encoded as a string.",
         ),
       )
  });
});

describe("ofTezString", () => {
  test("\"0\"", _ => {
    expect(Tezos_Mutez.ofTezString("0"))
    |> toEqual(Belt.Result.Ok(Tezos_Mutez.zero))
  });

  test("\"0.\"", _ => {
    expect(Tezos_Mutez.ofTezString("0."))
    |> toEqual(Belt.Result.Ok(Tezos_Mutez.zero))
  });

  test("\"0.0\"", _ => {
    expect(Tezos_Mutez.ofTezString("0.0"))
    |> toEqual(Belt.Result.Ok(Tezos_Mutez.zero))
  });

  test("\"1.1\"", _ =>
    expect(Tezos_Mutez.ofTezString("1.1"))
    |> toEqual(Tezos_Mutez.ofInt64(1100000L))
  );

  test("\"0.000001\"", _ =>
    expect(Tezos_Mutez.ofTezString("0.000001"))
    |> toEqual(Tezos_Mutez.ofInt64(1L))
  );

  test("\"1.12\"", _ =>
    expect(Tezos_Mutez.ofTezString("1.12"))
    |> toEqual(Tezos_Mutez.ofInt64(1120000L))
  );

  test("\"1.123456\"", _ =>
    expect(Tezos_Mutez.ofTezString("1.123456"))
    |> toEqual(Tezos_Mutez.ofInt64(1123456L))
  );

  test("\"12\"", _ =>
    expect(Tezos_Mutez.ofTezString("12"))
    |> toEqual(Tezos_Mutez.ofInt64(12000000L))
  );

  test("\"23\"", _ =>
    expect(Tezos_Mutez.ofTezString("23"))
    |> toEqual(Tezos_Mutez.ofInt64(23000000L))
  );

  test("\"123.145\"", _ =>
    expect(Tezos_Mutez.ofTezString("123.145"))
    |> toEqual(Tezos_Mutez.ofInt64(123145000L))
  );

  test("\"0.0000001\"", _ =>
    expect(Belt.Result.isError(Tezos_Mutez.ofTezString("0.0000001")))
    |> toEqual(true)
  );

  test("\"0.0000000\"", _ =>
    expect(Belt.Result.isError(Tezos_Mutez.ofTezString("0.0000000")))
    |> toEqual(true)
  );

  test("\"0.00000001\"", _ =>
    expect(Belt.Result.isError(Tezos_Mutez.ofTezString("0.00000001")))
    |> toEqual(true)
  );

  test("\"abc\"", _ =>
    expect(Belt.Result.isError(Tezos_Mutez.ofTezString("abc")))
    |> toEqual(true)
  );

  test("\"-1.0\"", _ =>
    expect(Belt.Result.isError(Tezos_Mutez.ofTezString("-1.0")))
    |> toEqual(true)
  );
});

describe("toInt64", () => {
  test("0", _ => {
    expect(Tezos_Mutez.toInt64(Tezos_Mutez.zero)) |> toEqual(0L)
  });

  test("1", _ => {
    expect(Tezos_Mutez.toInt64(Tezos_Mutez.one)) |> toEqual(1L)
  });

  test("1.000000", _ => {
    expect(Tezos_Mutez.toInt64(Tezos_Mutez.oneTez)) |> toEqual(1000000L)
  });

  test("45.670012", _ => {
    expect(
      Belt.Result.map(
        Tezos_Mutez.ofTezString("45.670012"),
        Tezos_Mutez.toInt64,
      ),
    )
    |> toEqual(Belt.Result.Ok(45670012L))
  });
});

describe("toFloat", () => {
  test("0", _ => {
    expect(Tezos_Mutez.toFloat(Tezos_Mutez.zero)) |> toEqual(0.0)
  });

  test("1", _ => {
    expect(Tezos_Mutez.toFloat(Tezos_Mutez.one)) |> toEqual(1.0)
  });

  test("1.000000", _ => {
    expect(Tezos_Mutez.toFloat(Tezos_Mutez.oneTez)) |> toEqual(1000000.0)
  });

  test("45.670012", _ => {
    expect(
      Belt.Result.map(
        Tezos_Mutez.ofTezString("45.670012"),
        Tezos_Mutez.toFloat,
      ),
    )
    |> toEqual(Belt.Result.Ok(45670012.0))
  });
});

describe("toTezFloat", () => {
  test("0", _ => {
    expect(Tezos_Mutez.toTezFloat(Tezos_Mutez.zero)) |> toEqual(0.0)
  });

  test("1", _ => {
    expect(Tezos_Mutez.toTezFloat(Tezos_Mutez.one)) |> toEqual(0.000001)
  });

  test("1.000000", _ => {
    expect(Tezos_Mutez.toTezFloat(Tezos_Mutez.oneTez)) |> toEqual(1.000000)
  });

  test("45.670012", _ => {
    expect(
      Belt.Result.map(
        Tezos_Mutez.ofTezString("45.670012"),
        Tezos_Mutez.toTezFloat,
      ),
    )
    |> toEqual(Belt.Result.Ok(45.670012))
  });
});

describe("toString", () => {
  test("0", _ => {
    expect(Tezos_Mutez.toString(Tezos_Mutez.zero)) |> toEqual("0")
  });

  test("1", _ => {
    expect(Tezos_Mutez.toString(Tezos_Mutez.one)) |> toEqual("1")
  });

  test("1.000000", _ => {
    expect(Tezos_Mutez.toString(Tezos_Mutez.oneTez)) |> toEqual("1000000")
  });

  test("45.670012", _ => {
    expect(
      Belt.Result.map(
        Tezos_Mutez.ofTezString("45.670012"),
        Tezos_Mutez.toString,
      ),
    )
    |> toEqual(Belt.Result.Ok("45670012"))
  });
});

describe("toTezString", () => {
  test("0", _ => {
    expect(Tezos_Mutez.toTezString(Tezos_Mutez.zero)) |> toEqual("0")
  });

  test("1", _ => {
    expect(Tezos_Mutez.toTezString(Tezos_Mutez.one)) |> toEqual("0.000001")
  });

  test("1.000000", _ => {
    expect(Tezos_Mutez.toTezString(Tezos_Mutez.oneTez)) |> toEqual("1")
  });

  test("45.670012", _ => {
    expect(
      Belt.Result.map(
        Tezos_Mutez.ofTezString("45.670012"),
        Tezos_Mutez.toTezString,
      ),
    )
    |> toEqual(Belt.Result.Ok("45.670012"))
  });
});

describe("toTezStringWithCommas", () => {
  test("0", _ => {
    expect(Tezos_Mutez.toTezStringWithCommas(Tezos_Mutez.zero))
    |> toEqual("0")
  });

  test("1", _ => {
    expect(Tezos_Mutez.toTezStringWithCommas(Tezos_Mutez.one))
    |> toEqual("0.000001")
  });

  test("1.000000", _ => {
    expect(Tezos_Mutez.toTezStringWithCommas(Tezos_Mutez.oneTez))
    |> toEqual("1")
  });

  test("45.670012", _ => {
    expect(
      Belt.Result.map(
        Tezos_Mutez.ofTezString("45.670012"),
        Tezos_Mutez.toTezStringWithCommas,
      ),
    )
    |> toEqual(Belt.Result.Ok("45.670012"))
  });

  test("1334.6", _ => {
    expect(
      Belt.Result.map(
        Tezos_Mutez.ofTezString("1334.6"),
        Tezos_Mutez.toTezStringWithCommas,
      ),
    )
    |> toEqual(Belt.Result.Ok("1,334.6"))
  });

  test("3991334.683928", _ => {
    expect(
      Belt.Result.map(
        Tezos_Mutez.ofTezString("3991334.683928"),
        Tezos_Mutez.toTezStringWithCommas,
      ),
    )
    |> toEqual(Belt.Result.Ok("3,991,334.683928"))
  });
});

describe("add", () => {
  test("0 + 0", _ => {
    expect(Tezos_Mutez.add(Tezos_Mutez.zero, Tezos_Mutez.zero))
    |> toEqual(Some(Tezos_Mutez.zero))
  });

  test("0 + 1", _ => {
    expect(Tezos_Mutez.add(Tezos_Mutez.zero, Tezos_Mutez.one))
    |> toEqual(Some(Tezos_Mutez.one))
  });

  test("1 + 0", _ => {
    expect(Tezos_Mutez.add(Tezos_Mutez.one, Tezos_Mutez.zero))
    |> toEqual(Some(Tezos_Mutez.one))
  });

  test("1234567 + 234345", _ => {
    expect(
      Tezos_Mutez.add(ofStringUnsafe("1234567"), ofStringUnsafe("234345")),
    )
    |> toEqual(Some(ofStringUnsafe("1468912")))
  });
});

describe("sub", () => {
  test("0 - 0", _ => {
    expect(Tezos_Mutez.sub(Tezos_Mutez.zero, Tezos_Mutez.zero))
    |> toEqual(Some(Tezos_Mutez.zero))
  });

  test("0 - 1", _ => {
    expect(Tezos_Mutez.sub(Tezos_Mutez.zero, Tezos_Mutez.one))
    |> toEqual(None)
  });

  test("1 - 0", _ => {
    expect(Tezos_Mutez.sub(Tezos_Mutez.one, Tezos_Mutez.zero))
    |> toEqual(Some(Tezos_Mutez.one))
  });

  test("2343450 - 1234567", _ => {
    expect(
      Tezos_Mutez.sub(ofStringUnsafe("2343450"), ofStringUnsafe("1234567")),
    )
    |> toEqual(Some(ofStringUnsafe("1108883")))
  });
});

describe("mul", () => {
  test("0 * 0", _ => {
    expect(Tezos_Mutez.mul(Tezos_Mutez.zero, Tezos_Mutez.zero))
    |> toEqual(Some(Tezos_Mutez.zero))
  });

  test("0 * 1", _ => {
    expect(Tezos_Mutez.mul(Tezos_Mutez.zero, Tezos_Mutez.one))
    |> toEqual(Some(Tezos_Mutez.zero))
  });

  test("1 * 0", _ => {
    expect(Tezos_Mutez.mul(Tezos_Mutez.one, Tezos_Mutez.zero))
    |> toEqual(Some(Tezos_Mutez.zero))
  });

  test("1 * 1", _ => {
    expect(Tezos_Mutez.mul(Tezos_Mutez.one, Tezos_Mutez.one))
    |> toEqual(Some(Tezos_Mutez.one))
  });

  test("4 * 5", _ => {
    expect(Tezos_Mutez.mul(ofStringUnsafe("4"), ofStringUnsafe("5")))
    |> toEqual(Some(ofStringUnsafe("20")))
  });


  test("9223372036854775807 * 0", _ => {
    expect(
      Tezos_Mutez.mul(
        Tezos_Mutez.maxBound,
        Tezos_Mutez.zero,
      ),
    )
    |> toEqual(Some(Tezos_Mutez.zero))
  });
  
  test("9223372036854775807 * 2", _ => {
    expect(
      Tezos_Mutez.mul(
        ofStringUnsafe("9223372036854775807"),
        ofStringUnsafe("2"),
      ),
    )
    |> toEqual(None)
  });

  test("9223372036854775807 * 3", _ => {
    expect(
      Tezos_Mutez.mul(
        Tezos_Mutez.maxBound,
        ofStringUnsafe("3"),
      ),
    )
    |> toEqual(None)
  });

  test("9223372036854775807 * 2", _ => {
    expect(
      Tezos_Mutez.mul(
        ofStringUnsafe("9223372036854775807"),
        ofStringUnsafe("2"),
      ),
    )
    |> toEqual(None)
  });  
});

describe("div", () => {
  test("3 / 2", _ => {
    expect(Tezos_Mutez.div(ofStringUnsafe("3"), ofStringUnsafe("2")))
    |> toEqual(Some(Tezos_Mutez.one))
  });

  test("3 / 0", _ => {
    expect(Tezos_Mutez.div(ofStringUnsafe("3"), ofStringUnsafe("0")))
    |> toEqual(None)
  });

  test("10 / 5", _ => {
    expect(Tezos_Mutez.div(ofStringUnsafe("10"), ofStringUnsafe("5")))
    |> toEqual(Some(ofStringUnsafe("2")))
  });    
});

describe("addUnsafe", () => {
  test("max + 0", _ => {
    expect(Tezos_Mutez.addUnsafe(Tezos_Mutez.maxBound, Tezos_Mutez.zero))
    |> toEqual(Tezos_Mutez.maxBound)
  });
  
  test("max + 1", _ => {
    expectFn(Tezos_Mutez.addUnsafe(Tezos_Mutez.maxBound), Tezos_Mutez.one)
    |> toThrow
  });
});

describe("subUnsafe", () => {
  test("minBound - 1", _ => {
    expectFn(Tezos_Mutez.subUnsafe(Tezos_Mutez.minBound), Tezos_Mutez.one)
    |> toThrow
  });
});

describe("mulUnsafe", () => {
  test("maxBound * 2", _ => {
    expectFn(Tezos_Mutez.mulUnsafe(Tezos_Mutez.maxBound), ofStringUnsafe("2"))
    |> toThrow
  });

  test("maxBound * 3", _ => {
    expectFn(Tezos_Mutez.mulUnsafe(Tezos_Mutez.maxBound), ofStringUnsafe("3"))
    |> toThrow
  });

  test("maxBound * 4", _ => {
    expectFn(Tezos_Mutez.mulUnsafe(Tezos_Mutez.maxBound), ofStringUnsafe("4"))
    |> toThrow
  });  
});


describe("compare", () => {
  test("(0,0)", _ => {
    expect(Tezos_Mutez.compare(Tezos_Mutez.zero, Tezos_Mutez.zero))
    |> toEqual(0)
  });
  test("(0,1)", _ => {
    expect(Tezos_Mutez.compare(Tezos_Mutez.zero, Tezos_Mutez.one))
    |> toEqual(-1)
  });
  test("(1,0)", _ => {
    expect(Tezos_Mutez.compare(Tezos_Mutez.one, Tezos_Mutez.zero))
    |> toEqual(1)
  });
});

describe("equal", () => {
  test("(0,0)", _ => {
    expect(Tezos_Mutez.equal(Tezos_Mutez.zero, Tezos_Mutez.zero))
    |> toEqual(true)
  });
  test("(0,1)", _ => {
    expect(Tezos_Mutez.equal(Tezos_Mutez.zero, Tezos_Mutez.one))
    |> toEqual(false)
  });
  test("(1,0)", _ => {
    expect(Tezos_Mutez.equal(Tezos_Mutez.one, Tezos_Mutez.zero))
    |> toEqual(false)
  });

  test("(4,10)", _ => {
    expect(Tezos_Mutez.equal(ofStringUnsafe("4"), ofStringUnsafe("10")))
    |> toEqual(false)
  });

  test("(13,27)", _ => {
    expect(Tezos_Mutez.equal(ofStringUnsafe("13"), ofStringUnsafe("27")))
    |> toEqual(false)
  });

  test("(56,56)", _ => {
    expect(Tezos_Mutez.equal(ofStringUnsafe("56"), ofStringUnsafe("56")))
    |> toEqual(true)
  });
});

describe("leq", () => {
  test("(0,0)", _ => {
    expect(Tezos_Mutez.leq(Tezos_Mutez.zero, Tezos_Mutez.zero))
    |> toEqual(true)
  });
  test("(0,1)", _ => {
    expect(Tezos_Mutez.leq(Tezos_Mutez.zero, Tezos_Mutez.one))
    |> toEqual(true)
  });
  test("(1,0)", _ => {
    expect(Tezos_Mutez.leq(Tezos_Mutez.one, Tezos_Mutez.zero))
    |> toEqual(false)
  });

  test("(4,10)", _ => {
    expect(Tezos_Mutez.leq(ofStringUnsafe("4"), ofStringUnsafe("10")))
    |> toEqual(true)
  });

  test("(13,27)", _ => {
    expect(Tezos_Mutez.leq(ofStringUnsafe("13"), ofStringUnsafe("27")))
    |> toEqual(true)
  });

  test("(56,56)", _ => {
    expect(Tezos_Mutez.leq(ofStringUnsafe("56"), ofStringUnsafe("56")))
    |> toEqual(true)
  });

  test("(123,245)", _ => {
    expect(Tezos_Mutez.leq(ofStringUnsafe("123"), ofStringUnsafe("245")))
    |> toEqual(true)
  });
});

describe("geq", () => {
  test("(0,0)", _ => {
    expect(Tezos_Mutez.geq(Tezos_Mutez.zero, Tezos_Mutez.zero))
    |> toEqual(true)
  });
  test("(0,1)", _ => {
    expect(Tezos_Mutez.geq(Tezos_Mutez.zero, Tezos_Mutez.one))
    |> toEqual(false)
  });
  test("(1,0)", _ => {
    expect(Tezos_Mutez.geq(Tezos_Mutez.one, Tezos_Mutez.zero))
    |> toEqual(true)
  });

  test("(4,10)", _ => {
    expect(Tezos_Mutez.geq(ofStringUnsafe("4"), ofStringUnsafe("10")))
    |> toEqual(false)
  });

  test("(13,27)", _ => {
    expect(Tezos_Mutez.geq(ofStringUnsafe("13"), ofStringUnsafe("27")))
    |> toEqual(false)
  });

  test("(56,56)", _ => {
    expect(Tezos_Mutez.geq(ofStringUnsafe("56"), ofStringUnsafe("56")))
    |> toEqual(true)
  });

  test("(123,245)", _ => {
    expect(Tezos_Mutez.geq(ofStringUnsafe("123"), ofStringUnsafe("245")))
    |> toEqual(false)
  });

  test("(245,123)", _ => {
    expect(Tezos_Mutez.geq(ofStringUnsafe("245"), ofStringUnsafe("123")))
    |> toEqual(true)
  });
});

describe("gt", () => {
  test("(0,0)", _ => {
    expect(Tezos_Mutez.gt(Tezos_Mutez.zero, Tezos_Mutez.zero))
    |> toEqual(false)
  });
  test("(0,1)", _ => {
    expect(Tezos_Mutez.gt(Tezos_Mutez.zero, Tezos_Mutez.one))
    |> toEqual(false)
  });
  test("(1,0)", _ => {
    expect(Tezos_Mutez.gt(Tezos_Mutez.one, Tezos_Mutez.zero))
    |> toEqual(true)
  });

  test("(4,10)", _ => {
    expect(Tezos_Mutez.gt(ofStringUnsafe("4"), ofStringUnsafe("10")))
    |> toEqual(false)
  });

  test("(13,27)", _ => {
    expect(Tezos_Mutez.gt(ofStringUnsafe("13"), ofStringUnsafe("27")))
    |> toEqual(false)
  });

  test("(56,56)", _ => {
    expect(Tezos_Mutez.gt(ofStringUnsafe("56"), ofStringUnsafe("56")))
    |> toEqual(false)
  });

  test("(123,245)", _ => {
    expect(Tezos_Mutez.gt(ofStringUnsafe("123"), ofStringUnsafe("245")))
    |> toEqual(false)
  });

  test("(245,123)", _ => {
    expect(Tezos_Mutez.gt(ofStringUnsafe("245"), ofStringUnsafe("123")))
    |> toEqual(true)
  });
});

describe("lt", () => {
  test("(0,0)", _ => {
    expect(Tezos_Mutez.lt(Tezos_Mutez.zero, Tezos_Mutez.zero))
    |> toEqual(false)
  });
  test("(0,1)", _ => {
    expect(Tezos_Mutez.lt(Tezos_Mutez.zero, Tezos_Mutez.one))
    |> toEqual(true)
  });
  test("(1,0)", _ => {
    expect(Tezos_Mutez.lt(Tezos_Mutez.one, Tezos_Mutez.zero))
    |> toEqual(false)
  });

  test("(4,10)", _ => {
    expect(Tezos_Mutez.lt(ofStringUnsafe("4"), ofStringUnsafe("10")))
    |> toEqual(true)
  });

  test("(13,27)", _ => {
    expect(Tezos_Mutez.lt(ofStringUnsafe("13"), ofStringUnsafe("27")))
    |> toEqual(true)
  });

  test("(56,56)", _ => {
    expect(Tezos_Mutez.lt(ofStringUnsafe("56"), ofStringUnsafe("56")))
    |> toEqual(false)
  });

  test("(123,245)", _ => {
    expect(Tezos_Mutez.lt(ofStringUnsafe("123"), ofStringUnsafe("245")))
    |> toEqual(true)
  });

  test("(245,123)", _ => {
    expect(Tezos_Mutez.lt(ofStringUnsafe("245"), ofStringUnsafe("123")))
    |> toEqual(false)
  });
});

describe("min", () => {
  test("(0,0)", _ => {
    expect(Tezos_Mutez.min(Tezos_Mutez.zero, Tezos_Mutez.zero))
    |> toEqual(Tezos_Mutez.zero)
  });
  test("(0,1)", _ => {
    expect(Tezos_Mutez.min(Tezos_Mutez.zero, Tezos_Mutez.one))
    |> toEqual(Tezos_Mutez.zero)
  });
  test("(1,0)", _ => {
    expect(Tezos_Mutez.min(Tezos_Mutez.one, Tezos_Mutez.zero))
    |> toEqual(Tezos_Mutez.zero)
  });

  test("(4,10)", _ => {
    expect(Tezos_Mutez.min(ofStringUnsafe("4"), ofStringUnsafe("10")))
    |> toEqual(ofStringUnsafe("4"))
  });

  test("(13,27)", _ => {
    expect(Tezos_Mutez.min(ofStringUnsafe("13"), ofStringUnsafe("27")))
    |> toEqual(ofStringUnsafe("13"))
  });

  test("(56,56)", _ => {
    expect(Tezos_Mutez.min(ofStringUnsafe("56"), ofStringUnsafe("56")))
    |> toEqual(ofStringUnsafe("56"))
  });

  test("(123,245)", _ => {
    expect(Tezos_Mutez.min(ofStringUnsafe("123"), ofStringUnsafe("245")))
    |> toEqual(ofStringUnsafe("123"))
  });

  test("(245,123)", _ => {
    expect(Tezos_Mutez.min(ofStringUnsafe("245"), ofStringUnsafe("123")))
    |> toEqual(ofStringUnsafe("123"))
  });
});

describe("max", () => {
  test("(0,0)", _ => {
    expect(Tezos_Mutez.max(Tezos_Mutez.zero, Tezos_Mutez.zero))
    |> toEqual(Tezos_Mutez.zero)
  });
  test("(0,1)", _ => {
    expect(Tezos_Mutez.max(Tezos_Mutez.zero, Tezos_Mutez.one))
    |> toEqual(Tezos_Mutez.one)
  });
  test("(1,0)", _ => {
    expect(Tezos_Mutez.max(Tezos_Mutez.one, Tezos_Mutez.zero))
    |> toEqual(Tezos_Mutez.one)
  });

  test("(4,10)", _ => {
    expect(Tezos_Mutez.max(ofStringUnsafe("4"), ofStringUnsafe("10")))
    |> toEqual(ofStringUnsafe("10"))
  });

  test("(13,27)", _ => {
    expect(Tezos_Mutez.max(ofStringUnsafe("13"), ofStringUnsafe("27")))
    |> toEqual(ofStringUnsafe("27"))
  });

  test("(56,56)", _ => {
    expect(Tezos_Mutez.max(ofStringUnsafe("56"), ofStringUnsafe("56")))
    |> toEqual(ofStringUnsafe("56"))
  });

  test("(123,245)", _ => {
    expect(Tezos_Mutez.max(ofStringUnsafe("123"), ofStringUnsafe("245")))
    |> toEqual(ofStringUnsafe("245"))
  });

  test("(245,123)", _ => {
    expect(Tezos_Mutez.max(ofStringUnsafe("245"), ofStringUnsafe("123")))
    |> toEqual(ofStringUnsafe("245"))
  });
});

describe("encode", () => {
  test("0", _ => {
    expect(Tezos_Mutez.encode(Tezos_Mutez.zero))
    |> toEqual(Json.Encode.string("0"))
  });

  test("1", _ => {
    expect(Tezos_Mutez.encode(Tezos_Mutez.one))
    |> toEqual(Json.Encode.string("1"))
  });

  test("123", _ => {
    expect(Tezos_Mutez.encode(ofStringUnsafe("123")))
    |> toEqual(Json.Encode.string("123"))
  });
});

describe("decode", () => {
  test("0", _ => {
    expect(Tezos_Mutez.decode(Json.Encode.string("0")))
    |> toEqual(Belt.Result.Ok(Tezos_Mutez.zero))
  });

  test("1", _ => {
    expect(Tezos_Mutez.decode(Json.Encode.string("1")))
    |> toEqual(Belt.Result.Ok(Tezos_Mutez.one))
  });

  test("23", _ => {
    expect(Tezos_Mutez.decode(Json.Encode.string("23")))
    |> toEqual(Belt.Result.Ok(ofStringUnsafe("23")))
  });

  test("abc", _ => {
    expect(Tezos_Mutez.decode(Json.Encode.string("abc")))
    |> toEqual(Belt.Result.Error("Mutez.decode failed: abc"))
  });
});
