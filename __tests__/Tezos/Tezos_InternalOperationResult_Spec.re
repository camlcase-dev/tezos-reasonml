open Jest;
open Expect;

describe("Tezos_InternalOperationResult.decode", () => {
  test("golden", () => {
    let json =
      Js.Json.parseExn(
        Node.Fs.readFileAsUtf8Sync(
          "__tests__/golden/internal_operation_result.json",
        ),
      );
    expect(
      Tezos_InternalOperationResult.decode(json)
      |> Belt.Result.isOk,
    )
    |> toEqual(true);
  })
});
