/**
 * "parameters"?:
 *   { "entrypoint": $alpha.entrypoint,
 *     "value": $micheline.alpha.michelson_v1.expression }
 */

/**
 * for JavaScript compatability
 */
module Raw = {
  type t = {
    [@bs.as "entrypoint"]
    entrypoint: string,
    [@bs.as "value"]
    value: Js.Json.t,
  };
};

type t = {
  entrypoint: Tezos_Entrypoint.t,
  value: Tezos_Expression.t,
};

/**
 * type conversions
 */

let toRaw = (t): Raw.t => {
  entrypoint: Tezos_Entrypoint.toString(t.entrypoint),
  value: Tezos_Expression.encode(t.value),
};

/**
 * JSON
 */

let decode = (json: Js.Json.t) => {
  switch (
    Json.Decode.(
      field(
        "entrypoint",
        a => Tezos_Util.unwrapResult(Tezos_Entrypoint.decode(a)),
        json,
      ),
      field(
        "value",
        a => Tezos_Util.unwrapResult(Tezos_Expression.decode(a)),
        json,
      ),
    )
  ) {
  | (entrypoint, value) => Belt.Result.Ok({entrypoint, value})
  | exception (Json.Decode.DecodeError(error)) => Belt.Result.Error(error)
  };
};
