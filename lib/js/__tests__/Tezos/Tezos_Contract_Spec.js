'use strict';

var Jest = require("@glennsl/bs-jest/lib/js/src/jest.js");
var Belt_Result = require("bs-platform/lib/js/belt_Result.js");
var Tezos_Contract = require("../../src/Tezos_Contract.js");
var Caml_exceptions = require("bs-platform/lib/js/caml_exceptions.js");

var TestError = Caml_exceptions.create("Tezos_Contract_Spec.TestError");

function ofStringUnsafe(s) {
  var s$1 = Tezos_Contract.ofString(s);
  if (!s$1.TAG) {
    return s$1._0;
  }
  throw {
        RE_EXN_ID: TestError,
        _1: s$1._0,
        Error: new Error()
      };
}

Jest.describe("ofString", (function (param) {
        Jest.test("KT1KYnDfY2s9FPtdLiP8ymusY6JZs9o4ugDr", (function (param) {
                return Jest.Expect.toEqual({
                            TAG: /* Ok */0,
                            _0: ofStringUnsafe("KT1KYnDfY2s9FPtdLiP8ymusY6JZs9o4ugDr")
                          }, Jest.Expect.expect(Tezos_Contract.ofString("KT1KYnDfY2s9FPtdLiP8ymusY6JZs9o4ugDr")));
              }));
        Jest.test("KT1HzQofKBxzfiKoMzGbkxBgjis2mWnCtbC2", (function (param) {
                return Jest.Expect.toEqual({
                            TAG: /* Ok */0,
                            _0: ofStringUnsafe("KT1HzQofKBxzfiKoMzGbkxBgjis2mWnCtbC2")
                          }, Jest.Expect.expect(Tezos_Contract.ofString("KT1HzQofKBxzfiKoMzGbkxBgjis2mWnCtbC2")));
              }));
        Jest.test("KT1HzQofKBxzfiKoMzGbkxBgjis2mWnCtbC0", (function (param) {
                return Jest.Expect.toEqual({
                            TAG: /* Error */1,
                            _0: "Tezos_Contract.ofString: unexpected candidate string: KT1HzQofKBxzfiKoMzGbkxBgjis2mWnCtbC0"
                          }, Jest.Expect.expect(Tezos_Contract.ofString("KT1HzQofKBxzfiKoMzGbkxBgjis2mWnCtbC0")));
              }));
        return Jest.test("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i", (function (param) {
                      return Jest.Expect.toEqual({
                                  TAG: /* Error */1,
                                  _0: "Tezos_Contract.ofString: unexpected candidate string: tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i"
                                }, Jest.Expect.expect(Tezos_Contract.ofString("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i")));
                    }));
      }));

Jest.describe("toString", (function (param) {
        return Jest.test("KT1LN4LPSqTMS7Sd2CJw4bbDGRkMv2t68Fy9", (function (param) {
                      return Jest.Expect.toEqual({
                                  TAG: /* Ok */0,
                                  _0: "KT1LN4LPSqTMS7Sd2CJw4bbDGRkMv2t68Fy9"
                                }, Jest.Expect.expect(Belt_Result.map(Tezos_Contract.ofString("KT1LN4LPSqTMS7Sd2CJw4bbDGRkMv2t68Fy9"), Tezos_Contract.toString)));
                    }));
      }));

Jest.describe("unpack", (function (param) {
        Jest.test("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn", (function (param) {
                return Jest.Expect.toEqual(undefined, Jest.Expect.expect(Tezos_Contract.unpack("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn")));
              }));
        return Jest.test("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i", (function (param) {
                      return Jest.Expect.toEqual(undefined, Jest.Expect.expect(Tezos_Contract.unpack("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i")));
                    }));
      }));

Jest.describe("roundtrip pack and unpack", (function (param) {
        Jest.test("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn", (function (param) {
                return Jest.Expect.toEqual(ofStringUnsafe("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn"), Jest.Expect.expect(Tezos_Contract.unpack(Tezos_Contract.pack(ofStringUnsafe("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn")))));
              }));
        return Jest.test("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn", (function (param) {
                      return Jest.Expect.toEqual(ofStringUnsafe("KT1LN4LPSqTMS7Sd2CJw4bbDGRkMv2t68Fy9"), Jest.Expect.expect(Tezos_Contract.unpack(Tezos_Contract.pack(ofStringUnsafe("KT1LN4LPSqTMS7Sd2CJw4bbDGRkMv2t68Fy9")))));
                    }));
      }));

Jest.describe("decode", (function (param) {
        Jest.test("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn", (function (param) {
                return Jest.Expect.toEqual({
                            TAG: /* Ok */0,
                            _0: ofStringUnsafe("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn")
                          }, Jest.Expect.expect(Tezos_Contract.decode("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn")));
              }));
        Jest.test("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i", (function (param) {
                return Jest.Expect.toEqual({
                            TAG: /* Error */1,
                            _0: "Tezos_Contract.decode failed: string is not an contract: tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i"
                          }, Jest.Expect.expect(Tezos_Contract.decode("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i")));
              }));
        return Jest.test("123", (function (param) {
                      return Jest.Expect.toEqual({
                                  TAG: /* Error */1,
                                  _0: "Tezos_Contract.decode failed: Expected string, got 123"
                                }, Jest.Expect.expect(Tezos_Contract.decode(123)));
                    }));
      }));

Jest.describe("roundtrip encode and decode", (function (param) {
        return Jest.test("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn", (function (param) {
                      return Jest.Expect.toEqual({
                                  TAG: /* Ok */0,
                                  _0: ofStringUnsafe("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn")
                                }, Jest.Expect.expect(Tezos_Contract.decode(Tezos_Contract.encode(ofStringUnsafe("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn")))));
                    }));
      }));

exports.TestError = TestError;
exports.ofStringUnsafe = ofStringUnsafe;
/*  Not a pure module */
