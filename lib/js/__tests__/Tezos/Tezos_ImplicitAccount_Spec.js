'use strict';

var Jest = require("@glennsl/bs-jest/lib/js/src/jest.js");
var Belt_Result = require("bs-platform/lib/js/belt_Result.js");
var Caml_exceptions = require("bs-platform/lib/js/caml_exceptions.js");
var Tezos_ImplicitAccount = require("../../src/Tezos_ImplicitAccount.js");

var TestError = Caml_exceptions.create("Tezos_ImplicitAccount_Spec.TestError");

function ofStringUnsafe(s) {
  var s$1 = Tezos_ImplicitAccount.ofString(s);
  if (!s$1.TAG) {
    return s$1._0;
  }
  throw {
        RE_EXN_ID: TestError,
        _1: s$1._0,
        Error: new Error()
      };
}

Jest.describe("ofString", (function (param) {
        Jest.test("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i", (function (param) {
                return Jest.Expect.toEqual({
                            TAG: /* Ok */0,
                            _0: ofStringUnsafe("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i")
                          }, Jest.Expect.expect(Tezos_ImplicitAccount.ofString("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i")));
              }));
        Jest.test("tz2L9474hvA2EXyNLi4u3YnqrKVf3GzeZ8tb", (function (param) {
                return Jest.Expect.toEqual({
                            TAG: /* Ok */0,
                            _0: ofStringUnsafe("tz2L9474hvA2EXyNLi4u3YnqrKVf3GzeZ8tb")
                          }, Jest.Expect.expect(Tezos_ImplicitAccount.ofString("tz2L9474hvA2EXyNLi4u3YnqrKVf3GzeZ8tb")));
              }));
        return Jest.test("tz2L9474hvA2EXyNLi4u3YnqrKVf3GzeZ8t0", (function (param) {
                      return Jest.Expect.toEqual({
                                  TAG: /* Error */1,
                                  _0: "Tezos_ImplicitAccount.ofString: unexpected candidate string: tz2L9474hvA2EXyNLi4u3YnqrKVf3GzeZ8t0"
                                }, Jest.Expect.expect(Tezos_ImplicitAccount.ofString("tz2L9474hvA2EXyNLi4u3YnqrKVf3GzeZ8t0")));
                    }));
      }));

Jest.describe("toString", (function (param) {
        return Jest.test("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i", (function (param) {
                      return Jest.Expect.toEqual({
                                  TAG: /* Ok */0,
                                  _0: "tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i"
                                }, Jest.Expect.expect(Belt_Result.map(Tezos_ImplicitAccount.ofString("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i"), Tezos_ImplicitAccount.toString)));
                    }));
      }));

Jest.describe("roundtrip pack and unpack", (function (param) {
        Jest.test("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i", (function (param) {
                return Jest.Expect.toEqual(ofStringUnsafe("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i"), Jest.Expect.expect(Tezos_ImplicitAccount.unpack(Tezos_ImplicitAccount.pack(ofStringUnsafe("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i")))));
              }));
        return Jest.test("tz2L9474hvA2EXyNLi4u3YnqrKVf3GzeZ8tb", (function (param) {
                      return Jest.Expect.toEqual(ofStringUnsafe("tz2L9474hvA2EXyNLi4u3YnqrKVf3GzeZ8tb"), Jest.Expect.expect(Tezos_ImplicitAccount.unpack(Tezos_ImplicitAccount.pack(ofStringUnsafe("tz2L9474hvA2EXyNLi4u3YnqrKVf3GzeZ8tb")))));
                    }));
      }));

Jest.describe("decode", (function (param) {
        Jest.test("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i", (function (param) {
                return Jest.Expect.toEqual({
                            TAG: /* Ok */0,
                            _0: ofStringUnsafe("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i")
                          }, Jest.Expect.expect(Tezos_ImplicitAccount.decode("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i")));
              }));
        Jest.test("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn", (function (param) {
                return Jest.Expect.toEqual({
                            TAG: /* Error */1,
                            _0: "Tezos_ImplicitAccount.decode failed: string is not an implicit account: KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn"
                          }, Jest.Expect.expect(Tezos_ImplicitAccount.decode("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn")));
              }));
        return Jest.test("123", (function (param) {
                      return Jest.Expect.toEqual({
                                  TAG: /* Error */1,
                                  _0: "Tezos_ImplicitAccount.decode failed: Expected string, got 123"
                                }, Jest.Expect.expect(Tezos_ImplicitAccount.decode(123)));
                    }));
      }));

Jest.describe("roundtrip encode and decode", (function (param) {
        Jest.test("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i", (function (param) {
                return Jest.Expect.toEqual({
                            TAG: /* Ok */0,
                            _0: ofStringUnsafe("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i")
                          }, Jest.Expect.expect(Tezos_ImplicitAccount.decode(Tezos_ImplicitAccount.encode(ofStringUnsafe("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i")))));
              }));
        return Jest.test("tz2L9474hvA2EXyNLi4u3YnqrKVf3GzeZ8tb", (function (param) {
                      return Jest.Expect.toEqual({
                                  TAG: /* Ok */0,
                                  _0: ofStringUnsafe("tz2L9474hvA2EXyNLi4u3YnqrKVf3GzeZ8tb")
                                }, Jest.Expect.expect(Tezos_ImplicitAccount.decode(Tezos_ImplicitAccount.encode(ofStringUnsafe("tz2L9474hvA2EXyNLi4u3YnqrKVf3GzeZ8tb")))));
                    }));
      }));

exports.TestError = TestError;
exports.ofStringUnsafe = ofStringUnsafe;
/*  Not a pure module */
