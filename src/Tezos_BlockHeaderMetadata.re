/**
 * $block_header_metadata:
 *   { "protocol": "ProtoALphaALphaALphaALphaALphaALphaALphaALphaDdp3zK",
 *     "next_protocol": "ProtoALphaALphaALphaALphaALphaALphaALphaALphaDdp3zK",
 *     "test_chain_status": $test_chain_status,
 *     "max_operations_ttl": integer ∈ [-2^30-2, 2^30+2],
 *     "max_operation_data_length": integer ∈ [-2^30-2, 2^30+2],
 *     "max_block_header_length": integer ∈ [-2^30-2, 2^30+2],
 *     "max_operation_list_length":
 *       [ { "max_size": integer ∈ [-2^30-2, 2^30+2],
 *           "max_op"?: integer ∈ [-2^30-2, 2^30+2] } ... ],
 *     "baker": $Signature.Public_key_hash,
 *     "level":
 *       { "level":
 *           integer ∈ [-2^31-2, 2^31+2]
 *           /* The level of the block relative to genesis. This is also the
 *              Shell's notion of level */,
 *         "level_position":
 *           integer ∈ [-2^31-2, 2^31+2]
 *           /* The level of the block relative to the block that starts
 *              protocol alpha. This is specific to the protocol alpha. Other
 *              protocols might or might not include a similar notion. */,
 *         "cycle":
 *           integer ∈ [-2^31-2, 2^31+2]
 *           /* The current cycle's number. Note that cycles are a
 *              protocol-specific notion. As a result, the cycle number starts
 *              at 0 with the first block of protocol alpha. */,
 *         "cycle_position":
 *           integer ∈ [-2^31-2, 2^31+2]
 *           /* The current level of the block relative to the first block of
 *              the current cycle. */,
 *         "voting_period":
 *           integer ∈ [-2^31-2, 2^31+2]
 *           /* The current voting period's index. Note that cycles are a
 *              protocol-specific notion. As a result, the voting period index
 *              starts at 0 with the first block of protocol alpha. */,
 *         "voting_period_position":
 *           integer ∈ [-2^31-2, 2^31+2]
 *           /* The current level of the block relative to the first block of
 *              the current voting period. */,
 *         "expected_commitment":
 *           boolean
 *           /* Tells wether the baker of this block has to commit a seed
 *              nonce hash. */ },
 *     "voting_period_kind":
 *       "proposal" || "testing_vote" || "testing" || "promotion_vote",
 *     "nonce_hash": $cycle_nonce /* Some */ || null /* None */,
 *     "consumed_gas": $positive_bignum,
 *     "deactivated": [ $Signature.Public_key_hash ... ],
 *     "balance_updates": $alpha.operation_metadata.alpha.balance_updates }
 */
