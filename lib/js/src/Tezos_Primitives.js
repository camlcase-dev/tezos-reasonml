'use strict';

var Json_decode = require("@glennsl/bs-json/lib/js/src/Json_decode.js");
var Caml_js_exceptions = require("bs-platform/lib/js/caml_js_exceptions.js");
var Tezos_PrimitiveData = require("./Tezos_PrimitiveData.js");
var Tezos_PrimitiveType = require("./Tezos_PrimitiveType.js");
var Tezos_PrimitiveInstruction = require("./Tezos_PrimitiveInstruction.js");

function instr(x) {
  return {
          TAG: /* PrimitiveInstruction */0,
          _0: x
        };
}

function data(x) {
  return {
          TAG: /* PrimitiveData */1,
          _0: x
        };
}

function type_(x) {
  return {
          TAG: /* PrimitiveType */2,
          _0: x
        };
}

function encode(primitives) {
  switch (primitives.TAG | 0) {
    case /* PrimitiveInstruction */0 :
        return Tezos_PrimitiveInstruction.encode(primitives._0);
    case /* PrimitiveData */1 :
        return Tezos_PrimitiveData.encode(primitives._0);
    case /* PrimitiveType */2 :
        return Tezos_PrimitiveType.encode(primitives._0);
    
  }
}

function decode(json) {
  var exit = 0;
  var primitiveInstruction;
  try {
    primitiveInstruction = Tezos_PrimitiveInstruction.decode(json);
    exit = 1;
  }
  catch (raw_error){
    var error = Caml_js_exceptions.internalToOCamlException(raw_error);
    if (error.RE_EXN_ID === Json_decode.DecodeError) {
      return {
              TAG: /* Error */1,
              _0: "Primitives.decode: " + error._1
            };
    }
    throw error;
  }
  if (exit === 1) {
    if (!primitiveInstruction.TAG) {
      return {
              TAG: /* Ok */0,
              _0: {
                TAG: /* PrimitiveInstruction */0,
                _0: primitiveInstruction._0
              }
            };
    }
    var exit$1 = 0;
    var primitiveData;
    try {
      primitiveData = Tezos_PrimitiveData.decode(json);
      exit$1 = 2;
    }
    catch (raw_error$1){
      var error$1 = Caml_js_exceptions.internalToOCamlException(raw_error$1);
      if (error$1.RE_EXN_ID === Json_decode.DecodeError) {
        return {
                TAG: /* Error */1,
                _0: "Primitives.decode: " + error$1._1
              };
      }
      throw error$1;
    }
    if (exit$1 === 2) {
      if (!primitiveData.TAG) {
        return {
                TAG: /* Ok */0,
                _0: {
                  TAG: /* PrimitiveData */1,
                  _0: primitiveData._0
                }
              };
      }
      var exit$2 = 0;
      var primitiveType;
      try {
        primitiveType = Tezos_PrimitiveType.decode(json);
        exit$2 = 3;
      }
      catch (raw_error$2){
        var error$2 = Caml_js_exceptions.internalToOCamlException(raw_error$2);
        if (error$2.RE_EXN_ID === Json_decode.DecodeError) {
          return {
                  TAG: /* Error */1,
                  _0: "Primitives.decode: " + error$2._1
                };
        }
        throw error$2;
      }
      if (exit$2 === 3) {
        if (primitiveType.TAG) {
          return {
                  TAG: /* Error */1,
                  _0: "Primitives.decode: " + primitiveType._0
                };
        } else {
          return {
                  TAG: /* Ok */0,
                  _0: {
                    TAG: /* PrimitiveType */2,
                    _0: primitiveType._0
                  }
                };
        }
      }
      
    }
    
  }
  
}

exports.instr = instr;
exports.data = data;
exports.type_ = type_;
exports.encode = encode;
exports.decode = decode;
/* No side effect */
