/**
 * $block_hash:
 *  /* A block identifier (Base58Check-encoded) */
 *  $unistring
 */

type t =
  | BlockHash(string);

/**
 * type conversions
 */

let ofString = (str: string): t => {
  BlockHash(str);
};

let toString = (t: t): string => {
  switch (t) {
  | BlockHash(str) => str
  };
};

/**
 * JSON encoding and decoding
 */

let encode = (t: t): Js.Json.t => {
  switch (t) {
  | BlockHash(str) => Json.Encode.string(str)
  };
};

let decode = (json: Js.Json.t): Belt.Result.t(t, string) => {
  switch (Json.Decode.string(json)) {
  | v => Belt.Result.Ok(BlockHash(v))
  | exception (Json.Decode.DecodeError(error)) =>
    Belt.Result.Error("Tezos_BlockHash.decode failed: " ++ error)
  };
};
