/**
 * $alpha.entrypoint:
 *   /* entrypoint
 *      Named entrypoint to a Michelson smart contract */
 *   "default"
 *   || "root"
 *   || "do"
 *   || "set_delegate"
 *   || "remove_delegate"
 *   || string
 *   /* named */
 */

type t =
  | Default
  | Root
  | Do
  | SetDelegate
  | RemoveDelegate
  | Named(string);

/**
 * type conversions
 */

let toString = (t: t): string => {
  switch (t) {
  | Default => "default"
  | Root => "root"
  | Do => "do"
  | SetDelegate => "set_delegate"
  | RemoveDelegate => "remove_delegate"
  | Named(named) => named
  };
};

/**
 * JSON
 */

let encode = (t: t): Js.Json.t => {
  switch (t) {
  | Default => Json.Encode.string("default")
  | Root => Json.Encode.string("root")
  | Do => Json.Encode.string("do")
  | SetDelegate => Json.Encode.string("set_delegate")
  | RemoveDelegate => Json.Encode.string("remove_delegate")
  | Named(named) => Json.Encode.string(named)
  };
};

let decode = (json: Js.Json.t) => {
  switch (Json.Decode.string(json)) {
  | "default" => Belt.Result.Ok(Default)
  | "root" => Belt.Result.Ok(Root)
  | "do" => Belt.Result.Ok(Do)
  | "set_delegate" => Belt.Result.Ok(SetDelegate)
  | "remove_delegate" => Belt.Result.Ok(RemoveDelegate)
  | str => Belt.Result.Ok(Named(str))
  | exception (Json.Decode.DecodeError(error)) => Belt.Result.Error(error)
  };
};
