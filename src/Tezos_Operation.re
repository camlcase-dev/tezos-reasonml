/**
 * $operation:
 *   { "protocol": "ProtoALphaALphaALphaALphaALphaALphaALphaALphaDdp3zK",
 *     "chain_id": $Chain_id,
 *     "hash": $Operation_hash,
 *     "branch": $block_hash,
 *     "contents":
 *       [ $alpha.operation.alpha.operation_contents_and_result ... ],
 *     "signature"?: $Signature }
 *   || { "protocol": "ProtoALphaALphaALphaALphaALphaALphaALphaALphaDdp3zK",
 *        "chain_id": $Chain_id,
 *        "hash": $Operation_hash,
 *        "branch": $block_hash,
 *        "contents": [ $alpha.operation.alpha.contents ... ],
 *        "signature"?: $Signature }
 *   || { "protocol": "ProtoALphaALphaALphaALphaALphaALphaALphaALphaDdp3zK",
 *        "chain_id": $Chain_id,
 *        "hash": $Operation_hash,
 *        "branch": $block_hash,
 *        "contents": [ $alpha.operation.alpha.contents ... ],
 *        "signature": $Signature }
 */
module Content = {
  type t =
    | ContentsAndResults(Tezos_OperationContentsAndResult.t)
    | Contents(Tezos_OperationContents.t);

  let decode = (json: Js.Json.t) => {
    switch (Tezos_OperationContentsAndResult.decode(json)) {
    | Belt.Result.Ok(o) => Belt.Result.Ok(ContentsAndResults(o))
    | Belt.Result.Error(_error) =>
      switch (Tezos_OperationContents.decode(json)) {
      | Belt.Result.Ok(o) => Belt.Result.Ok(Contents(o))
      | Belt.Result.Error(_error) =>
        Belt.Result.Error("Unable to decode operation")
      }
    };
  };
};

type t = {
  chainId: Tezos_ChainId.t,
  hash: Tezos_OperationHash.t,
  branch: Tezos_BlockHash.t,
  contents: list(Content.t),
};

/**
 * JSON
 */

let decode = (json: Js.Json.t) => {
  switch (
    Json.Decode.(
      field(
        "chain_id",
        a => Tezos_Util.unwrapResult(Tezos_ChainId.decode(a)),
        json,
      ),
      field(
        "hash",
        a => Tezos_Util.unwrapResult(Tezos_OperationHash.decode(a)),
        json,
      ),
      field(
        "branch",
        a => Tezos_Util.unwrapResult(Tezos_BlockHash.decode(a)),
        json,
      ),
      field(
        "contents",
        list(a =>
          Tezos_Util.unwrapResult(Content.decode(a))
        ),
        json,
      ),
    )
  ) {
  | (chainId, hash, branch, contents) =>
    Belt.Result.Ok({chainId, hash, branch, contents})
  | exception (Json.Decode.DecodeError(error)) => Belt.Result.Error(error)
  };
};
