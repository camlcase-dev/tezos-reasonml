open Jest;
open Expect;

exception TestError(string);

let ofStringUnsafe = s => {
  switch (Tezos_ImplicitAccount.ofString(s)) {
  | Belt.Result.Ok(s) => s
  | Belt.Result.Error(err) => raise @@ TestError(err)
  };
};

describe("ofString", () => {
  test("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i", _ => {
    expect(
      Tezos_ImplicitAccount.ofString("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i"),
    )
    |> toEqual(
         Belt.Result.Ok(
           ofStringUnsafe("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i"),
         ),
       )
  });

  test("tz2L9474hvA2EXyNLi4u3YnqrKVf3GzeZ8tb", _ => {
    expect(
      Tezos_ImplicitAccount.ofString("tz2L9474hvA2EXyNLi4u3YnqrKVf3GzeZ8tb"),
    )
    |> toEqual(
         Belt.Result.Ok(
           ofStringUnsafe("tz2L9474hvA2EXyNLi4u3YnqrKVf3GzeZ8tb"),
         ),
       )
  });

  test("tz2L9474hvA2EXyNLi4u3YnqrKVf3GzeZ8t0", _ => {
    expect(
      Tezos_ImplicitAccount.ofString("tz2L9474hvA2EXyNLi4u3YnqrKVf3GzeZ8t0"),
    )
    |> toEqual(
         Belt.Result.Error(
           "Tezos_ImplicitAccount.ofString: unexpected candidate string: tz2L9474hvA2EXyNLi4u3YnqrKVf3GzeZ8t0",
         ),
       )
  });
});

describe("toString", () => {
  test("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i", _ => {
    expect(
      Belt.Result.map(
        Tezos_ImplicitAccount.ofString(
          "tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i",
        ),
        Tezos_ImplicitAccount.toString,
      ),
    )
    |> toEqual(Belt.Result.Ok("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i"))
  })
});

describe("roundtrip pack and unpack", () => {
  test("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i", _ => {
    expect(
      ofStringUnsafe("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i")
      |> Tezos_ImplicitAccount.pack
      |> Tezos_ImplicitAccount.unpack,
    )
    |> toEqual(Some(ofStringUnsafe("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i")))
  });

  test("tz2L9474hvA2EXyNLi4u3YnqrKVf3GzeZ8tb", _ => {
    expect(
      ofStringUnsafe("tz2L9474hvA2EXyNLi4u3YnqrKVf3GzeZ8tb")
      |> Tezos_ImplicitAccount.pack
      |> Tezos_ImplicitAccount.unpack,
    )
    |> toEqual(Some(ofStringUnsafe("tz2L9474hvA2EXyNLi4u3YnqrKVf3GzeZ8tb")))
  });
});

describe("decode", () => {
  test("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i", _ => {
    expect(
      Json.Encode.string("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i")
      |> Tezos_ImplicitAccount.decode,
    )
    |> toEqual(
         Belt.Result.Ok(
           ofStringUnsafe("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i"),
         ),
       )
  });

  test("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn", _ => {
    expect(
      Json.Encode.string("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn")
      |> Tezos_ImplicitAccount.decode,
    )
    |> toEqual(
         Belt.Result.Error(
           "Tezos_ImplicitAccount.decode failed: string is not an implicit account: KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn",
         ),
       )
  });

  test("123", _ => {
    expect(Json.Encode.int(123) |> Tezos_ImplicitAccount.decode)
    |> toEqual(
         Belt.Result.Error(
           "Tezos_ImplicitAccount.decode failed: Expected string, got 123",
         ),
       )
  });
});

describe("roundtrip encode and decode", () => {
  test("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i", _ => {
    expect(
      ofStringUnsafe("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i")
      |> Tezos_ImplicitAccount.encode
      |> Tezos_ImplicitAccount.decode,
    )
    |> toEqual(
         Belt.Result.Ok(
           ofStringUnsafe("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i"),
         ),
       )
  });

  test("tz2L9474hvA2EXyNLi4u3YnqrKVf3GzeZ8tb", _ => {
    expect(
      ofStringUnsafe("tz2L9474hvA2EXyNLi4u3YnqrKVf3GzeZ8tb")
      |> Tezos_ImplicitAccount.encode
      |> Tezos_ImplicitAccount.decode,
    )
    |> toEqual(
         Belt.Result.Ok(
           ofStringUnsafe("tz2L9474hvA2EXyNLi4u3YnqrKVf3GzeZ8tb"),
         ),
       )
  });
});

/* describe("Belt.Id.cmp", () => { */
/*   test("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i < tz2L9474hvA2EXyNLi4u3YnqrKVf3GzeZ8tb", _ => { */
/*     expect( */
/*       Tezos_ImplicitAccount.Comparable.eq(ofStringUnsafe("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i"), ofStringUnsafe("tz2L9474hvA2EXyNLi4u3YnqrKVf3GzeZ8tb")) */
/*     ) */
/*     |> toEqual(0) */
/*   }); */

/* }); */

/* tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i */
/* tz1ZWBvw1g9PgfojpLr9rTbpRtvkQAjaxNda */
/* tz1MQSM9t3jE3b4Nj7T4LugUWcsUokk1y1AW */
/* tz1T61EF6Lr4reLHSnB1tunx2PvCDmbchbWE */
/* tz1P1y9ZLPPzn5uQjWD5qmjHz9hUKTsqaxcP */
/* tz2L9474hvA2EXyNLi4u3YnqrKVf3GzeZ8tb */
