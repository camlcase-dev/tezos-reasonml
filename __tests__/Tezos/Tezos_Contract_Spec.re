open Jest;
open Expect;

exception TestError(string);

let ofStringUnsafe = s => {
  switch (Tezos_Contract.ofString(s)) {
  | Belt.Result.Ok(s) => s
  | Belt.Result.Error(err) => raise @@ TestError(err)
  };
};

describe("ofString", () => {
  test("KT1KYnDfY2s9FPtdLiP8ymusY6JZs9o4ugDr", _ => {
    expect(Tezos_Contract.ofString("KT1KYnDfY2s9FPtdLiP8ymusY6JZs9o4ugDr"))
    |> toEqual(
         Belt.Result.Ok(
           ofStringUnsafe("KT1KYnDfY2s9FPtdLiP8ymusY6JZs9o4ugDr"),
         ),
       )
  });

  test("KT1HzQofKBxzfiKoMzGbkxBgjis2mWnCtbC2", _ => {
    expect(Tezos_Contract.ofString("KT1HzQofKBxzfiKoMzGbkxBgjis2mWnCtbC2"))
    |> toEqual(
         Belt.Result.Ok(
           ofStringUnsafe("KT1HzQofKBxzfiKoMzGbkxBgjis2mWnCtbC2"),
         ),
       )
  });

  test("KT1HzQofKBxzfiKoMzGbkxBgjis2mWnCtbC0", _ => {
    expect(Tezos_Contract.ofString("KT1HzQofKBxzfiKoMzGbkxBgjis2mWnCtbC0"))
    |> toEqual(
         Belt.Result.Error(
           "Tezos_Contract.ofString: unexpected candidate string: KT1HzQofKBxzfiKoMzGbkxBgjis2mWnCtbC0",
         ),
       )
  });

  test("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i", _ => {
    expect(Tezos_Contract.ofString("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i"))
    |> toEqual(
         Belt.Result.Error(
           "Tezos_Contract.ofString: unexpected candidate string: tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i",
         ),
       )
  });
});

describe("toString", () => {
  test("KT1LN4LPSqTMS7Sd2CJw4bbDGRkMv2t68Fy9", _ => {
    expect(
      Belt.Result.map(
        Tezos_Contract.ofString("KT1LN4LPSqTMS7Sd2CJw4bbDGRkMv2t68Fy9"),
        Tezos_Contract.toString,
      ),
    )
    |> toEqual(Belt.Result.Ok("KT1LN4LPSqTMS7Sd2CJw4bbDGRkMv2t68Fy9"))
  })
});

describe("unpack", () => {
  test("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn", _ => {
    expect("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn" |> Tezos_Contract.unpack)
    |> toEqual(None)
  });

  test("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i", _ => {
    expect("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i" |> Tezos_Contract.unpack)
    |> toEqual(None)
  });
});

describe("roundtrip pack and unpack", () => {
  test("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn", _ => {
    expect(
      ofStringUnsafe("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn")
      |> Tezos_Contract.pack
      |> Tezos_Contract.unpack,
    )
    |> toEqual(Some(ofStringUnsafe("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn")))
  });

  test("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn", _ => {
    expect(
      ofStringUnsafe("KT1LN4LPSqTMS7Sd2CJw4bbDGRkMv2t68Fy9")
      |> Tezos_Contract.pack
      |> Tezos_Contract.unpack,
    )
    |> toEqual(Some(ofStringUnsafe("KT1LN4LPSqTMS7Sd2CJw4bbDGRkMv2t68Fy9")))
  });
});

describe("decode", () => {
  test("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn", _ => {
    expect(
      Json.Encode.string("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn")
      |> Tezos_Contract.decode,
    )
    |> toEqual(
         Belt.Result.Ok(
           ofStringUnsafe("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn"),
         ),
       )
  });

  test("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i", _ => {
    expect(
      Json.Encode.string("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i")
      |> Tezos_Contract.decode,
    )
    |> toEqual(Belt.Result.Error("Tezos_Contract.decode failed: string is not an contract: tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i"))
  });

  test("123", _ => {
    expect(
      Json.Encode.int(123)
      |> Tezos_Contract.decode,
    )
    |> toEqual(Belt.Result.Error("Tezos_Contract.decode failed: Expected string, got 123"))
  });  
});

describe("roundtrip encode and decode", () => {
  test("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn", _ => {
    expect(
      ofStringUnsafe("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn")
      |> Tezos_Contract.encode
      |> Tezos_Contract.decode,
    )
    |> toEqual(
         Belt.Result.Ok(
           ofStringUnsafe("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn"),
         ),
       )
  })
});
