'use strict';

var Fetch = require("bs-fetch/lib/js/src/Fetch.js");
var Belt_List = require("bs-platform/lib/js/belt_List.js");
var Json_decode = require("@glennsl/bs-json/lib/js/src/Json_decode.js");
var Tezos_Block = require("./Tezos_Block.js");
var Tezos_Mutez = require("./Tezos_Mutez.js");
var Tezos_Address = require("./Tezos_Address.js");
var Tezos_Expression = require("./Tezos_Expression.js");
var Caml_js_exceptions = require("bs-platform/lib/js/caml_js_exceptions.js");

function toRoute(baseUrl, route) {
  var tmp;
  if (typeof route === "number") {
    tmp = "/network/version";
  } else {
    switch (route.TAG | 0) {
      case /* Balance */0 :
          tmp = "/chains/" + (route._0 + ("/blocks/" + (route._1 + ("/context/contracts/" + (route._2 + "/balance")))));
          break;
      case /* BigMap */1 :
          tmp = "/chains/" + (route._0 + ("/blocks/" + (route._1 + ("/context/big_maps/" + (String(route._2._0) + ("/" + route._3))))));
          break;
      case /* HeadBlockId */2 :
          tmp = "/chains/" + (route._0 + "/blocks");
          break;
      case /* Block */3 :
          tmp = "/chains/" + (route._0 + ("/blocks/" + route._1));
          break;
      case /* Storage */4 :
          tmp = "/chains/" + (route._0 + ("/blocks/" + (route._1 + ("/context/contracts/" + (route._2 + "/storage")))));
          break;
      case /* Delegate */5 :
          tmp = "/chains/" + (route._0 + ("/blocks/" + (route._1 + ("/context/contracts/" + (route._2 + "/delegate")))));
          break;
      
    }
  }
  return baseUrl + tmp;
}

function getBalance(baseUrl, chainId, blockId, contract) {
  var url = toRoute(baseUrl, {
        TAG: /* Balance */0,
        _0: chainId,
        _1: blockId,
        _2: contract
      });
  return fetch(url, Fetch.RequestInit.make(/* Get */0, [[
                            "Accept",
                            "application/json"
                          ]], undefined, undefined, undefined, undefined, /* Omit */0, undefined, undefined, undefined, undefined, undefined)(undefined)).then(function (prim) {
                  return prim.json();
                }).then(function (json) {
                return Promise.resolve(Tezos_Mutez.decode(json));
              }).catch(function (error) {
              console.log(error);
              return Promise.resolve({
                          TAG: /* Error */1,
                          _0: "Tezos_RPC.getBalance promise error."
                        });
            });
}

function getBigMapValue(baseUrl, chainId, blockId, bigMapId, scriptExpr) {
  var url = toRoute(baseUrl, {
        TAG: /* BigMap */1,
        _0: chainId,
        _1: blockId,
        _2: bigMapId,
        _3: scriptExpr
      });
  return fetch(url, Fetch.RequestInit.make(/* Get */0, [[
                          "Accept",
                          "application/json"
                        ]], undefined, undefined, undefined, /* CORS */3, /* Omit */0, undefined, undefined, undefined, undefined, undefined)(undefined)).then(function (response) {
                var status = response.status;
                if (status >= 200 && status < 300) {
                  return response.json().then(function (json) {
                              var expression = Tezos_Expression.decode(json);
                              var tmp;
                              tmp = expression.TAG ? ({
                                    TAG: /* Error */1,
                                    _0: expression._0
                                  }) : ({
                                    TAG: /* Ok */0,
                                    _0: expression._0
                                  });
                              return Promise.resolve(tmp);
                            });
                } else if (status === 404) {
                  return Promise.resolve({
                              TAG: /* Ok */0,
                              _0: undefined
                            });
                } else {
                  return Promise.resolve({
                              TAG: /* Error */1,
                              _0: "Tezos_RPC.getBigMapValue failed. Promise error."
                            });
                }
              }).catch(function (error) {
              console.log(error);
              return Promise.resolve({
                          TAG: /* Error */1,
                          _0: "Tezos_RPC.getBigMapValue failed. Promise error."
                        });
            });
}

function getFirst(xss) {
  var xs = Belt_List.head(xss);
  if (xs !== undefined) {
    return Belt_List.head(xs);
  }
  
}

function getHeadBlockId(baseUrl, chainId) {
  var url = toRoute(baseUrl, {
        TAG: /* HeadBlockId */2,
        _0: chainId
      });
  return fetch(url, Fetch.RequestInit.make(/* Get */0, [[
                            "Accept",
                            "application/json"
                          ]], undefined, undefined, undefined, undefined, /* Omit */0, undefined, undefined, undefined, undefined, undefined)(undefined)).then(function (prim) {
                  return prim.json();
                }).then(function (json) {
                var blocks;
                try {
                  blocks = Json_decode.list((function (a) {
                          return Json_decode.list(Json_decode.string, a);
                        }), json);
                }
                catch (raw_err){
                  var err = Caml_js_exceptions.internalToOCamlException(raw_err);
                  if (err.RE_EXN_ID === Json_decode.DecodeError) {
                    return Promise.resolve({
                                TAG: /* Error */1,
                                _0: err._1
                              });
                  }
                  throw err;
                }
                var block = getFirst(blocks);
                return Promise.resolve(block !== undefined ? ({
                                TAG: /* Ok */0,
                                _0: block
                              }) : ({
                                TAG: /* Error */1,
                                _0: "Tezos_RPC.getHeadBlockId return an empty list."
                              }));
              }).catch(function (error) {
              console.log(error);
              return Promise.resolve({
                          TAG: /* Error */1,
                          _0: "Tezos_RPC.getHeadBlockId failed. Promise error."
                        });
            });
}

function getBlock(baseUrl, chainId, blockId) {
  var url = toRoute(baseUrl, {
        TAG: /* Block */3,
        _0: chainId,
        _1: blockId
      });
  return fetch(url, Fetch.RequestInit.make(/* Get */0, [[
                            "Accept",
                            "application/json"
                          ]], undefined, undefined, undefined, undefined, /* Omit */0, undefined, undefined, undefined, undefined, undefined)(undefined)).then(function (prim) {
                  return prim.json();
                }).then(function (json) {
                return Promise.resolve(Tezos_Block.decode(json));
              }).catch(function (error) {
              console.log(error);
              return Promise.resolve({
                          TAG: /* Error */1,
                          _0: "Tezos_RPC.getBlock failed. Promise error."
                        });
            });
}

function getHeadBlock(baseUrl, chainId) {
  return getHeadBlockId(baseUrl, chainId).then(function (result) {
              if (result.TAG) {
                return Promise.resolve({
                            TAG: /* Error */1,
                            _0: result._0
                          });
              } else {
                return getBlock(baseUrl, chainId, result._0);
              }
            });
}

function getTimestamp(baseUrl) {
  return getHeadBlock(baseUrl, "main").then(function (result) {
              if (result.TAG) {
                return Promise.resolve({
                            TAG: /* Error */1,
                            _0: result._0
                          });
              } else {
                return Promise.resolve({
                            TAG: /* Ok */0,
                            _0: result._0.header.timestamp
                          });
              }
            });
}

function getStorage(baseUrl, chainId, blockId, contract) {
  var url = toRoute(baseUrl, {
        TAG: /* Storage */4,
        _0: chainId,
        _1: blockId,
        _2: contract
      });
  return fetch(url, Fetch.RequestInit.make(/* Get */0, [[
                            "Accept",
                            "application/json"
                          ]], undefined, undefined, undefined, undefined, /* Omit */0, undefined, undefined, undefined, undefined, undefined)(undefined)).then(function (prim) {
                  return prim.json();
                }).then(function (json) {
                return Promise.resolve(Tezos_Expression.decode(json));
              }).catch(function (error) {
              console.log(error);
              return Promise.resolve({
                          TAG: /* Error */1,
                          _0: "Tezos_RPC.getStorage failed. Promise error."
                        });
            });
}

function getDelegate(baseUrl, chainId, blockId, contract) {
  var url = toRoute(baseUrl, {
        TAG: /* Delegate */5,
        _0: chainId,
        _1: blockId,
        _2: contract
      });
  return fetch(url, Fetch.RequestInit.make(/* Get */0, [[
                          "Accept",
                          "application/json"
                        ]], undefined, undefined, undefined, undefined, /* Omit */0, undefined, undefined, undefined, undefined, undefined)(undefined)).then(function (response) {
                var status = response.status;
                if (status === 404) {
                  return Promise.resolve({
                              TAG: /* Ok */0,
                              _0: undefined
                            });
                } else if (status >= 200 && status < 300) {
                  return response.json().then(function (json) {
                              var ok = Tezos_Address.decode(json);
                              var tmp;
                              tmp = ok.TAG ? ({
                                    TAG: /* Error */1,
                                    _0: ok._0
                                  }) : ({
                                    TAG: /* Ok */0,
                                    _0: ok._0
                                  });
                              return Promise.resolve(tmp);
                            });
                } else {
                  return Promise.resolve({
                              TAG: /* Error */1,
                              _0: "Tezos_RPC.getDelegate failed. Unexpected network status:  " + String(status)
                            });
                }
              }).catch(function (error) {
              console.log(error);
              return Promise.resolve({
                          TAG: /* Error */1,
                          _0: "Tezos_RPC.getDelegate failed. Promise error."
                        });
            });
}

function getNetworkVersion(baseUrl) {
  var url = toRoute(baseUrl, /* NetworkVersion */0);
  return fetch(url, Fetch.RequestInit.make(/* Get */0, [[
                          "Accept",
                          "application/json"
                        ]], undefined, undefined, undefined, undefined, /* Omit */0, undefined, undefined, undefined, undefined, undefined)(undefined)).then(function (prim) {
                return prim.json();
              }).then(function (json) {
              var tmp;
              var exit = 0;
              var chainName;
              try {
                chainName = Json_decode.field("chain_name", Json_decode.string, json);
                exit = 1;
              }
              catch (raw_error){
                var error = Caml_js_exceptions.internalToOCamlException(raw_error);
                if (error.RE_EXN_ID === Json_decode.DecodeError) {
                  tmp = {
                    TAG: /* Error */1,
                    _0: error._1
                  };
                } else {
                  throw error;
                }
              }
              if (exit === 1) {
                tmp = {
                  TAG: /* Ok */0,
                  _0: chainName
                };
              }
              return Promise.resolve(tmp);
            });
}

exports.toRoute = toRoute;
exports.getBalance = getBalance;
exports.getBigMapValue = getBigMapValue;
exports.getFirst = getFirst;
exports.getHeadBlockId = getHeadBlockId;
exports.getBlock = getBlock;
exports.getHeadBlock = getHeadBlock;
exports.getTimestamp = getTimestamp;
exports.getStorage = getStorage;
exports.getDelegate = getDelegate;
exports.getNetworkVersion = getNetworkVersion;
/* Tezos_Block Not a pure module */
