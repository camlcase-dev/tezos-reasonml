'use strict';

var Belt_Id = require("bs-platform/lib/js/belt_Id.js");
var Json_decode = require("@glennsl/bs-json/lib/js/src/Json_decode.js");
var Caml_primitive = require("bs-platform/lib/js/caml_primitive.js");
var Caml_js_exceptions = require("bs-platform/lib/js/caml_js_exceptions.js");

var canDecodeBS58 = (function (value) {
     const bs58check = require('bs58check');
     if (bs58check.decodeUnsafe(value)) {
       return true;
     } else {
       return false;
     };
   });

function isImplicitAccountString(candidate) {
  if ((candidate.startsWith("tz1") || candidate.startsWith("tz2") || candidate.startsWith("tz3")) && candidate.length === 36) {
    return canDecodeBS58(candidate);
  } else {
    return false;
  }
}

function ofString(candidate) {
  if (isImplicitAccountString(candidate)) {
    return {
            TAG: /* Ok */0,
            _0: /* ImplicitAccount */{
              _0: candidate
            }
          };
  } else {
    return {
            TAG: /* Error */1,
            _0: "Tezos_ImplicitAccount.ofString: unexpected candidate string: " + candidate
          };
  }
}

function toString(t) {
  return t._0;
}

var packRaw = (function (input) {
     const bs58check = require('bs58check');
     const elliptic  = require('elliptic');

     var prefix;
     var tz_prefix;
     if (input.startsWith("tz1")) { // ed25519_public_key_hash
       prefix = new Uint8Array([6, 161, 159]);
       tz_prefix = '00';
     } else if (input.startsWith("tz2")) { // secp256k1_public_key_hash
       prefix = new Uint8Array([6, 161, 161]);
       tz_prefix = '01';
     } else if (input.startsWith("tz3")) { // p256_public_key_hash
       prefix = new Uint8Array([6, 161, 164]);
       tz_prefix = '02';
     };

     const bytes = '00' + tz_prefix + elliptic.utils.toHex(bs58check.decode(input).slice(prefix.length));
     const len = bytes.length / 2;
     const result = [];
     result.push('050a');
     result.push(len.toString(16).padStart(8, '0'));
     result.push(bytes);
     return result.join('');
   });

function pack(t) {
  return packRaw(t._0);
}

var unpackRaw = (function (input) {
     const bs58check = require('bs58check');
     const elliptic  = require('elliptic');

     if (input.length != 56) {
       return;
     }

     const prefix = input.slice(14,16);
     var prefix_bytes;
     if (prefix === '00') {
       prefix_bytes = new Uint8Array([6, 161, 159]);
     } else if (prefix === '01') {
       prefix_bytes = new Uint8Array([6, 161, 161]);
     } else if (prefix === '02') {
       prefix_bytes = new Uint8Array([6, 161, 164]);
     } else {
       return;
     }

     const bytes = new Uint8Array(elliptic.utils.toArray(input.slice(16), 'hex'));

     const bytes_concated = new Uint8Array(prefix_bytes.length + bytes.length);
     bytes_concated.set(prefix_bytes, 0);
     bytes_concated.set(bytes, prefix_bytes.length)

     return bs58check.encode(Buffer.from(bytes_concated));
   });

function unpack(s) {
  var s$1 = unpackRaw(s);
  if (s$1 == null) {
    return ;
  }
  var s$2 = ofString(s$1);
  if (s$2.TAG) {
    return ;
  } else {
    return s$2._0;
  }
}

var toScriptExprRaw = (function (input) {
     const blake    = require('blakejs');
     const elliptic = require('elliptic');
     const bs58check = require('bs58check');

     const prefix = new Uint8Array([13, 44, 64, 27]);

     var a = [];
     for (var i = 0, len = input.length; i < len; i+=2) {
       a.push(parseInt(input.substr(i,2),16));
     }

     const hex2buf =  new Uint8Array(a);

     const blakeHash = blake.blake2b(hex2buf, null, 32);

     const payloadAr = typeof blakeHash === 'string' ? Uint8Array.from(Buffer.from(blakeHash, 'hex')) : blakeHash;

     const n = new Uint8Array(prefix.length + payloadAr.length);
     n.set(prefix);
     n.set(payloadAr, prefix.length);

     return bs58check.encode(Buffer.from(n.buffer));
   });

function toScriptExpr(t) {
  return toScriptExprRaw(packRaw(t._0));
}

function encode(t) {
  return t._0;
}

function decode(json) {
  var v;
  try {
    v = Json_decode.string(json);
  }
  catch (raw_error){
    var error = Caml_js_exceptions.internalToOCamlException(raw_error);
    if (error.RE_EXN_ID === Json_decode.DecodeError) {
      return {
              TAG: /* Error */1,
              _0: "Tezos_ImplicitAccount.decode failed: " + error._1
            };
    }
    throw error;
  }
  if (isImplicitAccountString(v)) {
    return {
            TAG: /* Ok */0,
            _0: /* ImplicitAccount */{
              _0: v
            }
          };
  } else {
    return {
            TAG: /* Error */1,
            _0: "Tezos_ImplicitAccount.decode failed: string is not an implicit account: " + v
          };
  }
}

function cmp(c0, c1) {
  return Caml_primitive.caml_string_compare(c0._0, c1._0);
}

var Comparable = Belt_Id.MakeComparable({
      cmp: cmp
    });

var zero = /* ImplicitAccount */{
  _0: "tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU"
};

exports.zero = zero;
exports.canDecodeBS58 = canDecodeBS58;
exports.isImplicitAccountString = isImplicitAccountString;
exports.ofString = ofString;
exports.toString = toString;
exports.packRaw = packRaw;
exports.pack = pack;
exports.unpackRaw = unpackRaw;
exports.unpack = unpack;
exports.toScriptExprRaw = toScriptExprRaw;
exports.toScriptExpr = toScriptExpr;
exports.encode = encode;
exports.decode = decode;
exports.Comparable = Comparable;
/* Comparable Not a pure module */
