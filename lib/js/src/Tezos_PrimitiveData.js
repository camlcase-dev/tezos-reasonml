'use strict';

var Json_decode = require("@glennsl/bs-json/lib/js/src/Json_decode.js");
var Caml_js_exceptions = require("bs-platform/lib/js/caml_js_exceptions.js");

function encode(primitiveData) {
  switch (primitiveData) {
    case /* Elt */0 :
        return "Elt";
    case /* Right */1 :
        return "Right";
    case /* False */2 :
        return "False";
    case /* Unit */3 :
        return "Unit";
    case /* Some */4 :
        return "Some";
    case /* None */5 :
        return "None";
    case /* Left */6 :
        return "Left";
    case /* True */7 :
        return "True";
    case /* Pair */8 :
        return "Pair";
    
  }
}

function decode(json) {
  var str;
  try {
    str = Json_decode.string(json);
  }
  catch (raw_error){
    var error = Caml_js_exceptions.internalToOCamlException(raw_error);
    if (error.RE_EXN_ID === Json_decode.DecodeError) {
      return {
              TAG: /* Error */1,
              _0: "PrimitiveData.decode: " + error._1
            };
    }
    throw error;
  }
  switch (str) {
    case "Elt" :
        return {
                TAG: /* Ok */0,
                _0: /* Elt */0
              };
    case "False" :
        return {
                TAG: /* Ok */0,
                _0: /* False */2
              };
    case "Left" :
        return {
                TAG: /* Ok */0,
                _0: /* Left */6
              };
    case "None" :
        return {
                TAG: /* Ok */0,
                _0: /* None */5
              };
    case "Pair" :
        return {
                TAG: /* Ok */0,
                _0: /* Pair */8
              };
    case "Right" :
        return {
                TAG: /* Ok */0,
                _0: /* Right */1
              };
    case "Some" :
        return {
                TAG: /* Ok */0,
                _0: /* Some */4
              };
    case "True" :
        return {
                TAG: /* Ok */0,
                _0: /* True */7
              };
    case "Unit" :
        return {
                TAG: /* Ok */0,
                _0: /* Unit */3
              };
    default:
      return {
              TAG: /* Error */1,
              _0: "PrimitiveData.decode: " + str
            };
  }
}

exports.encode = encode;
exports.decode = decode;
/* No side effect */
