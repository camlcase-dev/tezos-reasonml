/**
 * $Signature:
 *   /* A Ed25519, Secp256k1 or P256 signature (Base58Check-encoded) */
 *   $unistring
 */

type t =
  | Signature(string);

/**
 * JSON encoding and decoding
 */

let encode = (t: t): Js.Json.t => {
  switch (t) {
  | Signature(str) => Json.Encode.string(str)
  };
};

let decode = (json: Js.Json.t): Belt.Result.t(t, string) => {
  switch (Json.Decode.string(json)) {
  | v => Belt.Result.Ok(Signature(v))
  | exception (Json.Decode.DecodeError(error)) =>
    Belt.Result.Error("Tezos_Signature.decode failed: " ++ error)
  };
};
