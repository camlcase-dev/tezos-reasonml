'use strict';

var Fs = require("fs");
var Jest = require("@glennsl/bs-jest/lib/js/src/jest.js");
var Curry = require("bs-platform/lib/js/curry.js");
var Belt_Result = require("bs-platform/lib/js/belt_Result.js");
var Tezos_OperationContentsAndResult = require("../../src/Tezos_OperationContentsAndResult.js");

Jest.describe("Tezos_OperationContentsAndResult.Transaction.decode", (function (param) {
        return Jest.test("golden", (function (param) {
                      var json = JSON.parse(Fs.readFileSync("__tests__/golden/operation_contents_and_result_transaction.json", "utf8"));
                      return Jest.Expect.toEqual(true, Jest.Expect.expect(Belt_Result.isOk(Tezos_OperationContentsAndResult.Transaction.decode(json))));
                    }));
      }));

Jest.describe("Tezos_Operation_Alpha.Contents.Result.Transaction.Metadata.decode", (function (param) {
        return Jest.test("golden", (function (param) {
                      var json = JSON.parse(Fs.readFileSync("__tests__/golden/operation_contents_and_result_transaction_metadata.json", "utf8"));
                      return Jest.Expect.toEqual(true, Jest.Expect.expect(Belt_Result.isOk(Curry._1(Tezos_OperationContentsAndResult.Transaction.Metadata.decode, json))));
                    }));
      }));

/*  Not a pure module */
