### 2020-11-02 -- v0.1.0
* Create safe types such as Mutez, ImplicitAccount, Contract, Address, etc. to
  encapsulate core values from the Tezos RPC.
* Support JSON encoding and decoding.
* Start working on pack and unpack for core types.
* Start working on script expression encode and decode for core types.
* Setup unit tests that test decoding values from the Tezos RPC.
