'use strict';

var List = require("bs-platform/lib/js/list.js");
var Tezos_Util = require("./Tezos_Util.js");
var Json_decode = require("@glennsl/bs-json/lib/js/src/Json_decode.js");
var Json_encode = require("@glennsl/bs-json/lib/js/src/Json_encode.js");
var Tezos_Fitness = require("./Tezos_Fitness.js");
var Tezos_BlockHash = require("./Tezos_BlockHash.js");
var Tezos_Signature = require("./Tezos_Signature.js");
var Tezos_Timestamp = require("./Tezos_Timestamp.js");
var Tezos_CycleNonce = require("./Tezos_CycleNonce.js");
var Tezos_ContextHash = require("./Tezos_ContextHash.js");
var Caml_js_exceptions = require("bs-platform/lib/js/caml_js_exceptions.js");
var Tezos_OperationListListHash = require("./Tezos_OperationListListHash.js");

function encode(t) {
  var seedNonceHash = t.seedNonceHash;
  return Json_encode.object_(List.concat({
                  hd: {
                    hd: [
                      "level",
                      t.level
                    ],
                    tl: {
                      hd: [
                        "proto",
                        t.proto
                      ],
                      tl: {
                        hd: [
                          "predecessor",
                          Tezos_BlockHash.encode(t.predecessor)
                        ],
                        tl: {
                          hd: [
                            "timestamp",
                            Tezos_Timestamp.encode(t.timestamp)
                          ],
                          tl: {
                            hd: [
                              "validation_pass",
                              t.validationPass
                            ],
                            tl: {
                              hd: [
                                "operations_hash",
                                Tezos_OperationListListHash.encode(t.operationsHash)
                              ],
                              tl: {
                                hd: [
                                  "fitness",
                                  Tezos_Fitness.encode(t.fitness)
                                ],
                                tl: {
                                  hd: [
                                    "context",
                                    Tezos_ContextHash.encode(t.context)
                                  ],
                                  tl: {
                                    hd: [
                                      "priority",
                                      t.priority
                                    ],
                                    tl: {
                                      hd: [
                                        "proof_of_work_nonce",
                                        t.proofOfWorkNonce
                                      ],
                                      tl: {
                                        hd: [
                                          "signature",
                                          Tezos_Signature.encode(t.signature)
                                        ],
                                        tl: /* [] */0
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  },
                  tl: {
                    hd: seedNonceHash !== undefined ? ({
                          hd: [
                            "seed_nonce_hash",
                            Tezos_CycleNonce.encode(seedNonceHash)
                          ],
                          tl: /* [] */0
                        }) : /* [] */0,
                    tl: /* [] */0
                  }
                }));
}

function decode(json) {
  var val;
  var val$1;
  var val$2;
  var val$3;
  var val$4;
  var val$5;
  var val$6;
  var val$7;
  var val$8;
  var val$9;
  var val$10;
  var val$11;
  try {
    val = Json_decode.field("level", Json_decode.$$int, json);
    val$1 = Json_decode.field("proto", Json_decode.$$int, json);
    val$2 = Json_decode.field("predecessor", (function (a) {
            return Tezos_Util.unwrapResult(Tezos_BlockHash.decode(a));
          }), json);
    val$3 = Json_decode.field("timestamp", (function (a) {
            return Tezos_Util.unwrapResult(Tezos_Timestamp.decode(a));
          }), json);
    val$4 = Json_decode.field("validation_pass", Json_decode.$$int, json);
    val$5 = Json_decode.field("operations_hash", (function (a) {
            return Tezos_Util.unwrapResult(Tezos_OperationListListHash.decode(a));
          }), json);
    val$6 = Json_decode.field("fitness", (function (a) {
            return Tezos_Util.unwrapResult(Tezos_Fitness.decode(a));
          }), json);
    val$7 = Json_decode.field("context", (function (a) {
            return Tezos_Util.unwrapResult(Tezos_ContextHash.decode(a));
          }), json);
    val$8 = Json_decode.field("priority", Json_decode.$$int, json);
    val$9 = Json_decode.field("proof_of_work_nonce", Json_decode.string, json);
    val$10 = Tezos_Util.optionalField("seed_nonce_hash", (function (a) {
            return Tezos_Util.unwrapResult(Tezos_CycleNonce.decode(a));
          }), json);
    val$11 = Json_decode.field("signature", (function (a) {
            return Tezos_Util.unwrapResult(Tezos_Signature.decode(a));
          }), json);
  }
  catch (raw_error){
    var error = Caml_js_exceptions.internalToOCamlException(raw_error);
    if (error.RE_EXN_ID === Json_decode.DecodeError) {
      return {
              TAG: /* Error */1,
              _0: error._1
            };
    }
    throw error;
  }
  return {
          TAG: /* Ok */0,
          _0: {
            level: val,
            proto: val$1,
            predecessor: val$2,
            timestamp: val$3,
            validationPass: val$4,
            operationsHash: val$5,
            fitness: val$6,
            context: val$7,
            priority: val$8,
            proofOfWorkNonce: val$9,
            seedNonceHash: val$10,
            signature: val$11
          }
        };
}

exports.encode = encode;
exports.decode = decode;
/* Tezos_Timestamp Not a pure module */
