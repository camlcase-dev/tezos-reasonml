/**
 * $alpha.operation.alpha.operation_result.transaction:
 *   { /* Applied */
 *     "status": "applied",
 *     "storage"?: $micheline.alpha.michelson_v1.expression,
 *     "big_map_diff"?: $alpha.contract.big_map_diff,
 *     "balance_updates"?: $alpha.operation_metadata.alpha.balance_updates,
 *     "originated_contracts"?: [ $alpha.contract_id ... ],
 *     "consumed_gas"?: $bignum,
 *     "storage_size"?: $bignum,
 *     "paid_storage_size_diff"?: $bignum,
 *     "allocated_destination_contract"?: boolean }
 *   || { /* Failed */
 *        "status": "failed",
 *        "errors": [ $alpha.error ... ] }
 *   || { /* Skipped */
 *        "status": "skipped" }
 *   || { /* Backtracked */
 *        "status": "backtracked",
 *        "errors"?: [ $alpha.error ... ],
 *        "storage"?: $micheline.alpha.michelson_v1.expression,
 *        "big_map_diff"?: $alpha.contract.big_map_diff,
 *        "balance_updates"?: $alpha.operation_metadata.alpha.balance_updates,
 *        "originated_contracts"?: [ $alpha.contract_id ... ],
 *        "consumed_gas"?: $bignum,
 *        "storage_size"?: $bignum,
 *        "paid_storage_size_diff"?: $bignum,
 *        "allocated_destination_contract"?: boolean }
 */
module Transaction = {
  module Applied = {
    type t = {
      storage: option(Tezos_Expression.t),
      bigMapDiff: option(Tezos_Expression.t),
      balanceUpdates: option(list(Tezos_BalanceUpdate.t)),
      originatedContracts: option(list(string)),
      consumedGas: option(Bigint.t),
      storageSize: option(Bigint.t),
      paidStorageSizeDiff: option(Bigint.t),
      allocatedDestinationContract: option(bool),
    };

    // optional(field("annots", list(string)), json)
    let decode = (json: Js.Json.t): Belt.Result.t(t, string) => {
      Json.Decode.(
        switch (
          optional(
            field("storage", a =>
              Tezos_Util.unwrapResult(Tezos_Expression.decode(a))
            ),
            json,
          ),
          optional(
            field("big_map_diff", a =>
              Tezos_Util.unwrapResult(Tezos_Expression.decode(a))
            ),
            json,
          ),
          optional(
            field("balance_updates", a =>
              list(
                b => Tezos_Util.unwrapResult(Tezos_BalanceUpdate.decode(b)),
                a,
              )
            ),
            json,
          ),
          optional(field("originated_contracts", list(string)), json),
          optional(
            field("consumed_gas", a =>
              Tezos_Util.unwrapResult(Tezos_Util.bigintDecode(a))
            ),
            json,
          ),
          optional(
            field("storage_size", a =>
              Tezos_Util.unwrapResult(Tezos_Util.bigintDecode(a))
            ),
            json,
          ),
          optional(
            field("paid_storage_size_diff", a =>
              Tezos_Util.unwrapResult(Tezos_Util.bigintDecode(a))
            ),
            json,
          ),
          optional(field("allocated_destination_contract", bool), json),
        ) {
        | (
            storage,
            bigMapDiff,
            balanceUpdates,
            originatedContracts,
            consumedGas,
            storageSize,
            paidStorageSizeDiff,
            allocatedDestinationContract,
          ) =>
          Belt.Result.Ok({
            storage,
            bigMapDiff,
            balanceUpdates,
            originatedContracts,
            consumedGas,
            storageSize,
            paidStorageSizeDiff,
            allocatedDestinationContract,
          })
        | exception (DecodeError(error)) => Belt.Result.Error(error)
        }
      );
    };
  };

  type t =
    | Applied(Applied.t)
    | Failed
    | Skipped
    | Backtracked;

  let decode = (json: Js.Json.t): Belt.Result.t(t, string) => {
    Json.Decode.(
      switch (field("status", string, json)) {
      | "applied" => Belt.Result.map(Applied.decode(json), a => Applied(a))
      | "failed" => Belt.Result.Ok(Failed)
      | "skipped" => Belt.Result.Ok(Skipped)
      | "backtracked" => Belt.Result.Ok(Backtracked)
      | status => Belt.Result.Error("Unexpected status: " ++ status)
      | exception (DecodeError(error)) => Belt.Result.Error(error)
      }
    );
  };
};
