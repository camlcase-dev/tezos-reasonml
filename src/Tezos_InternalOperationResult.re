/**
 *  $alpha.operation.alpha.internal_operation_result:
 *   { /* reveal */
 *     "kind": "reveal",
 *     "source": $alpha.contract_id,
 *     "nonce": integer ∈ [0, 2^16-1],
 *     "public_key": $Signature.Public_key,
 *     "result": $alpha.operation.alpha.operation_result.reveal }
 *   || { /* transaction */
 *        "kind": "transaction",
 *        "source": $alpha.contract_id,
 *        "nonce": integer ∈ [0, 2^16-1],
 *        "amount": $alpha.mutez,
 *        "destination": $alpha.contract_id,
 *        "parameters"?:
 *          { "entrypoint": $alpha.entrypoint,
 *            "value": $micheline.alpha.michelson_v1.expression },
 *        "result": $alpha.operation.alpha.operation_result.transaction }
 *   || { /* origination */
 *        "kind": "origination",
 *        "source": $alpha.contract_id,
 *        "nonce": integer ∈ [0, 2^16-1],
 *        "balance": $alpha.mutez,
 *        "delegate"?: $Signature.Public_key_hash,
 *        "script": $alpha.scripted.contracts,
 *        "result": $alpha.operation.alpha.operation_result.origination }
 *   || { /* delegation */
 *        "kind": "delegation",
 *        "source": $alpha.contract_id,
 *        "nonce": integer ∈ [0, 2^16-1],
 *        "delegate"?: $Signature.Public_key_hash,
 *        "result": $alpha.operation.alpha.operation_result.delegation }
 */

module Transaction = {
  type t = {
    source: string,
    amount: Tezos_Mutez.t,
    destination: string,
    parameters: option(Tezos_Parameters.t),
  };

  let decode = (json: Js.Json.t) => {
    switch (
      Json.Decode.(
        field("source", string, json),
        field("amount", a => Tezos_Util.unwrapResult(Tezos_Mutez.decode(a)), json),
        field("destination", string, json),
        optional(
          field("parameters", a => Tezos_Util.unwrapResult(Tezos_Parameters.decode(a))),
          json,
        ),
      )
    ) {
    | (source, amount, destination, parameters) =>
      Belt.Result.Ok({source, amount, destination, parameters})

    | exception (Json.Decode.DecodeError(error)) => Belt.Result.Error(error)
    };
  };
};

type t =
  | Reveal
  | Transaction(Transaction.t)
  | Origination
  | Delegation;

let decode = (json: Js.Json.t) => {
  switch (Json.Decode.(field("kind", string, json))) {
  | "reveal" => Belt.Result.Ok(Reveal)
  | "transaction" =>
    Belt.Result.map(Transaction.decode(json), a => Transaction(a))
  | "origination" => Belt.Result.Ok(Origination)
  | "delegation" => Belt.Result.Ok(Delegation)
  | kind => Belt.Result.Error("Unexpected kind: " ++ kind)
  | exception (Json.Decode.DecodeError(error)) => Belt.Result.Error(error)
  };
};
