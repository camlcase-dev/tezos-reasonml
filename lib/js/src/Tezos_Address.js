'use strict';

var Belt_Id = require("bs-platform/lib/js/belt_Id.js");
var Caml_obj = require("bs-platform/lib/js/caml_obj.js");
var Belt_Option = require("bs-platform/lib/js/belt_Option.js");
var Tezos_Contract = require("./Tezos_Contract.js");
var Tezos_ImplicitAccount = require("./Tezos_ImplicitAccount.js");

var zero = {
  TAG: /* ImplicitAccount */0,
  _0: Tezos_ImplicitAccount.zero
};

function ofString(candidate) {
  var c = Tezos_Contract.ofString(candidate);
  if (!c.TAG) {
    return {
            TAG: /* Ok */0,
            _0: {
              TAG: /* Contract */1,
              _0: c._0
            }
          };
  }
  var c$1 = Tezos_ImplicitAccount.ofString(candidate);
  if (c$1.TAG) {
    return {
            TAG: /* Error */1,
            _0: "Address.ofString: unexpected candidate string: " + candidate
          };
  } else {
    return {
            TAG: /* Ok */0,
            _0: {
              TAG: /* ImplicitAccount */0,
              _0: c$1._0
            }
          };
  }
}

function ofContract(c) {
  return {
          TAG: /* Contract */1,
          _0: c
        };
}

function ofImplicitAccount(c) {
  return {
          TAG: /* ImplicitAccount */0,
          _0: c
        };
}

function toString(t) {
  if (t.TAG) {
    return Tezos_Contract.toString(t._0);
  } else {
    return Tezos_ImplicitAccount.toString(t._0);
  }
}

function pack(t) {
  if (t.TAG) {
    return Tezos_Contract.pack(t._0);
  } else {
    return Tezos_ImplicitAccount.pack(t._0);
  }
}

function unpack(str) {
  if (str.startsWith("tz1") || str.startsWith("tz2") || str.startsWith("tz3")) {
    return Belt_Option.map(Tezos_ImplicitAccount.unpack(str), (function (a) {
                  return {
                          TAG: /* ImplicitAccount */0,
                          _0: a
                        };
                }));
  } else {
    return Belt_Option.map(Tezos_Contract.unpack(str), (function (a) {
                  return {
                          TAG: /* Contract */1,
                          _0: a
                        };
                }));
  }
}

function toScriptExpr(t) {
  if (t.TAG) {
    return Tezos_Contract.toScriptExpr(t._0);
  } else {
    return Tezos_ImplicitAccount.toScriptExpr(t._0);
  }
}

function encode(t) {
  if (t.TAG) {
    return Tezos_Contract.encode(t._0);
  } else {
    return Tezos_ImplicitAccount.encode(t._0);
  }
}

function decode(json) {
  var c = Tezos_Contract.decode(json);
  if (!c.TAG) {
    return {
            TAG: /* Ok */0,
            _0: {
              TAG: /* Contract */1,
              _0: c._0
            }
          };
  }
  var c$1 = Tezos_ImplicitAccount.decode(json);
  if (c$1.TAG) {
    return {
            TAG: /* Error */1,
            _0: "Address.decode failed: string is not an address."
          };
  } else {
    return {
            TAG: /* Ok */0,
            _0: {
              TAG: /* ImplicitAccount */0,
              _0: c$1._0
            }
          };
  }
}

function cmp(c0, c1) {
  if (c0.TAG) {
    if (c1.TAG) {
      return Caml_obj.caml_compare(c0._0, c1._0);
    } else {
      return -1;
    }
  } else if (c1.TAG) {
    return -1;
  } else {
    return Caml_obj.caml_compare(c0._0, c1._0);
  }
}

var Comparable = Belt_Id.MakeComparable({
      cmp: cmp
    });

exports.zero = zero;
exports.ofString = ofString;
exports.ofContract = ofContract;
exports.ofImplicitAccount = ofImplicitAccount;
exports.toString = toString;
exports.pack = pack;
exports.unpack = unpack;
exports.toScriptExpr = toScriptExpr;
exports.encode = encode;
exports.decode = decode;
exports.Comparable = Comparable;
/* Comparable Not a pure module */
