open Jest;
open Expect;

exception TestError(string);

let ofStringUnsafe = s => {
  switch (Tezos_Address.ofString(s)) {
  | Belt.Result.Ok(s) => s
  | Belt.Result.Error(err) => raise @@ TestError(err)
  };
};

describe("ofString", () => {
  test("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i", _ => {
    expect(
      Tezos_Address.ofString("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i"),
    )
    |> toEqual(Belt.Result.Ok(ofStringUnsafe("tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i")))
  });

  test("tz2L9474hvA2EXyNLi4u3YnqrKVf3GzeZ8tb", _ => {
    expect(
      Tezos_Address.ofString("tz2L9474hvA2EXyNLi4u3YnqrKVf3GzeZ8tb"),
    )
    |> toEqual(Belt.Result.Ok(ofStringUnsafe("tz2L9474hvA2EXyNLi4u3YnqrKVf3GzeZ8tb")))
  });

  test("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn", _ => {
    expect(
      Tezos_Address.ofString("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn"),
    )
    |> toEqual(Belt.Result.Ok(ofStringUnsafe("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn")))
  });  
});
