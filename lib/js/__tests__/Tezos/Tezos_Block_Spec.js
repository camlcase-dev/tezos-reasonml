'use strict';

var Fs = require("fs");
var Jest = require("@glennsl/bs-jest/lib/js/src/jest.js");
var Belt_Result = require("bs-platform/lib/js/belt_Result.js");
var Tezos_Block = require("../../src/Tezos_Block.js");

Jest.describe("Tezos_Block.decode", (function (param) {
        return Jest.test("golden", (function (param) {
                      var json = JSON.parse(Fs.readFileSync("__tests__/golden/block.json", "utf8"));
                      return Jest.Expect.toEqual(true, Jest.Expect.expect(Belt_Result.isOk(Tezos_Block.decode(json))));
                    }));
      }));

/*  Not a pure module */
