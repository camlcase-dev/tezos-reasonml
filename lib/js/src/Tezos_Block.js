'use strict';

var Tezos_Util = require("./Tezos_Util.js");
var Json_decode = require("@glennsl/bs-json/lib/js/src/Json_decode.js");
var Tezos_ChainId = require("./Tezos_ChainId.js");
var Tezos_BlockHash = require("./Tezos_BlockHash.js");
var Tezos_Operation = require("./Tezos_Operation.js");
var Caml_js_exceptions = require("bs-platform/lib/js/caml_js_exceptions.js");
var Tezos_RawBlockHeader = require("./Tezos_RawBlockHeader.js");

function decode(json) {
  var val;
  var val$1;
  var val$2;
  var val$3;
  try {
    val = Json_decode.field("chain_id", (function (a) {
            return Tezos_Util.unwrapResult(Tezos_ChainId.decode(a));
          }), json);
    val$1 = Json_decode.field("hash", (function (a) {
            return Tezos_Util.unwrapResult(Tezos_BlockHash.decode(a));
          }), json);
    val$2 = Json_decode.field("header", (function (a) {
            return Tezos_Util.unwrapResult(Tezos_RawBlockHeader.decode(a));
          }), json);
    val$3 = Json_decode.field("operations", (function (a) {
            return Json_decode.list((function (b) {
                          return Json_decode.list((function (c) {
                                        return Tezos_Util.unwrapResult(Tezos_Operation.decode(c));
                                      }), b);
                        }), a);
          }), json);
  }
  catch (raw_error){
    var error = Caml_js_exceptions.internalToOCamlException(raw_error);
    if (error.RE_EXN_ID === Json_decode.DecodeError) {
      return {
              TAG: /* Error */1,
              _0: error._1
            };
    }
    throw error;
  }
  return {
          TAG: /* Ok */0,
          _0: {
            chainId: val,
            hash: val$1,
            header: val$2,
            operations: val$3
          }
        };
}

exports.decode = decode;
/* Tezos_RawBlockHeader Not a pure module */
