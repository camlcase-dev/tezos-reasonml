type t =
  | OperationHash(string);

let ofString = (str: string) => OperationHash(str);

/**
 * JSON encoding and decoding
 */

let encode = (t: t): Js.Json.t => {
  switch (t) {
  | OperationHash(str) => Json.Encode.string(str)
  };
};

let decode = (json: Js.Json.t): Belt.Result.t(t, string) => {
  switch (Json.Decode.string(json)) {
  | v => Belt.Result.Ok(OperationHash(v))
  | exception (Json.Decode.DecodeError(error)) =>
    Belt.Result.Error("Tezos_OperationHash.decode failed: " ++ error)
  };
};
