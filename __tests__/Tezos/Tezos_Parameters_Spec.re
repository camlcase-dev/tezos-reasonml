open Jest;
open Expect;

describe("Tezos_Parameters.decode", () => {
  test("golden", () => {
    let json =
      Js.Json.parseExn(
        Node.Fs.readFileAsUtf8Sync("__tests__/golden/parameters.json"),
      );
    expect(Tezos_Parameters.decode(json))
    |> toEqual(
         Belt.Result.Ok(
           {
             entrypoint: Tezos_Entrypoint.Named("xtzToToken"),
             value:
               Tezos_Expression.(
                 SingleExpression(
                   Tezos_Primitives.PrimitiveData(Tezos_PrimitiveData.Pair),
                   Some([
                     StringExpression("tz1gV3LBv1XmVEkdeRTjuUdurvCf8P85eALM"),
                     SingleExpression(
                       Tezos_Primitives.PrimitiveData(
                         Tezos_PrimitiveData.Pair,
                       ),
                       Some([
                         IntExpression(Bigint.of_int(18440679)),
                         StringExpression("2020-10-11T01:20:22.807Z"),
                       ]),
                       None,
                     ),
                   ]),
                   None,
                 )
               ),
           }: Tezos_Parameters.t,
         ),
       );
  });

  test("golden", () => {
    let json =
      Js.Json.parseExn(
        Node.Fs.readFileAsUtf8Sync(
          "__tests__/golden/parameters_as_string.json",
        ),
      );
    expect(
      Tezos_Parameters.decode(
        Js.Json.parseExn(Json.Decode.string(json)),
      ),
    )
    |> toEqual(
         Belt.Result.Ok(
           {
             entrypoint: Tezos_Entrypoint.Named("xtzToToken"),
             value:
               Tezos_Expression.(
                 SingleExpression(
                   Tezos_Primitives.PrimitiveData(Tezos_PrimitiveData.Pair),
                   Some([
                     StringExpression("tz1gV3LBv1XmVEkdeRTjuUdurvCf8P85eALM"),
                     SingleExpression(
                       Tezos_Primitives.PrimitiveData(
                         Tezos_PrimitiveData.Pair,
                       ),
                       Some([
                         IntExpression(Bigint.of_int(18440679)),
                         StringExpression("2020-10-11T01:20:22.807Z"),
                       ]),
                       None,
                     ),
                   ]),
                   None,
                 )
               ),
           }: Tezos_Parameters.t,
         ),
       );
  });
});
